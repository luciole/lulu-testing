using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static ChargementJSONStructure;
using static mainManager;

public class GD_manager : MonoBehaviour
{
    public mainManager mM;
    public bool toucherLesCHoixGDActif = false;
    public bool reussiteItem = false;
    private dropZoneGD currentLandingZoneRolledOver;
    public List<dropZoneGD> sequenceDeDropZones;

    public void resetGDManager()
    {
        sequenceDeDropZones = new List<dropZoneGD>();
        toucherLesCHoixGDActif = false;
        reussiteItem = false;
        explanationSoundFinishedBool = false;
    }

    public void toucherLesChoix()
    {
        toucherLesCHoixGDActif = true;
        foreach (groupemementChoixEtPrefabBoutonQCM b in mM.list_choix_associed_with_prefabs)
        {
            b.elementGDAssocied.clignote(0.0f, true);
            mM.playerTurn = true;
        }
    }

    public void choixTouchedGD(elementGD eleGD)
    {
        if (eleGD.currentlyBlinking)
        {
            eleGD.stopClignote();
            mM.executeAsset(eleGD.choix.son, choixTouchedGDSoundFInishedPlaying,false);
        }
    }

    public void choixTouchedGDSoundFInishedPlaying()
    {
        Debug.Log("choixTouchedGDSoundFInishedPlaying");
        bool stepfinished = true;
        foreach (groupemementChoixEtPrefabBoutonQCM b in mM.list_choix_associed_with_prefabs)
        {
            if (b.elementGDAssocied.currentlyBlinking)
            {
                stepfinished = false;
            }
        }
        Debug.Log("stepfinished " + stepfinished);
        if (stepfinished && toucherLesCHoixGDActif)
        {
            mM.playerTurn = false;
            toucherLesCHoixGDActif = false;
            Debug.Log("last object clicked toucherLesCHoixGDActif is turned off");
            mM.playSteps();
        }


    }

    public elementGD oneElementIsMoving;
    public void checkifElementIsOverAnEligiblePlace()
    {
        currentLandingZoneRolledOver = null;
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        foreach (groupemementChoixEtPrefabBoutonQCM pf in mM.list_choix_associed_with_prefabs)
        {
            if (pf.elementDropZoneAssocied != null)
            {
                pf.elementDropZoneAssocied.boxcoll.enabled = true; // on n'a besoin du collider de la dropzone que au moment du check (là on l'allume)
            }
        }

        RaycastHit2D[] hits = Physics2D.RaycastAll(mousePosition, Vector2.zero);
        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider != null && hit.collider.GetComponent<dropZoneGD>() != null)
            {
                // L'objet est au-dessus d'une landing zone
                currentLandingZoneRolledOver = hit.collider.GetComponent<dropZoneGD>();
                break;
            }
        }

        foreach (groupemementChoixEtPrefabBoutonQCM pf in mM.list_choix_associed_with_prefabs)
        {
            if (pf.elementDropZoneAssocied != null)
            {
                pf.elementDropZoneAssocied.boxcoll.enabled = false; // on n'a besoin du collider de la dropzone que au moment du check (là on l'eteint)
            }
        }
        if (currentLandingZoneRolledOver != null)
        {
            if (!currentLandingZoneRolledOver.occupied)
            {

                if (oneElementIsMoving.dropZOneWhereItsCurrentlyPlaced != null)
                {
                    oneElementIsMoving.dropZOneWhereItsCurrentlyPlaced.occupied = false;
                    oneElementIsMoving.dropZOneWhereItsCurrentlyPlaced.bienrepondu = false;
                }
                oneElementIsMoving.dropZOneWhereItsCurrentlyPlaced = currentLandingZoneRolledOver;
                currentLandingZoneRolledOver.elementgdActuellementOnIt = oneElementIsMoving;
                currentLandingZoneRolledOver.occupied = true;
                if ((currentLandingZoneRolledOver.choix.id != null && oneElementIsMoving.choix.id != null && currentLandingZoneRolledOver.choix.id == oneElementIsMoving.choix.id) || (currentLandingZoneRolledOver.choix.son!=null && oneElementIsMoving.choix.son!=null && currentLandingZoneRolledOver.choix.son == oneElementIsMoving.choix.son))
                {
                    // bonne réponse les sons sont les mêmes
                    oneElementIsMoving.transform.localPosition = currentLandingZoneRolledOver.transform.localPosition;
                    currentLandingZoneRolledOver.bienrepondu = true;
                }
                else
                {
                    // mauvaise réponse
                    currentLandingZoneRolledOver.bienrepondu = false;
                    if (mM.i.type == "tuto" || mM.i.bloqueErreurs)
                    {
                        remiseElementGDPositionDepart();
                        // en tuto ou si bloqueErreurs, on ne veut pas que le joueur fasse des mauvaise reponses, on le renvoit à la case depart avec feedback-
                        mM.executeAsset(mM.i.feedbackMoins, feedbackMoinsOuPlusHasBeenPlayed,false);

                    }
                    else
                    {
                        oneElementIsMoving.transform.localPosition = currentLandingZoneRolledOver.transform.localPosition;
                    }
                }
                bool suiteTerminee = true;
                foreach (groupemementChoixEtPrefabBoutonQCM lz in mM.list_choix_associed_with_prefabs)
                {
                    if (lz.elementDropZoneAssocied != null)
                    {
                        if (!lz.elementDropZoneAssocied.occupied)
                        {
                            suiteTerminee = false;
                            break;
                        }
                    }
                }
                reussiteItem = true;
                if (suiteTerminee)
                {
                    foreach (groupemementChoixEtPrefabBoutonQCM lz in mM.list_choix_associed_with_prefabs)
                    {
                        if (lz.elementDropZoneAssocied != null)
                        {
                            if (!lz.elementDropZoneAssocied.bienrepondu)
                            {
                                reussiteItem = false;
                                break;
                            }
                        }
                    }
                    finChoix();

                }
            }
            else if (oneElementIsMoving.dropZOneWhereItsCurrentlyPlaced == currentLandingZoneRolledOver)
            {
                // on a laché la souris au dessus de la landing zone sur laquelle on était déjà placé
                //Debug.Log("on a laché la souris au dessus de la landing zone sur laquelle on était déjà placé - il ne se passe rien");
                oneElementIsMoving.transform.localPosition = currentLandingZoneRolledOver.transform.localPosition;
            }
            else
            {
                //Debug.Log("on a laché la souris au dessus d'une autre landing zone qui est occupé et ne correspond PAS à celle de l'element actuellement déplacé");
                // on a laché la souris au dessus d'une autre landing zone qui est occupé et ne correspond PAS à celle de l'element actuellement déplacé
                remiseElementGDPositionDepart();
            }

        }
        else if (oneElementIsMoving.dropZOneWhereItsCurrentlyPlaced != null)
        {
            //Debug.Log("on a laché la souris au dessus du néant et il se trouve que l'element en trin d'etre bougé occupé précédemment une place, qu'il faut dégager");
            remiseElementGDPositionDepart();
        }
        else
        {
            //Debug.Log("on a laché la souris au dessus du néant l'élement était à sa position initiale - il ne se passe rien");
            remiseElementGDPositionDepart();
        }
    }

    private void finChoix()
    {
        //Debug.LogError("finChoix");
        if (reussiteItem)
        {
            mM.scoreItem = 1;
            //Debug.Log("suite terminée et reussie !");
        }
        else
        {
            mM.scoreItem = 0;
            //Debug.Log("suite pas encore terminée et non reussie!");
        }
        mM.playerTurn = false;
        if(mM.c.id != null && mM.c.id.Length > 0)
        {
            mM.user_choix = mM.c.id;
        }
        else if (mM.c.son != null)
        {
            mM.user_choix = mM.c.son;
        }


        //Debug.Log("playSteps final GD");
        mM.playSteps(); // on lance le feedback

    }

    private void feedbackMoinsOuPlusHasBeenPlayed()
    {

    }

    private void remiseElementGDPositionDepart()
    {
        //Debug.Log("remiseElementGDPositionDepart");
        if (oneElementIsMoving.dropZOneWhereItsCurrentlyPlaced != null)
        {
            oneElementIsMoving.dropZOneWhereItsCurrentlyPlaced.occupied = false;
            oneElementIsMoving.dropZOneWhereItsCurrentlyPlaced.bienrepondu = false;
            oneElementIsMoving.dropZOneWhereItsCurrentlyPlaced.elementgdActuellementOnIt = null;
        }
        oneElementIsMoving.dropZOneWhereItsCurrentlyPlaced = null;
        StartCoroutine(oneElementIsMoving.ReturnToInitialPosition());
    }

    public void playStepsGD()
    {
        bool playswitch = true;
        bool enditemToBeLaunched = false;
        while (playswitch)
        {
            playswitch = false;
            //Debug.Log("current_step = " + mM.current_step+ " playswitch="+ playswitch);
            switch (mM.current_step)
            {
                case -1:
                    //mM.texteCheat.text += "\npret";
                    bool uneImageExiste = mM.executeAsset(mM.i.pret, null, true);
                   mM.executeAsset(mM.i.pret, mM.flecheReadyDisplayed, false);
                break;
                case 0:
                    if (mM.posScript.currentlyInstanciatedprefabImagePrincipale != null)
                    {
                        Destroy(mM.posScript.currentlyInstanciatedprefabImagePrincipale.gameObject);
                    }
                    if (!mM.sessionConsignebeenPlayed)
                    {
                        mM.texteCheat.text += " /lancement session N°"+(mM.currentSession+1).ToString();
                        mM.sessionConsignebeenPlayed = true;
                        if(mM.executeAsset(mM.s.consigne, null, true))
                        {
                            mM.posScript.currentlyInstanciatedprefabImagePrincipale.gameObject.SetActive(true);
                        } // consigne de la session (absente du déroulement)

                        mM.executeAsset(mM.s.consigne, mM.playSteps); // consigne de la session (absente du déroulement)
                    }
                    else { playswitch = true; }
                break;
                case 1:
                    if (mM.posScript.currentlyInstanciatedprefabImagePrincipale != null)
                    {
                        Destroy(mM.posScript.currentlyInstanciatedprefabImagePrincipale.gameObject);
                    }
                    if (mM.executeAsset(mM.e.fond, null, true, true))
                    {
                        //Debug.Log("on refait apparaitre image pricnipale");
                        if (mM.posScript.currentlyInstanciatedImageEpreuveCentered != null)
                        {
                            mM.posScript.currentlyInstanciatedImageEpreuveCentered.gameObject.SetActive(true);

                        }
                    } // consigne de l'épreuve (absente du déroulement)
                    if (!mM.epreuveConsignebeenPlayed)
                    {
                        //Debug.Log("on joue la consigne d'epreuve qui n'a encore jamais été jouée");
                        mM.texteCheat.text += " /Epreuve N°" + (mM.currentEpreuve + 1).ToString() + "/" + mM.s.epreuves.Count.ToString()+" id=" + mM.e.id;
                        mM.epreuveConsignebeenPlayed = true;
                        mM.executeAsset(mM.e.consigne, mM.playSteps,false); // consigne de l'épreuve (absente du déroulement)
                    }
                    else { playswitch = true; }
                    break;
                case 2:
                    mM.texteCheat.text += " /Item N°"+(mM.currentItem +1).ToString() + "/" + mM.e.items.Count.ToString() + " id=" + mM.i.id;
                    if (mM.executeAsset(mM.i.fond, null, true))
                    {
                        mM.posScript.currentlyInstanciatedprefabImagePrincipale.gameObject.SetActive(true);
                    }
                    mM.executeAsset(mM.i.consigne, mM.playSteps,false); // consigne de l'item (présent dans le déroulement)
                break;
                case 3: // TOUCHER LES CHOIX (en GD tuto seulement !)
                    if (mM.list_choix_associed_with_prefabs != null)
                    {
                        mM.list_choix_associed_with_prefabs.Clear();
                    }
                    mM.list_choix_associed_with_prefabs = new List<groupemementChoixEtPrefabBoutonQCM>();
                    foreach (Choix ccc in mM.i.choix)
                    {
                        if (((ccc.id != null && ccc.id.Length > 0) || (ccc.son != null && ccc.son.Length > 0)) && (ccc.image != null && mM.jsonStructureScript.checkifitemexists(ccc.image,true)))
                        {
                            groupemementChoixEtPrefabBoutonQCM q = new groupemementChoixEtPrefabBoutonQCM();
                            q.choix = ccc;
                            mM.list_choix_associed_with_prefabs.Add(q);
                        }
                        else
                        {
                            // choix pas elligible car pas de string son
                        }

                    }

                    if (mM.list_choix_associed_with_prefabs.Count > 0)
                    {

                        if (mM.i.randomiser)
                        {
                            mM.list_choix_associed_with_prefabs.Sort((a, b) => UnityEngine.Random.Range(-1, 2));
                        }
                        mM.posScript.CalculateAlignment(mM.list_choix_associed_with_prefabs);

                        int indexdropzone = 0;
                        int indexgdel = 0;
                        foreach (groupemementChoixEtPrefabBoutonQCM gcpq in mM.list_choix_associed_with_prefabs)
                        {
                            gcpq.boutonqcmAssocied = gcpq.elementGDAssocied = mM.posScript.placeImages(indexgdel, gcpq.choix).GetComponent<elementGD>();
                            if (gcpq.choix.numZone > 0)
                            {
                                gcpq.elementDropZoneAssocied = mM.posScript.placeImages(indexdropzone, gcpq.choix, true).GetComponent<dropZoneGD>();
                                indexdropzone++;
                            }
                            indexgdel++;
                        }
                        if (indexdropzone == 0)
                        {
                            enditemToBeLaunched = true;
                            // probleme, cet item est invalide car aucune dropzone ne peut etre placee
                        }
                        else
                        {
                            // c'est bon on continue
                            if (mM.i.type == "tuto")
                            {
                                mM.executeAsset(mM.i.toucherLesChoix, toucherLesChoix, false); // consigne de l'item (présent dans le déroulement)
                            }
                            else { playswitch = true; }
                        }
                    }
                    else
                    {
                        Debug.Log("GD error no image to display this item, lets go to the next one");

                        mM.current_step = 10;
                    }
                    break;
                case 4: // EXPLICATION (présent dans le déroulement) equivalent au step 5 dans item_gd
                    //Debug.Log("EXPLICATION");
                    if (mM.i.type == "tuto")
                    {
                        //Debug.Log("mM.i.surlignerExplication="+ mM.i.surlignerExplication);
                        if (mM.i.surlignerExplication >= 0.0f)
                        {
                            foreach (dropZoneGD b in sequenceDeDropZones)
                            {
                                foreach (groupemementChoixEtPrefabBoutonQCM b2 in mM.list_choix_associed_with_prefabs)
                                {
                                    if (b2.elementGDAssocied != null && ((b2.choix.id!=null && b.choix.id !=null && b2.choix.id == b.choix.id) || (b2.choix.son!=null && b.choix.son!=null && b2.choix.son == b.choix.son)))
                                    {
                                        //Debug.Log("mouvement commence");
                                        StartCoroutine(b2.elementGDAssocied.GoingToDropZone(1.0f, b.trans.position, b2.choix.numZone, explanationGDDemoFinished));
                                        break;
                                    }
                                    // ces bons elements doivent etres glissés deposes vers leur cible !
                                }
                            }
                        }
                        else
                        {
                            explanationGDDemoFinished();
                        }
                        if (mM.i.explication != null && mM.i.explication.Length > 0)
                        {
                            mM.executeAsset(mM.i.explication, explanationSoundFinished, false);
                            // plus on fait clignoter le bon resultat
                        }
                        Debug.Log("mM.i.type=" + mM.i.type + " mM.i.surlignerExplication=" + mM.i.surlignerExplication);
                    }
                    else { playswitch = true; }
                    break;
                case 5: // MISE EN ACTION  (présent dans le déroulement)
                    foreach (groupemementChoixEtPrefabBoutonQCM b in mM.list_choix_associed_with_prefabs)
                    {
                        if (b.elementDropZoneAssocied == null && b.choix.numZone > 0)
                        {
                            StartCoroutine(b.elementGDAssocied.ReturnToInitialPosition(true));
                        }
                    }
                    mM.user_dureeActionOnChoix = 0.0f;
                    mM.user_choix = "";
                    mM.scoreItem = -1;
                    mM.user_choixPossibles = new string[mM.i.choix.Count];
                    for (int indexi = 0; indexi < mM.i.choix.Count; indexi++)
                    {
                        if (mM.i.choix[indexi].id != null)
                        {
                            mM.user_choixPossibles[indexi] = mM.i.choix[indexi].id;

                        }
                        else if (mM.i.choix[indexi].son != null)
                        {
                            mM.user_choixPossibles[indexi] = mM.i.choix[indexi].son;

                        }
                    }
                    if (mM.i.type == "tuto" && mM.i.miseEnAction != null && mM.i.miseEnAction.Length > 0)
                    { mM.executeAsset(mM.i.miseEnAction, mM.playerturnnow, false); }
                    else { mM.playerturnnow(); }
                    break;
                case 6:
                    mM.user_dureeActionOnChoix = Time.time - mM.actionUtilisateurStartTimeItem;
                    // action utilisateur vient d'avoir lieu
                    if (mM.i.type == "training")
                    {
                        mM.executeAsset(mM.i.cible, mM.playSteps,false);
                    }
                    else { playswitch = true; }
                    // en mode GD seulement et en training seulement on lance le son "cible" et si "explication est  TRUE" on illumine chacun des sons.
                    // apres quoi si echec ou non de la precedente chose, on lance feedback- si echec et feedback+ si reussite
                    break;
                case 7:
                   if (mM.i.type == "training")
                    {
                        if (reussiteItem)
                        {
                            { mM.executeAsset(mM.i.feedbackPlus, mM.playSteps, false); }
                        }
                        else
                        {
                            { mM.executeAsset(mM.i.feedbackMoins, mM.playSteps, false); }
                        }
                    }
                    else { playswitch = true; }
                    break;
                case 8:
                    // Mise à jour score + stockage de la réponse
                    int nbZones = 0;
                    foreach (groupemementChoixEtPrefabBoutonQCM cc in mM.list_choix_associed_with_prefabs)
                    {
                        if (cc.elementDropZoneAssocied != null)
                        {
                            nbZones++;
                        }
                    }
                    mM.user_choix_ChoisiDansLordre = new string[nbZones];
                    int indexzone = 0;
                    foreach (groupemementChoixEtPrefabBoutonQCM cc in mM.list_choix_associed_with_prefabs)
                    {
                        if (cc.elementDropZoneAssocied != null)
                        {
                            if (cc.choix.id != null)
                            {
                                mM.user_choix_ChoisiDansLordre[indexzone] = cc.choix.id;
                            }
                            else if (cc.choix.son != null)
                            {
                                mM.user_choix_ChoisiDansLordre[indexzone] = cc.choix.son;
                            }

                            indexzone++;
                        }
                    }
                    mM.newSystemeDeTraces.writeNewTrace_internal();
                    if (mM.i.type == "training")
                    {
                        mM.executeAsset(mM.i.explication, mM.playSteps, false);
                    }
                    else { playswitch = true; }
                    break;
                case 9:
                    // Explication +
                    Debug.Log("on lance explication if any "+ mM.i.type+ " mM.i.explication="+ mM.i.explication);
                    if (mM.i.type == "training")
                    {
                        Debug.Log("mM.i.surlignerExplication " + mM.i.surlignerExplication);
                        if (mM.i.surlignerExplication >= 0.0f)
                        {
                            // on fait clignoter les elementGD dans l'ordre des numzones
                            sequenceDeDropZones = sequenceDeDropZones.OrderBy(person => person.choix.numZone).ToList();
                            Debug.Log("la sequence est maintenant ordonnée sequenceDeDropZones.count="+ sequenceDeDropZones.Count);
                            explanationSoundFinishedBool = true;
                            sequenceDeDropZones[0].elementgdActuellementOnIt.clignote(0.0f,false);
                            mM.executeAsset(sequenceDeDropZones[0].elementgdActuellementOnIt.choix.son, explanationGDDemoFinished, false);
                        }
                    }
                    else { playswitch = true; }
                    break;
                case 10: // transition le son est manquant actuellement
                    foreach (groupemementChoixEtPrefabBoutonQCM cc in mM.list_choix_associed_with_prefabs)
                    {
                        if (cc.elementDropZoneAssocied != null)
                        {
                            Destroy(cc.elementDropZoneAssocied.gameobj);
                        }
                        Destroy(cc.boutonqcmAssocied.gameobj);
                        Destroy(cc.elementGDAssocied.gameobj);
                    }
                    mM.list_choix_associed_with_prefabs.Clear();
                    Debug.Log("on lance transition if any");

                    mM.executeAsset(mM.i.transition, mM.playSteps);
                    break;
                case 11:
                    Debug.Log("on lance end item");
                    enditemToBeLaunched = true;
                    break;

                default:
                    Debug.LogError("DEFAULT");
                    break;

            }
            Debug.Log(mM.current_step);
            //mM.texteCheat.text += "/" + mM.current_step;
            mM.current_step++;
            if (enditemToBeLaunched)
            {
                playswitch = false;
                mM.endItem();
            }
        }
        Debug.Log("sortie");
    }

    private bool explanationSoundFinishedBool = false;
    public void explanationSoundFinished()
    {
        Debug.Log("explanationSoundFinished");
        explanationSoundFinishedBool = true;
        if (nbDeDemoCompleted >= sequenceDeDropZones.Count)
        {
            nbDeDemoCompleted = 0;
            mM.playSteps();
        }
    }
    private int nbDeDemoCompleted = 0;
    public void explanationGDDemoFinished()
    {
        Debug.Log("explanationGDDemoFinished nbDeDemoCompleted="+ nbDeDemoCompleted+ " sequenceDeDropZones.Count="+ sequenceDeDropZones.Count);
        if (sequenceDeDropZones[nbDeDemoCompleted].elementgdActuellementOnIt != null)
        {
            sequenceDeDropZones[nbDeDemoCompleted].elementgdActuellementOnIt.stopClignote();
        }

        nbDeDemoCompleted++;
        Debug.Log("explanationGDDemoFinished nbDeDemoCompleted=" + nbDeDemoCompleted+ " explanationSoundFinishedBool="+ explanationSoundFinishedBool);
        if (nbDeDemoCompleted>= sequenceDeDropZones.Count && explanationSoundFinishedBool)
        {
            Debug.LogError("END evyrthing");
            nbDeDemoCompleted = 0;
            explanationSoundFinishedBool = false;
            mM.playSteps();
        }
        else if (sequenceDeDropZones[nbDeDemoCompleted].elementgdActuellementOnIt != null)
        {
            sequenceDeDropZones[nbDeDemoCompleted].elementgdActuellementOnIt.clignote();
            mM.executeAsset(sequenceDeDropZones[nbDeDemoCompleted].elementgdActuellementOnIt.choix.son, explanationGDDemoFinished, false);
        }
        else
        {
            Debug.Log("explanationGDDemoFinished nbDeDemoCompleted=" + nbDeDemoCompleted + " NE contient pas de elementgdActuellementOnIt");
        }
    }


}
