using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using static EvaluluScript_SendTracesFromHMSceneStart;
using System;

public static class uploadStaticMechanism
{
    // Start is called before the first frame update
    private static bool currentlySending = false;
    private static int nbOfFilesToUpload = 0;
    private static int nbOfFilesUploaded = 0;
    private static List<path_andteacherid_and_userid> savedFilesWaitingToBeUploaded;
    private static string pathDirectory;
    private static string pathDirectoryArchive;
    private static string PathDirectoryTemporary;
    private static string pathDirectoryArchiveAncien;
    private static string currentlySentFilePath = "";
    private static string currentlySentTemporaryFilePath = "";
    private static bool terminateSceneAFterTransfert = false;
    private static mainManager mM;
    private static bool fromhmscenestart = false;
    private static bool isthereOldArchivesTotest = false;
    private static int indexArchiveToTestExistenceOnServer = 0;
    private static bool modeCheckIfExistsOnServer = false;

    public class path_andteacherid_and_userid
    {
        public string path;
        public string teachercode;
        public string usercode;
        public int usericonID;
        public bool isOldArchive;
        public bool existsAlreadyOnTheServer;
    }

    [System.Serializable]
    public class NewSystemeDeTracesJSONRoot
    {
        public string deviceUniqueID;
        public string fluenceId;
        public string teacherCode;
        public string loginCode;
        public string userID;
        public string userIconID;
    }

    public static void sendTraces(bool fromhmscenestart1 = false, List<string> successSynchroAccount = null, bool terminateSceneAFterTrasfert1 = false, mainManager mM1=null)
    {
        modeCheckIfExistsOnServer = true;
        //Debug.LogError("sendTraces thisTeacherCode="+ thisTeacherCode+ " loginCode = "+ loginCode);
        pathDirectory = Application.persistentDataPath + "/newSystemeTraces/";
        pathDirectoryArchive = Application.persistentDataPath + "/newSystemeTraces/ArchiveReal";
        pathDirectoryArchiveAncien = Application.persistentDataPath + "/newSystemeTraces/Archive";
        PathDirectoryTemporary = Application.persistentDataPath + "/newSystemeTraces/Temporary";

        if (!Directory.Exists(pathDirectory))
        {
            // Si le dossier n'existe pas, le cr�er
            Directory.CreateDirectory(pathDirectory);
        }
        if (!Directory.Exists(pathDirectoryArchive))
        {
            // Si le dossier n'existe pas, le cr�er
            Directory.CreateDirectory(pathDirectoryArchive);
        }
        if (!Directory.Exists(pathDirectoryArchiveAncien))
        {
            // Si le dossier n'existe pas, le cr�er
            Directory.CreateDirectory(pathDirectoryArchiveAncien);
        }
        if (!Directory.Exists(PathDirectoryTemporary))
        {
            // Si le dossier n'existe pas, le cr�er
            Directory.CreateDirectory(PathDirectoryTemporary);
        }

        mM = mM1;

        terminateSceneAFterTransfert = terminateSceneAFterTrasfert1;
        if (!currentlySending)
        {
            nbFilesReallyUploaded = 0;
            isthereOldArchivesTotest = false;
            fromhmscenestart = fromhmscenestart1;
            currentlySending = true;
            nbOfFilesToUpload = 0;
            nbOfFilesUploaded = 0;
            savedFilesWaitingToBeUploaded = new List<path_andteacherid_and_userid>();
            savedFilesWaitingToBeUploaded.Clear();
            string[] savedFilesWaitingToBeUploaded_temp1 = Directory.GetFiles(pathDirectory, "*.json"); // attention, ici on prend TOUT les fichiers sans distinction alors qu'il faudrait � minima check le enseignant ID
            string[] savedFilesWaitingToBeUploaded_inOldArciveFolder_temp = Directory.GetFiles(pathDirectoryArchiveAncien, "*.json"); // attention, ici on prend TOUT les fichiers sans distinction alors qu'il faudrait � minima check le enseignant ID
            string[] savedFilesWaitingToBeUploaded_temp = savedFilesWaitingToBeUploaded_temp1.Concat(savedFilesWaitingToBeUploaded_inOldArciveFolder_temp).ToArray();
            if (fromhmscenestart)
            {
                foreach (string teacheraccount in successSynchroAccount)
                {
                    //Debug.LogError("account in successSynchroAccount = " + teacheraccount);
                    List<HM_ChildUser> childUsers = HM_PluginController.GetAllActiveChildsOfTeacher(teacheraccount);
                    if (childUsers != null)
                    {
                        foreach (HM_ChildUser child in childUsers)
                        {
                            //Debug.Log("userIconID=" + child.userIconID + " loginCode=" + child.loginCode + " userID=" + child.userID);
                            //si besoin �a c'est l'UUID qui est aussi renvoy� par la fonction "GetMainUserID" (au o� tu l'utilisais pour identifier les �l�ves, permettra du coup de faire le lien)
                            foreach (string filePath in savedFilesWaitingToBeUploaded_temp)
                            {
                                // Lire le contenu du fichier JSON
                                string jsonContent = System.IO.File.ReadAllText(filePath);

                                // D�s�rialiser le JSON en une instance de la classe
                                NewSystemeDeTracesJSONRoot data = JsonUtility.FromJson<NewSystemeDeTracesJSONRoot>(jsonContent);

                                // Acc�der aux propri�t�s n�cessaires
                                string teacherCode = data.teacherCode;
                                string logincodeuser = data.loginCode;
                                // Faire quelque chose avec les valeurs, par exemple, les imprimer dans la console

                                if (teacherCode == teacheraccount && logincodeuser == child.loginCode)
                                {
                                    path_andteacherid_and_userid aaa = new path_andteacherid_and_userid();
                                    aaa.path = filePath;
                                    aaa.teachercode = teacherCode;
                                    aaa.usericonID = child.userIconID;
                                    aaa.usercode = logincodeuser;
                                    aaa.existsAlreadyOnTheServer = false;
                                    //Debug.LogError("bingo");
                                    //Debug.Log($"Fichier : {filePath}, teacherCode : {teacherCode}, userIconID : {usericonID}");
                                    if (filePath.Contains("Archive"))
                                    {
                                        aaa.isOldArchive = true;
                                        isthereOldArchivesTotest = true;

                                    }
                                    else
                                    {
                                        aaa.isOldArchive = false;
                                    }
                                    savedFilesWaitingToBeUploaded.Add(aaa);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                //Debug.Log("userIconID="+child.userIconID+ " loginCode="+ child.loginCode+ " userID="+ child.userID);
                //si besoin �a c'est l'UUID qui est aussi renvoy� par la fonction "GetMainUserID" (au o� tu l'utilisais pour identifier les �l�ves, permettra du coup de faire le lien)
                foreach (string filePath in savedFilesWaitingToBeUploaded_temp)
                {
                    // Lire le contenu du fichier JSON
                    string jsonContent = System.IO.File.ReadAllText(filePath);

                    // D�s�rialiser le JSON en une instance de la classe
                    NewSystemeDeTracesJSONRoot data = JsonUtility.FromJson<NewSystemeDeTracesJSONRoot>(jsonContent);

                    // Acc�der aux propri�t�s n�cessaires
                    string teacherCode = data.teacherCode;
                    string usericonID = data.userIconID;
                    string logincodeuser = data.loginCode;
                    // Faire quelque chose avec les valeurs, par exemple, les imprimer dans la console

                    if (HM_DataController.currentTeacherAccount != null && teacherCode == HM_DataController.currentTeacherAccount.teacherCode && logincodeuser == HM_DataController.currentChildUser.loginCode)
                    {
                        path_andteacherid_and_userid aaa = new path_andteacherid_and_userid();
                        aaa.path = filePath;
                        aaa.teachercode = teacherCode;
                        aaa.usericonID = HM_DataController.currentChildUser.userIconID;
                        aaa.usercode = logincodeuser;
                        aaa.existsAlreadyOnTheServer = false;
                        //Debug.LogError("bingo");
                        //Debug.Log($"Fichier : {filePath}, teacherCode : {teacherCode}, userIconID : {usericonID}");
                        if (filePath.Contains("Archive"))
                        {
                            aaa.isOldArchive = true;
                            isthereOldArchivesTotest = true;
                        }
                        else
                        {
                            aaa.isOldArchive = false;
                        }
                        savedFilesWaitingToBeUploaded.Add(aaa);
                    }
                }
            }
            Debug.Log("au final on a � uploader et/ou a verifier " + savedFilesWaitingToBeUploaded.Count + " fichiers");


            if (!Directory.Exists(pathDirectoryArchive))
            {
                Directory.CreateDirectory(pathDirectoryArchive);
            }
            if (!Directory.Exists(PathDirectoryTemporary))
            {
                Directory.CreateDirectory(PathDirectoryTemporary);
            }
            else
            {
                deleteTemporaryFiles();
            }
            nbOfFilesToUpload = savedFilesWaitingToBeUploaded.Count;

            if (nbOfFilesToUpload > 0)
            {
                if (isthereOldArchivesTotest && modeCheckIfExistsOnServer)
                {
                    Debug.Log("START CHECK ARCHIVES ON SERVER");

                    indexArchiveToTestExistenceOnServer = 0;
                    compteurArchiveAlreadyexisting = 0;

                    isEligibleArchive(savedFilesWaitingToBeUploaded[indexArchiveToTestExistenceOnServer]);
                }
                else
                {
                    Debug.Log("NOT CHECKING ARCHIVES ON SERVER");
                    uploadOnefichierTraceToHM();
                }
            }
            else
            {
                endAllUploads();
            }
        }
    }
    private static int compteurArchiveAlreadyexisting;
    private static void isEligibleArchive(path_andteacherid_and_userid archTotest)
    {
        if (archTotest.isOldArchive)
        {
            try
            {
                if (fromhmscenestart)
                {
                    HM_PluginController.CheckTraceExist(archTotest.path, callBackIsEligibleArchive, archTotest.teachercode, archTotest.usericonID, archTotest.usercode);

                }
                else
                {
                    HM_PluginController.CheckTraceExist(archTotest.path, callBackIsEligibleArchive);

                }
            }
            catch (Exception ex)
            {
                // G�rez toute exception survenue lors de l'appel � CheckTraceExist ici.
                Debug.LogError("Erreur lors de l'appel � CheckTraceExist : " + ex.Message);
                endAllUploads();
            }
        }
        else
        {
            checkNextArchive();
        }
        
    }



    private static void callBackIsEligibleArchive(int resultCode, string resultMessage)
    {
        //Debug.LogError("CheckArchive exists on server : " + resultCode + " resultMessage=" + resultMessage);
        savedFilesWaitingToBeUploaded[indexArchiveToTestExistenceOnServer].existsAlreadyOnTheServer = false;
        if (resultCode > 0)
        {
            compteurArchiveAlreadyexisting++;
            // exists
            //Debug.LogError("EXISTS ON THE SERVER !");
            savedFilesWaitingToBeUploaded[indexArchiveToTestExistenceOnServer].existsAlreadyOnTheServer = true;
        }
        else if (resultCode == 0)
        {
            //Debug.LogError("DOES NOT EXISTS ON THE SERVER !");
            // doesnot exists
        }
        else
        {
            //Debug.LogError("ERROR WITH WIFI OR WITH THE SERVER !");
            // erreur
        }
        checkNextArchive();
    }

    private static void checkNextArchive()
    {
        indexArchiveToTestExistenceOnServer++;
        if (indexArchiveToTestExistenceOnServer >= savedFilesWaitingToBeUploaded.Count)
        {
            Debug.Log("ENDING ARCHIVE CHECK UP compteurArchiveAlreadyexisting="+ compteurArchiveAlreadyexisting+ " savedFilesWaitingToBeUploaded.Count="+ savedFilesWaitingToBeUploaded.Count);
            uploadOnefichierTraceToHM();
        }
        else
        {
            isEligibleArchive(savedFilesWaitingToBeUploaded[indexArchiveToTestExistenceOnServer]);
        }
    }

    private static void deleteTemporaryFiles()
    {
        // Obtenez la liste de tous les fichiers dans le dossier sp�cifi�.
        string[] files = Directory.GetFiles(PathDirectoryTemporary);

        foreach (string filePath in files)
        {
            if (Path.GetExtension(filePath) == ".json")
            {
                try
                {
                    // Supprimez le fichier s'il a l'extension .json.
                    File.Delete(filePath);
                    ////Debug.Log("Fichier supprim� : " + filePath);
                }
                catch (Exception e)
                {
                    ////Debug.LogError("Erreur lors de la suppression du fichier : " + e.Message);
                }
            }
        }
    }
    private static int nbFilesReallyUploaded = 0;
    private static void uploadOnefichierTraceToHM()
    {
        if (nbOfFilesUploaded >= savedFilesWaitingToBeUploaded.Count)
        {
            endAllUploads();
        }
        else
        {

            if (savedFilesWaitingToBeUploaded[nbOfFilesUploaded].existsAlreadyOnTheServer)
            {
                //Debug.Log("exists already on server, so we move it locally from the old Archive folder to the realArchive folder. path =" + savedFilesWaitingToBeUploaded[nbOfFilesUploaded].path);
                string fileName = Path.GetFileName(savedFilesWaitingToBeUploaded[nbOfFilesUploaded].path);
                // Cr�ez le chemin complet du fichier de destination dans le nouveau dossier
                string destinationARCHIVEFilePath = Path.Combine(pathDirectoryArchive, fileName);
                // D�placez le fichier source vers le dossier de destination
                ////Debug.Log("fichierTemporary moved in archive" + fichierTemporary);
                if (File.Exists(savedFilesWaitingToBeUploaded[nbOfFilesUploaded].path))
                {
                    if (!File.Exists(destinationARCHIVEFilePath))
                    {
                        File.Move(savedFilesWaitingToBeUploaded[nbOfFilesUploaded].path, destinationARCHIVEFilePath);
                        //Debug.Log("old archive already existing on the server was moved into realArchive folder");
                    }
                    else
                    {
                        File.Delete(savedFilesWaitingToBeUploaded[nbOfFilesUploaded].path);
                        //Debug.Log("old archive already existing on the server also already existed in the realArchive folder, so we delete it");

                    }
                }

                if (nbOfFilesUploaded == nbOfFilesToUpload - 1)
                {
                    //Debug.Log("old archive already existing on the server was the last piece to review. End all uploads now");

                    endAllUploads();

                }
                else
                {
                    // le transfeet pr�c�dent c'est bien pass� (retour == 0) on lance le fichier suivant
                    nbOfFilesUploaded++;
                    uploadOnefichierTraceToHM();
                }
            }
            else
            {

                currentlySentFilePath = savedFilesWaitingToBeUploaded[nbOfFilesUploaded].path;
                // Le fichier JSON existe, donc nous le lisons
                string dateEnvoi = DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss");

                string nouveauNomAvecDate = Path.GetFileNameWithoutExtension(currentlySentFilePath) + "-" + dateEnvoi + ".json";
                //Debug.Log("on etudie " + currentlySentFilePath);
                if (savedFilesWaitingToBeUploaded[nbOfFilesUploaded].isOldArchive)
                {
                    //Debug.LogError("on traite une archive l�");
                    nouveauNomAvecDate = Path.GetFileNameWithoutExtension(currentlySentFilePath) + "-ARC-" + dateEnvoi + ".json";
                }



                currentlySentTemporaryFilePath = Path.Combine(PathDirectoryTemporary, nouveauNomAvecDate);
                //Debug.Log("nouveauNomAvecDate = " + nouveauNomAvecDate + " currentlySentTemporaryFilePath=" + currentlySentTemporaryFilePath);
                // Copiez le fichier source vers le dossier de destination en le renommant si n�cessaire.

                File.Copy(currentlySentFilePath, currentlySentTemporaryFilePath, true);
                if (File.Exists(currentlySentTemporaryFilePath))
                {
                    try
                    {
                        string teachercode = savedFilesWaitingToBeUploaded[nbOfFilesUploaded].teachercode;
                        int userIconID = savedFilesWaitingToBeUploaded[nbOfFilesUploaded].usericonID;
                        string userloginCode = savedFilesWaitingToBeUploaded[nbOfFilesUploaded].usercode;
                        if (fromhmscenestart)
                        {
                            HM_PluginController.UploadTrace(currentlySentTemporaryFilePath, uploadOneFileAsynchroneFinished, teachercode, userIconID, userloginCode);

                        }
                        else
                        {
                            HM_PluginController.UploadTrace(currentlySentTemporaryFilePath, uploadOneFileAsynchroneFinished);

                        }
                    }
                    catch (Exception ex)
                    {
                        // G�rez toute exception survenue lors de l'appel � CheckTraceExist ici.
                        Debug.LogError("Erreur lors de l'appel � UploadTrace : " + ex.Message);
                        deletecurrentFileFromTemporaryFolder(false);
                        endAllUploads();
                    }
                }
                else
                {
                    Debug.LogError("Traces 2 - ne devrait jamais arriver");
                    deletecurrentFileFromTemporaryFolder(false);
                    endAllUploads();
                    /* sauf si : 
                     * Fichier source inexistant : Si le fichier source sp�cifi� n'existe pas, la copie �chouera.
                        Permissions insuffisantes : Si l'application n'a pas les permissions n�cessaires pour lire le fichier source ou �crire dans le r�pertoire de destination, la copie �chouera.
                        Espace disque insuffisant : Si le disque de destination est plein ou manque d'espace, la copie �chouera.
                        Fichier de destination verrouill� : Si le fichier de destination est d�j� ouvert ou verrouill� par une autre application, la copie �chouera.
                        Probl�mes mat�riels : Des probl�mes mat�riels, tels que des secteurs d�fectueux sur le disque, peuvent entra�ner des erreurs lors de la copie
                    */
                }
            }

        }

        
    }

    private static void deletecurrentFileFromTemporaryFolder(bool SuccesSomoveitToTheArchiveFolder)
    {
        if (SuccesSomoveitToTheArchiveFolder)
        {
            // Obtenez le nom du fichier sans le chemin
            string fileName = Path.GetFileName(currentlySentTemporaryFilePath);
            // Cr�ez le chemin complet du fichier de destination dans le nouveau dossier
            string destinationARCHIVEFilePath = Path.Combine(pathDirectoryArchive, fileName);
            // D�placez le fichier source vers le dossier de destination
            ////Debug.Log("fichierTemporary moved in archive" + fichierTemporary);
            if (File.Exists(currentlySentTemporaryFilePath))
            {
                if (!File.Exists(destinationARCHIVEFilePath))
                {
                    File.Move(currentlySentTemporaryFilePath, destinationARCHIVEFilePath);
                }
                else
                {
                    File.Delete(currentlySentTemporaryFilePath);
                }
            }
            ////Debug.Log("ToDeleteOriginalFilePath = " + fichierOriginal);
            // et on supprime le fichier original vu qu'il a �t� mis dans l'archive
            if (File.Exists(currentlySentFilePath))
            {
                File.Delete(currentlySentFilePath);
                ////Debug.Log("fichier original deleted " + fichierOriginal);
            }
            else
            {
                ////Debug.Log("le fichier original n'existe pas ");
            }
        }
        else
        {
            // on supprime le fichier de temporary
            if (File.Exists(currentlySentTemporaryFilePath))
            {
                File.Delete(currentlySentTemporaryFilePath);
                ////Debug.Log("fichier original deleted " + fichierOriginal);
            }
        }
    }

    private static void uploadOneFileAsynchroneFinished(int resultCode, string resultMessage)
    {
        //Debug.LogError("resultCode=" + resultCode + " resultMessage=" + resultMessage + " nombreDeFichiersTraites=" + nbOfFilesUploaded + " nombreDeFichiersATransferer=" + nbOfFilesToUpload);
        ////Debug.Log("transfertAsynchroneFinished resultCode = " + resultCode.ToString() + " resultMessage=" + resultMessage + " nombreDeFichiersTraites = " + nombreDeFichiersTraites);

        if (resultCode == 1 || resultCode == 0)
        {
            nbFilesReallyUploaded++;
            deletecurrentFileFromTemporaryFolder(true);
            if (nbOfFilesUploaded == nbOfFilesToUpload - 1)
            {
                endAllUploads();

            }
            else
            {
                // le transfeet pr�c�dent c'est bien pass� (retour == 0) on lance le fichier suivant
                nbOfFilesUploaded++;
                uploadOnefichierTraceToHM();
            }


            // Bien pass�
        }
        else
        {
            Debug.LogError("erreur lors de l'envoi on stop tout");
            deletecurrentFileFromTemporaryFolder(false);
            endAllUploads();
        }
    }

    private static void endAllUploads()
    {
        Debug.LogError("endTransferts nbFilesReallyUploaded="+ nbFilesReallyUploaded);
        nbOfFilesUploaded = 0;
        nbOfFilesToUpload = 0;
        nbFilesReallyUploaded = 0;
        currentlySending = false;
        if (terminateSceneAFterTransfert && mM != null)
        {
            //  StartCoroutine(mM.fenetrePause.terminateScenefterDeconnexionAndTracesSent());
            mM.launchTerminateScene();
        }
        else
        {

        }
    }
}
