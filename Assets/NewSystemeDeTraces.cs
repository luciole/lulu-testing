using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Linq;
using static System.Collections.Specialized.BitVector32;
using UnityEngine.SceneManagement;
using UnityEditor;
using DG.Tweening;
using static ChargementJSONStructure;

public class NewSystemeDeTraces : MonoBehaviour
{

    public mainManager mM;
    private NewSystemeDeTracesJSONRoot traceJson;
    private int volumeCourant = 1;
    private long seuilPoids = 200000;
    public EvaluluScript_SendTracesFromHMSceneStart prefabUpdateTraces;

    [System.Serializable]
    public class NewSystemeDeTracesJSONRoot
    {
        public string deviceUniqueID;
        public string fluenceId;
        public string teacherCode;
        public string loginCode;
        public string userID;
        public string userIconID;
        public List<NewSystemeDeTraces_appliVersionLuciole> versionsLuciole;
    }
    [System.Serializable]
    public class NewSystemeDeTraces_appliVersionLuciole
    {
        public string applicationVersionLuciole;
        public string iosBuild;
        public string androidBundleVersionCode;
        public List<NewSystemeDeTraces_session> sessions;
    }
    [System.Serializable]
    public class NewSystemeDeTraces_session
    {
        public int session;
        public string date;
        public List<NewSystemeDeTraces_epreuve> epreuves;
        public int scoreSession;
    }
    [System.Serializable]
    public class NewSystemeDeTraces_epreuve
    {
        public string epreuve;
        public List<NewSystemeDeTraces_item> items;
        public int scoreEpreuve;
    }
    [System.Serializable]
    public class NewSystemeDeTraces_item
    {
        public string id;
        public string[] listeChoix;
        public float duree;
        public string choix;
        public string[] choixGDDansLordreVouluParUser;
        public int scoreItem;
    }
    private string jsonFilePath;
    string pathDirectory;
    string pathDirectoryArchive;
   
    public void initialiseNewSystemeDeTraces(bool poidsDepassedAfterSave = false)
    {
        pathDirectory = Application.persistentDataPath + "/newSystemeTraces/";
        pathDirectoryArchive = Application.persistentDataPath + "/newSystemeTraces/ArchiveReal";
        // Obtenez le chemin du fichier JSON en fonction de la plateforme
        jsonFilePath = GetJsonFilePath(poidsDepassedAfterSave);

        // V�rifiez si le fichier JSON existe
        string jsonContents = "";
        if (File.Exists(jsonFilePath))
        {
            // Le fichier JSON existe, donc nous le lisons

            jsonContents = File.ReadAllText(jsonFilePath);
            //mM.texteCheat.text += " un json existe deja length= " + jsonContents.Length;
            //Debug.Log("Contenu JSON existant : " + jsonContents);
        }
        else
        {
            // Le fichier JSON n'existe pas, donc nous le cr�ons avec des donn�es JSON par d�faut
            jsonContents = produceNewJsonTrace();
            File.WriteAllText(jsonFilePath, jsonContents);
            //Debug.Log("Fichier JSON cr�� avec des donn�es par d�faut : " + jsonContents);
        }

        if (jsonContents.Length > 0)
        {
            traceJson = JsonUtility.FromJson<NewSystemeDeTracesJSONRoot>(jsonContents);

        }
    }



    public static string returnUserIDFLuence_or_UserIDHM()
    {
        string UserIdToUse = "Evalulu_missin_gmode_Plugin_HM_cant_fetchUserID";
        
        if (SplashScreenScript.mode_Plugin_HM && HM_DataController.currentChildUser != null)
        {
            UserIdToUse = "Evalulu_currentTeacherAccount_" + HM_DataController.currentTeacherAccount.teacherCode.ToString() + "_HM_userID_" + HM_DataController.currentChildUser.userID.ToString() + "_HM_loginCode_" + HM_DataController.currentChildUser.loginCode;
        }
        return UserIdToUse;
    }
    private int nombreDeFichiersATransferer = 0;
    private int nombreDeFichiersTraites = 0;
    private bool terminateSceneAFterTransfert = false;
    private bool envoieEnCours = false;
    private string[] fichiersSauvegardes;
    public void sendThisUserTracesToHMPlugin(bool terminateSceneAFterTrasfert1 = false)
    {
        //Debug.LogError("sendThisUserTracesToHMPlugin launched envoieEnCours="+ envoieEnCours+ " terminateSceneAFterTransfert="+ terminateSceneAFterTransfert);
        uploadStaticMechanism.sendTraces(false, null, terminateSceneAFterTrasfert1,mM);
    }

    private string GetJsonFilePath(bool poidsDepassedAfterSave = false)
    {
        // Assurez-vous que le dossier existe
        if (!Directory.Exists(pathDirectory))
        {
            Directory.CreateDirectory(pathDirectory);
        }
        if (!Directory.Exists(pathDirectoryArchive))
        {
            Directory.CreateDirectory(pathDirectoryArchive);
        }
        string nomFichierBase = returnUserIDFLuence_or_UserIDHM();
        string extension = ".json";
        string path = pathDirectory + nomFichierBase + "_vol" + volumeCourant.ToString() + extension;

        if (!poidsDepassedAfterSave)
        {
            string[] fichiersSauvegardes = Directory.GetFiles(pathDirectory, $"{nomFichierBase}_vol*.json");
            if (fichiersSauvegardes.Length > 0)
            {
                // Triez les fichiers par ordre d�croissant de num�ro de volume.
                Array.Sort(fichiersSauvegardes, (a, b) =>
                {
                    int numeroA = int.Parse(Path.GetFileNameWithoutExtension(a).Split('_').LastOrDefault().Replace("vol", ""));
                    int numeroB = int.Parse(Path.GetFileNameWithoutExtension(b).Split('_').LastOrDefault().Replace("vol", ""));
                    return numeroB.CompareTo(numeroA);
                });

                // Obtenez le nom du fichier le plus r�cent.
                string fichierLePlusRecent = fichiersSauvegardes[0];
                volumeCourant = int.Parse(Path.GetFileNameWithoutExtension(fichierLePlusRecent).Split('_').LastOrDefault().Replace("vol", ""));
                //Console.WriteLine("Le fichier le plus r�cent est : " + fichierLePlusRecent);

                // Chargez les donn�es � partir du fichier le plus r�cent.
                // Ajoutez votre logique de chargement ici.
            }
            else
            {
                //Console.WriteLine("Aucun fichier de sauvegarde trouv�.");
            }
            path = pathDirectory + nomFichierBase + "_vol" + volumeCourant.ToString() + extension;

            if (File.Exists(path))
            {
                FileInfo infoFichier = new FileInfo(path);
                long poidsFichier = infoFichier.Length;

                if (poidsFichier > seuilPoids)
                {

                    //Console.WriteLine($"Le fichier JSON d�passe {seuilPoids} octets.");
                    // Effectuez ici l'action que vous souhaitez, par exemple, versionnez le fichier.
                    volumeCourant++;
                }
                else
                {
                    //Console.WriteLine("Le fichier JSON est en dessous du seuil de poids.");
                }
            }
            else
            {
                //Console.WriteLine("Le fichier JSON n'existe pas.");
            }
        }
        else
        {
            volumeCourant++;
        }



        path = pathDirectory + nomFichierBase + "_vol" + volumeCourant.ToString() + extension;
        return path;
    }
    [SerializeField] private string version_text;
    [SerializeField] private string applicationVersionLuciole;
    [SerializeField] private string iosBuild;
    [SerializeField] private int androidBundleVersionCode;
    public string updateVersionLuciole()
    {
#if UNITY_EDITOR
        version_text = "v_" + Application.version;
        version_text = version_text + "bvc_" + PlayerSettings.Android.bundleVersionCode;
        applicationVersionLuciole = Application.version;
        androidBundleVersionCode = PlayerSettings.Android.bundleVersionCode;
        iosBuild = PlayerSettings.iOS.buildNumber;
#endif
        ////Debug.LogError("androidBundleVersionCode =" + androidBundleVersionCode);
        return version_text;
    }
    public string getApplicationVersionLuciole()
    {
        updateVersionLuciole();
        return applicationVersionLuciole;
    }
    public string getIosBuild()
    {
        updateVersionLuciole();
        return iosBuild;
    }
    public int getAndroidBundleVersionCode()
    {
        updateVersionLuciole();
        return androidBundleVersionCode;
    }
    public string produceNewJsonTrace()
    {
        NewSystemeDeTracesJSONRoot tracesJson = new NewSystemeDeTracesJSONRoot();
        tracesJson.fluenceId = returnUserIDFLuence_or_UserIDHM();
        
        if (HM_DataController.currentTeacherAccount != null)
        {
            tracesJson.teacherCode = HM_DataController.currentTeacherAccount.teacherCode.ToString();

        }
        else
        {
            tracesJson.teacherCode = "missingHMdataController";
        }
        if (HM_DataController.currentChildUser != null)
        {
            tracesJson.loginCode = HM_DataController.currentChildUser.loginCode.ToString();
            tracesJson.userID = HM_DataController.currentChildUser.userID.ToString();
            tracesJson.userIconID = HM_DataController.currentChildUser.userIconID.ToString();
        }
        else
        {
            tracesJson.loginCode = "missing";
            tracesJson.userID = "missing";
            tracesJson.userIconID = "missing";
        }

        tracesJson.deviceUniqueID = SystemInfo.deviceUniqueIdentifier;
        tracesJson.versionsLuciole = new List<NewSystemeDeTraces_appliVersionLuciole>();
        NewSystemeDeTraces_appliVersionLuciole versionCourante = new NewSystemeDeTraces_appliVersionLuciole();
        versionCourante.applicationVersionLuciole = getApplicationVersionLuciole();
        versionCourante.iosBuild = getIosBuild();
        versionCourante.androidBundleVersionCode = getAndroidBundleVersionCode().ToString();
        tracesJson.versionsLuciole.Add(versionCourante);
        return JsonUtility.ToJson(tracesJson, true);
    }
    public string GetFormattedDate()
    {
        DateTime currentDate = DateTime.Now;
        string formattedDate = currentDate.ToString("yyyy-MM-dd");
        return formattedDate;
    }
    public void writeNewTrace_internal()
    {

        String applicationVersionLuciole = getApplicationVersionLuciole();
        if (traceJson.versionsLuciole == null)
        {
            traceJson.versionsLuciole = new List<NewSystemeDeTraces_appliVersionLuciole>();
            NewSystemeDeTraces_appliVersionLuciole versionCourante = new NewSystemeDeTraces_appliVersionLuciole();
            versionCourante.applicationVersionLuciole = applicationVersionLuciole;
            traceJson.versionsLuciole.Add(versionCourante);
        }
        else if (traceJson.versionsLuciole[traceJson.versionsLuciole.Count - 1].applicationVersionLuciole != applicationVersionLuciole)
        {
            traceJson.versionsLuciole.Add(new NewSystemeDeTraces_appliVersionLuciole());
        }
        int n = traceJson.versionsLuciole.Count - 1;
        traceJson.versionsLuciole[n].applicationVersionLuciole = applicationVersionLuciole;
        traceJson.versionsLuciole[n].iosBuild = getIosBuild();
        traceJson.versionsLuciole[n].androidBundleVersionCode = getAndroidBundleVersionCode().ToString();
        if (traceJson.versionsLuciole[n].sessions == null)
        {
            traceJson.versionsLuciole[n].sessions = new List<NewSystemeDeTraces_session>();
            traceJson.versionsLuciole[n].sessions.Add(new NewSystemeDeTraces_session());
        }
        else if (traceJson.versionsLuciole[n].sessions.Count == 0 || traceJson.versionsLuciole[n].sessions[traceJson.versionsLuciole[n].sessions.Count - 1].date != mM.dateWhenSessionWasLaunched)
        {
            traceJson.versionsLuciole[n].sessions.Add(new NewSystemeDeTraces_session());
        }
        else if (traceJson.versionsLuciole[n].sessions[traceJson.versionsLuciole[n].sessions.Count - 1].session != mM.currentSession)
        {
            traceJson.versionsLuciole[n].sessions.Add(new NewSystemeDeTraces_session());
        }
        int m = traceJson.versionsLuciole[n].sessions.Count - 1;
        traceJson.versionsLuciole[n].sessions[m].session = mM.currentSession;
        traceJson.versionsLuciole[n].sessions[m].date = mM.dateWhenSessionWasLaunched;
        traceJson.versionsLuciole[n].sessions[m].scoreSession = mM.scoreSession;
        if (traceJson.versionsLuciole[n].sessions[m].epreuves == null)
        {
            traceJson.versionsLuciole[n].sessions[m].epreuves = new List<NewSystemeDeTraces_epreuve>();
            traceJson.versionsLuciole[n].sessions[m].epreuves.Add(new NewSystemeDeTraces_epreuve());
        }
        else if (traceJson.versionsLuciole[n].sessions[m].epreuves.Count == 0 || traceJson.versionsLuciole[n].sessions[m].epreuves[traceJson.versionsLuciole[n].sessions[m].epreuves.Count - 1].epreuve != mM.e.id)
        {
            traceJson.versionsLuciole[n].sessions[m].epreuves.Add(new NewSystemeDeTraces_epreuve());
        }
        int o = traceJson.versionsLuciole[n].sessions[m].epreuves.Count - 1;
        traceJson.versionsLuciole[n].sessions[m].epreuves[o].epreuve = mM.e.id;

        if (traceJson.versionsLuciole[n].sessions[m].epreuves[o].items == null)
        {
            traceJson.versionsLuciole[n].sessions[m].epreuves[o].items = new List<NewSystemeDeTraces_item>();
        }

        NewSystemeDeTraces_item newitem = new NewSystemeDeTraces_item();
        newitem.id = mM.i.id;
        newitem.duree = mM.user_dureeActionOnChoix;
        newitem.choix = mM.user_choix;
        newitem.scoreItem = mM.scoreItem;
        newitem.choixGDDansLordreVouluParUser = mM.user_choix_ChoisiDansLordre;
        newitem.listeChoix = mM.user_choixPossibles;
        traceJson.versionsLuciole[n].sessions[m].epreuves[o].items.Add(newitem);

        // si tous les items de cette epreuves sont pass�s, on �tabli le scoring de l'�preuve � partir de ses scores d' items, sinon, le scoring de l'�preuve est � -1
        if (mM.currentItem >= mM.jsonStructureScript.data.sessions[mM.currentSession].epreuves[mM.currentEpreuve].items.Count - 1)
        {
            int scoreepreuve = 0;
            foreach (NewSystemeDeTraces_item itt in traceJson.versionsLuciole[n].sessions[m].epreuves[o].items)
            {
                scoreepreuve += itt.scoreItem;
            }
            traceJson.versionsLuciole[n].sessions[m].epreuves[o].scoreEpreuve = scoreepreuve;
        }
        else
        {
            traceJson.versionsLuciole[n].sessions[m].epreuves[o].scoreEpreuve = -1;
        }

        // si toutes les epreuves de cette session sont pass�s, on �tabli le scoring de la session � partir de ses scores d'epreuves, sinon, le scoring de la session est � -1
        if (mM.currentEpreuve >= mM.jsonStructureScript.data.sessions[mM.currentSession].epreuves.Count - 1)
        {
            int scoreSession = 0;
            foreach (NewSystemeDeTraces_epreuve epr in traceJson.versionsLuciole[n].sessions[m].epreuves)
            {
                scoreSession += epr.scoreEpreuve;
            }
            traceJson.versionsLuciole[n].sessions[m].scoreSession = scoreSession;
        }
        else
        {
            traceJson.versionsLuciole[n].sessions[m].scoreSession = -1;
        }

        saveNewSystemeTraces();
    }
    public void saveNewSystemeTraces()
    {
        System.IO.File.WriteAllText(jsonFilePath, JsonUtility.ToJson(traceJson, true));
        if (File.Exists(jsonFilePath))
        {
            FileInfo infoFichier = new FileInfo(jsonFilePath);
            long poidsFichier = infoFichier.Length;

            if (poidsFichier > seuilPoids)
            {

                //Console.WriteLine($"Le fichier JSON d�passe {seuilPoids} octets.");
                // Effectuez ici l'action que vous souhaitez, par exemple, versionnez le fichier.
                initialiseNewSystemeDeTraces(true);

            }
            else
            {
                //Console.WriteLine("Le fichier JSON est en dessous du seuil de poids.");
            }
        }
        else
        {
            //Console.WriteLine("Le fichier JSON n'existe pas.");
        }

    }
    public string GetTimeAsString()
    {
        DateTime now = DateTime.Now;
        string timeString = now.Hour.ToString("D2") + ":" +
                           now.Minute.ToString("D2") + ":" +
                           now.Second.ToString("D2");
        return timeString;
    }

}
