using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ChargementJSONStructure;
using static mainManager;

public class QCM_manager : MonoBehaviour
{
    public mainManager mM;
    public void playStepsQCM()
    {
        bool playswitch = true;
        bool enditemToBeLaunched = false;
        while (playswitch)
        {
            playswitch = false;
            switch (mM.current_step)
            {
                case -1:
                    //mM.texteCheat.text += "\nPret";
                    bool uneImageExiste = mM.executeAsset(mM.i.pret, null,true);
                    mM.executeAsset(mM.i.pret, mM.flecheReadyDisplayed,false);
                    break;
                case 0:
                    //Debug.Log("on destroy image principale");
                    if (mM.posScript.currentlyInstanciatedprefabImagePrincipale != null)
                    {
                        Destroy(mM.posScript.currentlyInstanciatedprefabImagePrincipale.gameObject);
                    }
                    if (!mM.sessionConsignebeenPlayed)
                    {
                        //mM.texteCheat.text += "\nSession";
                        mM.sessionConsignebeenPlayed = true;
                        mM.texteCheat.text += " /lancement session N°" + (mM.currentSession+1).ToString();
                        if (mM.executeAsset(mM.s.consigne, null, true))
                        {
                            //Debug.Log("on refait apparaitre image principale de session");
                            mM.posScript.currentlyInstanciatedprefabImagePrincipale.gameObject.SetActive(true);
                        } // consigne de la session (absente du déroulement)
                        if(mM.executeAsset(mM.s.consigne, mM.playSteps, false))
                        {
                            mM.oreilleListen.SetActive(true);
                            mM.oreilleListenScript.launchListen();
                        } // consigne de la session (absente du déroulement)
                    }
                    else { playswitch = true; }
                    break;
                case 1:
                    //Debug.Log("on destroy image principale");
                    mM.oreilleListen.SetActive(false);
                    if (mM.posScript.currentlyInstanciatedprefabImagePrincipale != null)
                    {
                        Destroy(mM.posScript.currentlyInstanciatedprefabImagePrincipale.gameObject);
                    }
                    if (mM.executeAsset(mM.e.fond, null, true,true))
                    {
                        //Debug.Log("on refait apparaitre image principale d'epreuve");
                        if (mM.posScript.currentlyInstanciatedImageEpreuveCentered != null)
                        {
                            mM.posScript.currentlyInstanciatedImageEpreuveCentered.gameObject.SetActive(true);

                        }
                    } // consigne de l'épreuve (absente du déroulement)
                    if (!mM.epreuveConsignebeenPlayed)
                    {
                        //Debug.Log("on joue la consigne d'epreuve qui n'a encore jamais été jouée");

                        mM.texteCheat.text += "\nEpreuve N°"+(mM.currentEpreuve + 1).ToString() + "/" + mM.s.epreuves.Count.ToString() + " id=" + mM.e.id;
                        mM.epreuveConsignebeenPlayed = true;
                        if(mM.executeAsset(mM.e.consigne, mM.playSteps, false))
                        {
                            mM.oreilleListen.SetActive(true);
                            mM.oreilleListenScript.launchListen();
                        }
                        // consigne de l'épreuve (absente du déroulement)
                    }
                    else { playswitch = true; }
                    break;
                case 2:
                    mM.oreilleListen.SetActive(false);
                    mM.texteCheat.text += "\nItem N°"+(mM.currentItem+1).ToString() +"/"+mM.e.items.Count.ToString()+" id=" + mM.i.id + " type=" + mM.i.type;
                    if(mM.executeAsset(mM.i.fond, null, true))
                    {
                        mM.posScript.currentlyInstanciatedprefabImagePrincipale.gameObject.SetActive(true);
                    }
                    mM.executeAsset(mM.i.consigne, mM.playSteps,false); // consigne de l'item (présent dans le déroulement)
                    if (mM.list_choix_associed_with_prefabs != null)
                    {
                        mM.list_choix_associed_with_prefabs.Clear();
                    }
                    mM.list_choix_associed_with_prefabs = new List<groupemementChoixEtPrefabBoutonQCM>();
                    foreach (Choix ccc in mM.i.choix)
                    {
                        if (((ccc.id != null && ccc.id.Length > 0) || (ccc.son != null && ccc.son.Length > 0)) && (ccc.image != null && mM.jsonStructureScript.checkifitemexists(ccc.image, true)))
                        {
                            groupemementChoixEtPrefabBoutonQCM q = new groupemementChoixEtPrefabBoutonQCM();
                            q.choix = ccc;
                            mM.list_choix_associed_with_prefabs.Add(q);

                        }
                        else
                        {
                            // choix pas elligible car pas de string son
                        }

                    }
                    if (mM.list_choix_associed_with_prefabs.Count > 0)
                    {
                        if (mM.i.randomiser)
                        {
                            mM.list_choix_associed_with_prefabs.Sort((a, b) => UnityEngine.Random.Range(-1, 2));
                        }
                        mM.posScript.CalculateAlignment(mM.list_choix_associed_with_prefabs);
                        mM.instancieAllChoixAtOnce();
                    }
                    else
                    {
                        //Debug.Log("QCM error no image to display this item, lets go to the next one");
                        mM.current_step = 10;
                    }
                    break;
                case 3: // instancition + Liste CHOIX (en QCM) OU TOUCHER LES CHOIX (en GD tuto seulement !)
                    mM.makeEachChoiceClignoteOneAfterTheOther();
                    break;
                case 4: // EXPLICATION (présent dans le déroulement)
                    if (mM.i.type == "tuto" && mM.i.explication != null && mM.i.explication.Length > 0)
                    {
                        if (mM.i.surlignerExplication >= 0.0f)
                        {
                            foreach (groupemementChoixEtPrefabBoutonQCM b in mM.list_choix_associed_with_prefabs)
                            {
                                if (b.choix.correct)
                                {
                                    //Debug.Log("choix correct explication doit clignoter i.surlignerExplication=" + mM.i.surlignerExplication);
                                    b.boutonqcmAssocied.clignote(mM.i.surlignerExplication);
                                }
                            }
                        }
                        mM.executeAsset(mM.i.explication, mM.playSteps, false);
                        // plus on fait clignoter le bon resultat
                    }
                    else { playswitch = true; }
                    break;
                case 5: // MISE EN ACTION  (présent dans le déroulement)
                    mM.user_dureeActionOnChoix = 0.0f;
                    mM.user_choix = "";
                    mM.scoreItem = -1;
                    mM.user_choixPossibles = new string[mM.i.choix.Count];
                    for (int indexi = 0; indexi < mM.i.choix.Count; indexi++)
                    {
                        if (mM.i.choix[indexi].id != null)
                        {
                            mM.user_choixPossibles[indexi] = mM.i.choix[indexi].id;

                        }
                        else if (mM.i.choix[indexi].son!=null)
                        {
                            mM.user_choixPossibles[indexi] = mM.i.choix[indexi].son;
                        }
                    }
                    if (mM.i.type == "tuto" && mM.i.miseEnAction != null && mM.i.miseEnAction.Length > 0)
                    { mM.executeAsset(mM.i.miseEnAction, mM.playerturnnow,false); }
                    else { mM.playerturnnow(); }
                    break;
                case 6:
                    mM.user_dureeActionOnChoix = Time.time - mM.actionUtilisateurStartTimeItem;
                    // action utilisateur vient d'avoir lieu
                    playswitch = true;
                    break;
                case 7:
                    if ((mM.i.type == "tuto" || mM.i.type == "training") && mM.c.feedback != null && mM.c.feedback.Length > 0)
                    { mM.executeAsset(mM.c.feedback, mM.playSteps,false); }
                    else { playswitch = true; }
                    break;
                case 8:
                    // Mise à jour score + stockage de la réponse
                    mM.newSystemeDeTraces.writeNewTrace_internal();
                    playswitch = true;
                    break;
                case 9:
                    // Explication
                    if (mM.i.type == "training" && mM.i.explication != null && mM.i.explication.Length > 0)
                    {
                        if (mM.i.surlignerExplication >= 0.0f)
                        {
                            foreach (groupemementChoixEtPrefabBoutonQCM b in mM.list_choix_associed_with_prefabs)
                            {
                                if (b.choix.correct)
                                {
                                    //Debug.Log("choix correct explication doit clignoter i.surlignerExplication=" + mM.i.surlignerExplication);
                                    b.boutonqcmAssocied.clignote(mM.i.surlignerExplication);
                                }
                            }
                        }
                        mM.executeAsset(mM.i.explication, mM.playSteps, false);
                        // plus on fait clignoter le bon resultat
                    }
                    else { playswitch = true; }
                    break;
                case 10: // transition le son est manquant actuellement
                    mM.executeAsset(mM.i.transition, mM.playSteps,false);
                    break;
                case 11: // transition le son est manquant actuellement
                    enditemToBeLaunched = true;
                    break;

                default: break;

            }
            mM.current_step++;
            //mM.texteCheat.text += "/" + mM.current_step;
            if (enditemToBeLaunched)
            {
                playswitch = false;
                mM.endItem();
            }
        }
    }

    public void QCM_objectClicked(ChargementJSONStructure.Choix c1)
    {

        mM.c = c1;
        mM.playerTurn = false;

        if (mM.c.correct)
        {
            mM.scoreItem = 1;
        }
        else
        {
            mM.scoreItem = 0;
        }
        //Debug.Log("mM.c.id=" + mM.c.id+ " mM.c.son="+ mM.c.son);
        if (mM.c.id != null && mM.c.id.Length>0)
        {
            mM.user_choix = mM.c.id;
        }
        else if (mM.c.son != null)
        {
            mM.user_choix = mM.c.son;
        }
        foreach (groupemementChoixEtPrefabBoutonQCM cc in mM.list_choix_associed_with_prefabs)
        {
            Destroy(cc.boutonqcmAssocied.gameobj);
        }
        mM.list_choix_associed_with_prefabs.Clear();
        //Debug.Log("object clicked");
        mM.playSteps(); // on lance le feedback
    }

}
