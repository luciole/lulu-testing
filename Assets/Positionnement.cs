using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using static mainManager;

public class Positionnement : MonoBehaviour
{
    public Camera mainCamera;
    public Transform placeWhereToInstantiateThings;
    public mainManager mM;
    private float spriteSpacingPercentage = 0.05f; // 5% spacing
    private Vector2 initialPosition;
    private float spriteWidthBouton;
    private float screenWidth;
    private float screenHeight;
    public GameObject prefabImagePrincipale;
    public GameObject prefabImageEpreuveCentered;
    public GameObject prefabBoutonQCM;
    public GameObject prefabDraggableGD;
    public GameObject prefabDropZoneGD;
    public imageCentraleScript currentlyInstanciatedprefabImagePrincipale;
    public AnimationCurve animationCurveSmoothStartEnd;


    // GD DropZones :
    private Vector2 initialPositionDropZones;
    private float spriteWidthDropZones;

    public GameObject currentlyInstanciatedImageEpreuveCentered;
    private float longueurDeLenfiladeDropZones;
    private float longueurDeLenfiladeQCM;
    public void CalculateAlignment(List<groupemementChoixEtPrefabBoutonQCM> listeDeChoix)
    {
        screenHeight = mainCamera.orthographicSize * 2.0f;
        screenWidth = screenHeight * mainCamera.aspect;

        // Calculate the maximum allowable sprite height
        float maxSpriteHeight = screenHeight * 0.2f; // 20% of the screen height

        // Calculate the width of the sprites to make them have the same size
        spriteWidthBouton = (screenWidth - (screenWidth*spriteSpacingPercentage * (listeDeChoix.Count - 1))) / listeDeChoix.Count;

        // Check if the sprite height exceeds the maximum allowed height
        if (spriteWidthBouton > maxSpriteHeight)
        {
            spriteWidthBouton = maxSpriteHeight; // Set the sprite height to the maximum allowed height
        }

        // Calculate the initial X position to center the alignment
        longueurDeLenfiladeQCM = (spriteWidthBouton * listeDeChoix.Count) + (screenWidth*spriteSpacingPercentage * (listeDeChoix.Count - 1));
        float startX = -screenWidth / 2.0f + (screenWidth - longueurDeLenfiladeQCM) / 2.0f+ spriteWidthBouton/2.0f;

        // Calculate the targetHeight using the corrected formula
        float targetHeight = -screenHeight / 2.0f + screenHeight * 0.1f + spriteWidthBouton / 2.0f; // on veut que la ligne se trouve � 10% du bas de l'�cran

        initialPosition = new Vector2(startX, targetHeight);


        if (mM.e.modalite == "GD")
        {

            int nbDeGD = 0;
            foreach(groupemementChoixEtPrefabBoutonQCM gcp in listeDeChoix)
            {
                if (gcp.choix.numZone > 0)
                {
                    nbDeGD++;
                }
            }

            // Calculate the maximum allowable sprite height
            float maxSpriteHeightDropZone = screenHeight * 0.25f; // 50% of the screen height

            // Calculate the width of the sprites to make them have the same size
            spriteWidthDropZones = (screenWidth - (screenWidth * spriteSpacingPercentage * (nbDeGD - 1))) / nbDeGD;

            // Check if the sprite height exceeds the maximum allowed height
            if (spriteWidthDropZones > maxSpriteHeightDropZone)
            {
                spriteWidthDropZones = maxSpriteHeightDropZone; // Set the sprite height to the maximum allowed height
            }
            // Calculate the initial X position to center the alignment
            longueurDeLenfiladeDropZones = (spriteWidthDropZones * nbDeGD) + (screenWidth * spriteSpacingPercentage * (nbDeGD - 1));
            float startXDropZones = -screenWidth / 2.0f + (screenWidth - longueurDeLenfiladeDropZones) / 2.0f + spriteWidthDropZones / 2.0f;
            Debug.Log("longueurDeLenfiladeDropZones here = " + longueurDeLenfiladeDropZones);

            // Calculate the targetHeight using the corrected formula
            float targetHeightDropZones = -screenHeight / 2.0f + screenHeight * 0.5f + spriteWidthDropZones / 2.0f; // on veut que la ligne se trouve � 10% du bas de l'�cran

            initialPositionDropZones = new Vector2(startXDropZones, targetHeightDropZones);
        }

    }
    public void SetSpriteAndPosition(Sprite sprite)
    {
        if (sprite == null)
        {
            Debug.LogWarning("Le sprite ou le SpriteRenderer est manquant.");
            return;
        }
        if (currentlyInstanciatedprefabImagePrincipale != null)
        {
            Destroy(currentlyInstanciatedprefabImagePrincipale.gameObject);
        }
        currentlyInstanciatedprefabImagePrincipale = Instantiate(prefabImagePrincipale, placeWhereToInstantiateThings).GetComponent<imageCentraleScript>();
        currentlyInstanciatedprefabImagePrincipale.mM = mM;
        SpriteRenderer imagePrincipaleSpriteRenderer = currentlyInstanciatedprefabImagePrincipale.gameObject.GetComponent<SpriteRenderer>();
        imagePrincipaleSpriteRenderer.sprite = sprite;
        imagePrincipaleSpriteRenderer.gameObject.GetComponent<BoxCollider2D>().size = imagePrincipaleSpriteRenderer.sprite.bounds.size;
        // Ajustez la position du sprite pour remplir la partie sup�rieure de l'�cran
        screenHeight = mainCamera.orthographicSize * 2.0f;
        screenWidth = screenHeight * mainCamera.aspect;
        float desiredYPosition = -screenHeight / 2.0f + screenHeight * 0.5f; // 30% du bas de l'�cran
        if (mM.current_step >= 2)
        {
            desiredYPosition = -screenHeight / 2.0f + screenHeight * 0.7f; // 30% du bas de l'�cran
        }
        // Obtenez la hauteur du sprite
        float spriteHeight = imagePrincipaleSpriteRenderer.bounds.size.y;
        // Obtenez la largeur du sprite
        float spriteWidth = imagePrincipaleSpriteRenderer.bounds.size.x;
        //Debug.Log("spriteHeight=" + spriteHeight + "/  spriteWidth=" + spriteWidth);
        float scaleFactorwidth = screenWidth / spriteWidth;
        // Calculez le facteur d'�chelle pour la hauteur
        float scaleFactorHeight = (screenHeight * 0.5f) / spriteHeight;
        if (scaleFactorHeight > scaleFactorwidth)
        {
            scaleFactorHeight = scaleFactorwidth;
        }
        // Calculez la position Y en fonction de la nouvelle �chelle
        Vector3 newPosition = new Vector3(0.0f, desiredYPosition, 1);
        // Appliquez la nouvelle position
        imagePrincipaleSpriteRenderer.transform.position = newPosition;
        //Debug.Log("scaleFactorHeight=" + scaleFactorHeight);
        imagePrincipaleSpriteRenderer.transform.localScale = new Vector3(scaleFactorHeight, scaleFactorHeight, 1.0f);
    }

    public void SetImageEpreuve(Sprite sprite)
    {
        if (sprite == null)
        {
            Debug.LogWarning("Le sprite ou le SpriteRenderer est manquant.");
            return;
        }
        if (currentlyInstanciatedImageEpreuveCentered != null)
        {
            Destroy(currentlyInstanciatedImageEpreuveCentered.gameObject);
        }
        currentlyInstanciatedImageEpreuveCentered = Instantiate(prefabImageEpreuveCentered, placeWhereToInstantiateThings).gameObject;
        SpriteRenderer imageEpreuveSpriteRenderer = currentlyInstanciatedImageEpreuveCentered.gameObject.GetComponent<SpriteRenderer>();
        imageEpreuveSpriteRenderer.sprite = sprite;
        // Ajustez la position du sprite pour remplir la partie sup�rieure de l'�cran
        screenHeight = mainCamera.orthographicSize * 2.0f;
        screenWidth = screenHeight * mainCamera.aspect;
        float desiredYPosition = -screenHeight / 2.0f + screenHeight * 0.5f; // 30% du bas de l'�cran
       
        // Obtenez la hauteur du sprite
        float spriteHeight = imageEpreuveSpriteRenderer.bounds.size.y;
        // Obtenez la largeur du sprite
        float spriteWidth = imageEpreuveSpriteRenderer.bounds.size.x;
        float scaleFactorwidth = screenWidth / spriteWidth;
        // Calculez le facteur d'�chelle pour la hauteur
        float scaleFactorHeight = (screenHeight * 1.0f) / spriteHeight;
        if (scaleFactorHeight > scaleFactorwidth)
        {
            scaleFactorHeight = scaleFactorwidth;
        }
        // Calculez la position Y en fonction de la nouvelle �chelle
        Vector3 newPosition = new Vector3(0.0f, desiredYPosition, 0);
        // Appliquez la nouvelle position
        imageEpreuveSpriteRenderer.transform.position = newPosition;
        imageEpreuveSpriteRenderer.transform.localScale = new Vector3(scaleFactorHeight, scaleFactorHeight, 1.0f);
    }


    public GameObject placeImages(int indexBouton, ChargementJSONStructure.Choix choix, bool dropzone = false)
    {
        boutonQCM bt = new boutonQCM();
        if (mM.e.modalite == "GD")
        {
            bt = new elementGD();
        }
        if(choix!=null && choix.image!=null && choix.image.Length > 0)
        {
            ChargementJSONStructure.Asset a = mM.returnA(choix.image);
            if (a != null)
            {
                if (a.img_path != null && a.img_path.Length > 0)
                {
                    Sprite image = Resources.Load<Sprite>(a.img_path);
                    if (image != null)
                    {
                        if (mM.e.modalite == "GD")
                        {
                            Debug.Log("dropzone=" + dropzone);
                            if (dropzone)
                            {
                                bt = Instantiate(prefabDropZoneGD, placeWhereToInstantiateThings).gameObject.GetComponent<dropZoneGD>();
                                mM.gdmanager.sequenceDeDropZones.Add(bt.gameObject.GetComponent<dropZoneGD>());
                            }
                            else
                            {
                                bt = Instantiate(prefabDraggableGD, placeWhereToInstantiateThings).gameObject.GetComponent<elementGD>();
                                
                            }
                        }
                        else
                        {
                            bt = Instantiate(prefabBoutonQCM, placeWhereToInstantiateThings).gameObject.GetComponent<boutonQCM>();
                        }
                        bt.spriterenderer.sprite = image;
                        bt.boxcoll.size = bt.spriterenderer.bounds.size;
                        bt.choix = choix;
                        bt.mM = mM;
                        Transform spriteTransform = bt.trans;
                        if (dropzone)
                        {
                            if(choix.imgZone!=null && choix.imgZone.Length > 0)
                            {
                                a = mM.returnA(choix.imgZone);
                                if (a != null)
                                {
                                    image = Resources.Load<Sprite>(a.img_path);
                                    bt.spriterenderer.sprite = image;
                                }
                               
                                
                            }
                            bt.boxcoll.size = bt.spriterenderer.bounds.size;
                            int emplacement = choix.numZone - 1;
                            spriteTransform.position = new Vector3(
                            initialPositionDropZones.x + (spriteSpacingPercentage * screenWidth + spriteWidthDropZones) * emplacement,
                            initialPositionDropZones.y,
                            spriteTransform.position.z);
                            // Set the sprite's local scale to match the calculated width
                            float newScaleX = spriteWidthDropZones / bt.spriterenderer.bounds.size.x;
                            spriteTransform.localScale = new Vector3(newScaleX, newScaleX, 1.0f);
                            bt.GetComponent<dropZoneGD>().textmesh.transform.localScale = bt.spriterenderer.bounds.size;
                            bt.GetComponent<dropZoneGD>().textmesh.text = choix.numZone.ToString();
                        }
                        else
                        {
                            
                            bt.spriterenderer.sprite = image;
                            bt.boxcoll.size = bt.spriterenderer.bounds.size;
                            if (mM.e.modalite == "QCM" && (mM.posScript.currentlyInstanciatedprefabImagePrincipale == null || !mM.posScript.currentlyInstanciatedprefabImagePrincipale.gameObject.activeSelf))
                            {
                                //Debug.Log("heeeeeeeeeeeeeeeeeeeerrrre longueurDeLenfiladeQCM=" + longueurDeLenfiladeQCM);
                                int multiplieurHeight = DetermineParite(indexBouton);
                                int multiplieurX = DetermineValeur(indexBouton);
                                spriteTransform.position = new Vector3(
                               (spriteWidthBouton+ spriteSpacingPercentage*2.0f) * DetermineValeur(indexBouton)- (spriteWidthBouton + spriteSpacingPercentage*2.0f)* GetClosestGreaterEven(mM.list_choix_associed_with_prefabs.Count)/2.0f/2.0f+ spriteWidthBouton/2.0f,
                               -screenHeight/2.0f+ screenHeight*0.5f + multiplieurHeight * (spriteSpacingPercentage*3.0f+ spriteWidthBouton/2.0f),
                               spriteTransform.position.z);
                            }
                            else
                            {
                                //Debug.Log("lllllllllllllll");
                                spriteTransform.position = new Vector3(
                                initialPosition.x + (spriteSpacingPercentage * screenWidth + spriteWidthBouton) * indexBouton,
                                initialPosition.y,
                                spriteTransform.position.z);
                            }
                            
                            // Set the sprite's local scale to match the calculated width
                            float newScaleX = spriteWidthBouton / bt.spriterenderer.bounds.size.x;
                            spriteTransform.localScale = new Vector3(newScaleX, newScaleX, 1.0f);
                        }
                    }
                }
            }
        }
        
        return bt.gameObject;
    }

    public static int DetermineParite(int nombre)
    {
        if (nombre % 2 == 0)
        {
            return 1; // Renvoie 1 pour les nombres pairs
        }
        else
        {
            return -1; // Renvoie -1 pour les nombres impairs
        }
    }

  

    public static int DetermineValeur(int nombre)
    {
        if (nombre == 0 || nombre == 1)
        {
            return 0;
        }
        else if (nombre == 2 || nombre == 3)
        {
            return 1;
        }
        else if (nombre == 4 || nombre == 5)
        {
            return 2;
        }
        else if (nombre == 6 || nombre == 7)
        {
            return 3;
        }
        else if (nombre == 8 || nombre == 9)
        {
            return 4;
        }
        else if (nombre == 9 || nombre == 10)
        {
            return 5;
        }
        // Ajoutez autant de conditions que n�cessaire pour couvrir vos intervalles
        else
        {
            return -1; // Si le nombre ne correspond � aucun intervalle, retourne -1 (ou une valeur par d�faut de votre choix).
        }
    }

        public int GetClosestGreaterEven(int inputNumber)
        {
            // Si l'entier d'entr�e est d�j� pair, le renvoyer tel quel.
            if (inputNumber % 2 == 0)
            {
                return inputNumber;
            }
            else
            {
                // Sinon, retourner le prochain entier pair en ajoutant 1.
                return inputNumber + 1;
            }
        }
    


}
