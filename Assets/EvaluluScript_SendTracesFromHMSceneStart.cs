using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

public class EvaluluScript_SendTracesFromHMSceneStart : MonoBehaviour
{
    // Start is called before the first frame update
    public void CallbackSynchroEnd(List<string> allAccount, List<string> successSynchroAccount, List<string> errorSynchroAccount)
    {
        uploadStaticMechanism.sendTraces(true,successSynchroAccount,false);  
    }
}
