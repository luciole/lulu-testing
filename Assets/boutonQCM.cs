using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static mainManager;

public class boutonQCM : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject gameobj;
    public Transform trans;
    public SpriteRenderer spriterenderer;
    public BoxCollider2D boxcoll;
    private Coroutine clignoteCoroute;
    public bool currentlyBlinking = false;
    public mainManager mM;
    public ChargementJSONStructure.Choix choix;

    private void OnMouseDown()
    {
        if (mM.playerTurn && mM.e.modalite == "QCM" && !mM.gameObjFin.activeSelf)
        {
            mM.qcmmanager.QCM_objectClicked(choix);

        }
    }

    public void clignote(float afterthisdelay=0.0f, bool withoutstop = false, float dureeclignotement=0.0f)
    {
        if (clignoteCoroute != null)
        {
            StopCoroutine(clignoteCoroute);
        }
        clignoteCoroute =StartCoroutine(BlinkContinuously(afterthisdelay, withoutstop, dureeclignotement));
    }

    public void stopClignote()
    {
        currentlyBlinking = false;
        if (clignoteCoroute != null)
        {
            StopCoroutine(clignoteCoroute);
        }
        spriterenderer.color = Color.white;

    }
    private IEnumerator BlinkContinuously(float afterThisDelay = 0.0f, bool withoutStop=false, float dureeclignotement = 0.0f)
    {
        currentlyBlinking = true;
        Debug.Log("BEFORE BlinkContinuously lanuched after a wait of " + afterThisDelay + " dureeclignotement=" + dureeclignotement);
        yield return new WaitForSeconds(afterThisDelay);
        Debug.Log("AFTER BlinkContinuously lanuched after a wait of " + afterThisDelay+ " dureeclignotement="+ dureeclignotement);
        elementGD elementGD = this.gameobj.GetComponent<elementGD>();
        if (elementGD != null)
        {
            if (elementGD.dropZOneWhereItsCurrentlyPlaced != null)
            {
                if (elementGD.dropZOneWhereItsCurrentlyPlaced.bienrepondu)
                {
                    elementGD.dropZOneWhereItsCurrentlyPlaced.spriterenderer.color = Color.green;
                }
                else
                {
                    elementGD.dropZOneWhereItsCurrentlyPlaced.spriterenderer.color = Color.red;
                }
                
            }
            
        }

        int nbClignoting = 0;
       
        float blinkDuration = 0.3f;
        float dureepassee = 0.0f;
        while (true)
        {
            // Passez en alpha 0
            spriterenderer.color = new Color(spriterenderer.color.r, spriterenderer.color.g, spriterenderer.color.b, 0.0f);

            // Attendez la moiti� de la dur�e de clignotement
            yield return new WaitForSeconds(blinkDuration * 0.5f);

            // Passez en alpha 1
            spriterenderer.color = new Color(spriterenderer.color.r, spriterenderer.color.g, spriterenderer.color.b, 1.0f);
            dureepassee += Time.deltaTime;
            // Attendez la moiti� de la dur�e de clignotement
            yield return new WaitForSeconds(blinkDuration * 0.5f);

            nbClignoting++;
            if (nbClignoting > 6 && !withoutStop && (dureeclignotement==0.0f || dureepassee <= dureeclignotement))
            {
                break;
            }
        }
        spriterenderer.color = new Color(spriterenderer.color.r, spriterenderer.color.g, spriterenderer.color.b, 1.0f);
        currentlyBlinking = false;
        if (elementGD != null)
        {
            if (elementGD.dropZOneWhereItsCurrentlyPlaced != null)
            {
                
               elementGD.dropZOneWhereItsCurrentlyPlaced.spriterenderer.color = Color.white;
                

            }

        }
    }
}
