using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HM_Scripts
{
    public static HM_PluginController pluginController;
    public static HM_OptionPanelController optionPanel;
    public static HM_DebugPanel debugPanel;

    public static HM_ContentCode contentCodeChild;
    public static HM_ContentCode contentCodeTeacher;
    public static HM_ContentBonjour contentBonjour;
    public static HM_ContentDeviceColor contentDeviceColor;
    public static HM_ContentGroupsChoice contentGroupsChoice;
    public static HM_ContentProfil contentProfil;
    public static HM_ContentTeacherAccount contentTeacherAccount;
    public static HM_ContentLoginAWS contentLoginAWS;
    public static HM_ContentSettingApp_Elargir contentSettingApp_Elargir;
    public static HM_ContentSettingApp_Evasion contentSettingApp_Evasion;
    public static HM_ContentSettingApp_Luciole contentSettingApp_Luciole;
    public static HM_ContentSynchroBluetooth contentSynchroBluetooth;
    public static HM_ContentSynchroAWS contentSynchroAWS;
    public static HM_ContentRefreshAWS contentRefreshAWS;
    public static HM_ContentConfig contentConfig;

    public static HM_PopupAlertDissocier popupAlertDissocier;
    public static HM_PopupAfterDissocier popupAfterDissocier;
    public static HM_PopupAlertDisconnect popupAlertDisconnect;
    public static HM_PopupSynchro popupSynchro;
    public static HM_PopupSynchroInfo popupSynchroInfo;
    public static HM_PopupLotInfo popupLotInfo;
    public static HM_PopupWifiAlert popupWifiAlert;

    public static void Initialize()
    {
        pluginController = GameObject.Find("HM_Plugin")?.GetComponent<HM_PluginController>();
        optionPanel = GameObject.Find("HM_OptionPanel")?.GetComponent<HM_OptionPanelController>();
        debugPanel = GameObject.Find("HM_DebugPanel")?.GetComponent<HM_DebugPanel>();

        contentCodeChild = GameObject.Find("HM_ContentCodeChild")?.GetComponent<HM_ContentCode>();
        contentCodeTeacher = GameObject.Find("HM_ContentCodeTeacher")?.GetComponent<HM_ContentCode>();
        contentBonjour = GameObject.Find("HM_ContentBonjour")?.GetComponent<HM_ContentBonjour>();
        contentDeviceColor = GameObject.Find("HM_ContentDeviceColor")?.GetComponent<HM_ContentDeviceColor>();
        contentGroupsChoice = GameObject.Find("HM_ContentGroupsChoice")?.GetComponent<HM_ContentGroupsChoice>();
        contentProfil = GameObject.Find("HM_ContentProfil")?.GetComponent<HM_ContentProfil>();
        contentTeacherAccount = GameObject.Find("HM_ContentTeacherAccount")?.GetComponent<HM_ContentTeacherAccount>();
        contentLoginAWS = GameObject.Find("HM_ContentLoginAWS")?.GetComponent<HM_ContentLoginAWS>();
        contentSettingApp_Elargir = GameObject.Find("HM_ContentSettingsApp_Elargir")?.GetComponent<HM_ContentSettingApp_Elargir>();
        contentSettingApp_Evasion = GameObject.Find("HM_ContentSettingsApp_Evasion")?.GetComponent<HM_ContentSettingApp_Evasion>();
        contentSettingApp_Luciole = GameObject.Find("HM_ContentSettingsApp_Luciole")?.GetComponent<HM_ContentSettingApp_Luciole>();
        contentSynchroBluetooth = GameObject.Find("HM_ContentSynchroBluetooth")?.GetComponent<HM_ContentSynchroBluetooth>();
        contentSynchroAWS = GameObject.Find("HM_ContentSynchroAWS")?.GetComponent<HM_ContentSynchroAWS>();
        contentRefreshAWS = GameObject.Find("HM_ContentRefreshAWS")?.GetComponent<HM_ContentRefreshAWS>();
        contentConfig = GameObject.Find("HM_ContentConfig")?.GetComponent<HM_ContentConfig>();

        popupAlertDissocier = GameObject.Find("HM_PopupAlertDissocier")?.GetComponent<HM_PopupAlertDissocier>();
        popupAfterDissocier = GameObject.Find("HM_PopupAfterDissocier")?.GetComponent<HM_PopupAfterDissocier>();
        popupAlertDisconnect = GameObject.Find("HM_PopupAlertDisconnect")?.GetComponent<HM_PopupAlertDisconnect>();
        popupSynchro = GameObject.Find("HM_PopupSynchro")?.GetComponent<HM_PopupSynchro>();
        popupSynchroInfo = GameObject.Find("HM_PopupSynchroInfo")?.GetComponent<HM_PopupSynchroInfo>();
        popupLotInfo = GameObject.Find("HM_PopupLotInfo")?.GetComponent<HM_PopupLotInfo>();
        popupWifiAlert = GameObject.Find("HM_PopupWifiAlert")?.GetComponent<HM_PopupWifiAlert>();
    }
}
