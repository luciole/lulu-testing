using Shatalmic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.IO.Compression;
using System;
using Amazon.CognitoIdentityProvider;
using System.Net;
using Amazon.CognitoIdentity.Model;
using Amazon.Extensions.CognitoAuthentication;
using UnityEngine.Networking;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Linq;
using UnityEngine.SceneManagement;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System.Threading;

public struct HM_ReturnInfo
{
    public int code;
    public string message;
}

public class HM_NetworkController : MonoBehaviour
{
    public enum ENV
    {
        DEVELOP,
        PREPROD,
        PROD
    }

    public static HM_NetworkController instance = null;

    private string deviceID = "";

    public static int majorVersionCurrent = -1;
    public static int minorVersionCurrent = -1;
    public static int patchVersionCurrent = -1;
    public static int majorVersionMin = -1;
    public static int minorVersionMin = -1;
    public static int patchVersionMin = -1;
    public static bool badVersion = false;

    public static string backURL_Develop = "https://dev.api.trans3.school.humansmatter.co/";
    public static string backURL_Preprod = "https://staging.api.trans3.school.humansmatter.co/";
    public static string backURL_Prod = "https://api.trans3.school.humansmatter.co/";

    public static Amazon.RegionEndpoint Region = Amazon.RegionEndpoint.EUWest3;

    public const string IdentityPool_Develop = "eu-west-3:88272d17-e6d4-42a2-ba45-2baadd1d1891";
    public const string AppClientID_Develop = "5m8j3natra96jop5k1ljff2e2u";
    public const string userPoolId_Develop = "eu-west-3_MpfAR4np6";
    public const string accessKeyS3_Develop = "AKIATYE5JLJKE4KNKJGR";
    public const string secretKeyS3_Develop = "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";
    public const string bucketNameS3_Develop = "dev-trans3-backend-storage";

    public const string IdentityPool_Preprod = "eu-west-3:900ac551-914a-4705-9ba5-92725a8442b3";
    public const string AppClientID_Preprod = "7tldm37gmb6j54s9u7a3nlo0sq";
    public const string userPoolId_Preprod = "eu-west-3_u5WVrOVhv";
    public const string accessKeyS3_Preprod = "AKIA4EMYFFZ3KHIJ6MMQ";
    public const string secretKeyS3_Preprod = "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";
    public const string bucketNameS3_Preprod = "staging-trans3-backend-storage";

    public const string IdentityPool_Prod = "eu-west-3:9366a901-7f88-4b71-a01e-73cb35e05c30";
    public const string AppClientID_Prod = "2shn6qt37ce69qtotr188upcu4";
    public const string userPoolId_Prod = "eu-west-3_m3ERjnfOr";
    public const string accessKeyS3_Prod = "AKIA6JANPCTH36AU7WVA";
    public const string secretKeyS3_Prod = "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";
    public const string bucketNameS3_Prod = "prod-trans3-backend-storage";

    public static AmazonCognitoIdentityProviderClient providerAWS;
    //public static string sessionAWS;

    [HideInInspector]
    public static ENV currentENV = ENV.PROD;

    public ENV debugEnv = ENV.PREPROD;

    public static string backURL = backURL_Prod;
    public static string IdentityPool = IdentityPool_Prod;
    public static string AppClientID = AppClientID_Prod;
    public static string userPoolId = userPoolId_Prod;
    public static string accessKeyS3 = accessKeyS3_Prod;
    public static string secretKeyS3 = secretKeyS3_Prod;
    public static string bucketNameS3 = bucketNameS3_Prod;

    //public static HM_DataAWS data_aws = new HM_DataAWS();

    private static float timeLastCheck = -1;

    private System.Action<bool> refreshCallback = null;
    private bool forceRefresh = false;

    private static AmazonS3Client S3Client = null;


    void Awake()
    {

        instance = this;

        if(timeLastCheck == -1)
        {
            timeLastCheck = Time.realtimeSinceStartup;
        }

#if UNITY_EDITOR
        switch (debugEnv)
        {
            case ENV.DEVELOP:
                currentENV = ENV.DEVELOP;
                backURL = backURL_Develop;
                IdentityPool = IdentityPool_Develop;
                AppClientID = AppClientID_Develop;
                userPoolId = userPoolId_Develop;
                accessKeyS3 = accessKeyS3_Develop;
                secretKeyS3 = secretKeyS3_Develop;
                bucketNameS3 = bucketNameS3_Develop;
                break;
            case ENV.PREPROD:
                currentENV = ENV.PREPROD;
                backURL = backURL_Preprod;
                IdentityPool = IdentityPool_Preprod;
                AppClientID = AppClientID_Preprod;
                userPoolId = userPoolId_Preprod;
                accessKeyS3 = accessKeyS3_Preprod;
                secretKeyS3 = secretKeyS3_Preprod;
                bucketNameS3 = bucketNameS3_Preprod;
                break;
            case ENV.PROD:
                currentENV = ENV.PROD;
                backURL = backURL_Prod;
                IdentityPool = IdentityPool_Prod;
                AppClientID = AppClientID_Prod;
                userPoolId = userPoolId_Prod;
                accessKeyS3 = accessKeyS3_Prod;
                secretKeyS3 = secretKeyS3_Prod;
                bucketNameS3 = bucketNameS3_Prod;
                break;
        }
#else
        string envSave = PlayerPrefs.GetString("HM_ENV", "");

        if (envSave.Length != 0)
        {
            if (envSave.Equals("DEVELOP"))
            {
                debugEnv = ENV.DEVELOP;
                currentENV = ENV.DEVELOP;
                backURL = backURL_Develop;
                IdentityPool = IdentityPool_Develop;
                AppClientID = AppClientID_Develop;
                userPoolId = userPoolId_Develop;
            }
            else if (envSave.Equals("PREPROD"))
            {
                debugEnv = ENV.PREPROD;
                currentENV = ENV.PREPROD;
                backURL = backURL_Preprod;
                IdentityPool = IdentityPool_Preprod;
                AppClientID = AppClientID_Preprod;
                userPoolId = userPoolId_Preprod;
            }
            else if (envSave.Equals("PROD"))
            {
                debugEnv = ENV.PROD;
                currentENV = ENV.PROD;
                backURL = backURL_Prod;
                IdentityPool = IdentityPool_Prod;
                AppClientID = AppClientID_Prod;
                userPoolId = userPoolId_Prod;
            }
        }
#endif

        switch (currentENV)
        {
            case ENV.DEVELOP:
                Debug.Log("Environnement branch� : DEVELOP");
                PlayerPrefs.SetString("HM_ENV", "DEVELOP");
                break;
            case ENV.PREPROD:
                Debug.Log("Environnement branch� : PREPROD");
                PlayerPrefs.SetString("HM_ENV", "PREPROD");
                break;
            case ENV.PROD:
                Debug.Log("Environnement branch� : PROD");
                PlayerPrefs.SetString("HM_ENV", "PROD");
                break;
        }

        //Debug.Log(accessKeyS3);
        //Debug.Log(secretKeyS3);

        if (S3Client == null && accessKeyS3 != null && secretKeyS3 != null)
        {
            S3Client = new AmazonS3Client(accessKeyS3, secretKeyS3, RegionEndpoint.EUWest3);
        }

        /*test audio upload s3
        if(Application.internetReachability != NetworkReachability.NotReachable)
        {
            List<HM_DataAudio> audioPending = new List<HM_DataAudio>();
            HM_DataAudio da = new HM_DataAudio();
            da.localPath = "Assets/TRANS3_PluginHM/Resources/audio.wav";
            da.serverPath = "audios/test/audiotest5.wav";
            audioPending.Add(da);
            PlayerPrefs.SetString("HM_DataAudio", JsonConvert.SerializeObject(audioPending));
            SendAudioData();
        }*/

        /*if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            List<HM_DataAudio> audioPending = new List<HM_DataAudio>();
            HM_DataAudio da = new HM_DataAudio();
            da.localPath = "Assets/TRANS3_PluginHM/Resources/audio.wav";
            da.filename = "audio.wav";
            da.childIconID = 8;
            da.childCode = "638666";
            da.teacherCode = "MSVLWU";
            da.resultID = "MSVLWU";
            audioPending.Add(da);
            PlayerPrefs.SetString("HM_DataAudio", JsonConvert.SerializeObject(audioPending));
            SendAudioData();
        }*/

        /*test num�ro version
        if (Application.version.Split('.').Length != 3)
        {
            Debug.Log("Attention num�ro de version App non conforme : " + Application.version);
        }
        else
        {
            majorVersionCurrent = int.Parse(Application.version.Split('.')[0]);
            minorVersionCurrent = int.Parse(Application.version.Split('.')[1]);
            patchVersionCurrent = int.Parse(Application.version.Split('.')[2]);
            Debug.Log("Version de l'application : " + Application.version);
        }*/



    }

    private static List<HM_DataAudio> audioPending = null;

    public void SendAudioData()
    {
        string audioData = PlayerPrefs.GetString("HM_DataAudio", "");

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            return;
        }

        if (S3Client == null || audioData == null || audioData.Length == 0)
        {
            return;
        }

        audioPending = JsonConvert.DeserializeObject<List<HM_DataAudio>>(audioData);

        List<HM_DataAudio> audioCheck = new List<HM_DataAudio>(audioPending);

        foreach(HM_DataAudio da in audioCheck)
        {
            HM_TeacherAccount teacher = null;
            foreach (HM_TeacherAccount t in HM_DataController.model.teacherAccounts)
            {
                if (t.teacherCode.Equals(da.teacherCode))
                {
                    teacher = t;
                    break;
                }
            }
            if (teacher == null || teacher.dataAWS._idToken == null || teacher.dataAWS._idToken.Length == 0 || teacher.dataAWS.identityId == null || teacher.dataAWS.identityId.Length == 0)
            {
                continue;
            }

            //Debug.Log(teacher.dataAWS.identityId);

            //construction 

            da.serverPath = "private/"+ teacher.dataAWS.identityId + "/audios/" + da.childIconID + "/" +da.childCode +"/"+ da.resultID + "/"+ da.filename;

            //Debug.Log(da.serverPath);
            Task.Run(async () =>
            {
                //Debug.Log(da.serverPath);
                int resultCpt = await S3List(da.serverPath);

                //Debug.Log(resultCpt);
                if (resultCpt == 0)//si l'audio n'est pas pr�sent c�t� S3, on l'ajoute
                {
                    try
                    {
                        //v�rification si le fichier existe bien en local
                        using (var fileStream = File.OpenRead(da.localPath))
                        {
                            bool uploadOK = await S3Upload(da.localPath, da.serverPath);

                            if(uploadOK)
                            {
                                Debug.Log("UPLOAD AUDIO OK : "+da.serverPath);
                                //HM_Log.AppendTextToFile("HM_Log.txt", "UploadAudio OK " + da.localPath + " to " + da.serverPath);
                                audioPending.RemoveAll(x => x.serverPath.Equals(da.serverPath));
                                UnityMainThreadDispatcher.Instance().Enqueue(AudioListSave());
                            }
                            else
                            {
                                //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorUploadAudio " + da.localPath + " to " +da.serverPath);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.Log("exception "+e);
                        audioPending.RemoveAll(x => x.serverPath.Equals(da.serverPath));
                        UnityMainThreadDispatcher.Instance().Enqueue(AudioListSave());
                    }
                }
                else if(resultCpt > 0)
                {
                    audioPending.RemoveAll(x => x.serverPath.Equals(da.serverPath));
                    UnityMainThreadDispatcher.Instance().Enqueue(AudioListSave());
                }
            });
        }
    }

    public IEnumerator AudioListSave()
    {
        if(audioPending == null || audioPending.Count == 0)
        {
            PlayerPrefs.SetString("HM_DataAudio", "");
        }
        else
        {
            PlayerPrefs.SetString("HM_DataAudio", JsonConvert.SerializeObject(audioPending));
        }

        yield return true;
    }

    async Task<int> S3List(string pathServer)
    {
        if (S3Client == null || accessKeyS3 == null || secretKeyS3 == null)
        {
            return -2;
        }

        ListObjectsRequest listRequest = new ListObjectsRequest
        {
            BucketName = bucketNameS3,
            Prefix = pathServer
        };

        CancellationTokenSource cts = new CancellationTokenSource(TimeSpan.FromSeconds(5));
        CancellationToken cancelToken = cts.Token;

        try
        {
            ListObjectsResponse listResponse = await S3Client.ListObjectsAsync(listRequest, cancelToken);

            /*foreach (S3Object obj in listResponse.S3Objects)
            {
                Debug.Log("Object - " + obj.Key);
                Debug.Log(" Size - " + obj.Size);
                Debug.Log(" LastModified - " + obj.LastModified);
                Debug.Log(" Storage class - " + obj.StorageClass);
            }*/

            return listResponse.S3Objects.Count;
        }
        catch (AmazonS3Exception s3Exception)
        {
            //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorListAudio " + pathServer);
            //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorListAudio Erreur Amazon S3 : " + s3Exception.Message);
            Debug.Log("Erreur Amazon S3 : " + s3Exception.Message);
            // Traitez sp�cifiquement les erreurs li�es � Amazon S3 ici, si n�cessaire.
        }
        catch (OperationCanceledException cancelException)
        {
            //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorListAudio " + pathServer);
            //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorListAudio Annulation de la requ�te : " + cancelException.Message);
            Debug.Log("Annulation de la requ�te : " + cancelException.Message);
            // Handle cancellation exception here, if necessary.
        }
        catch (Exception e)
        {
            //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorListAudio " + pathServer);
            //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorListAudio Echec envoi, exception : " + e);
            Debug.Log("Echec envoi, exception: " + e);
        }

        return -1;

    }

    async Task<bool> S3Upload(string pathLocal, string pathServer)
    {
        if (accessKeyS3 == null || secretKeyS3 == null)
        {
            return false;
        }

        // Create a PutObject request
        PutObjectRequest request = new PutObjectRequest
        {
            BucketName = bucketNameS3,
            Key = pathServer,
            FilePath = pathLocal
        };

        CancellationTokenSource cts = new CancellationTokenSource(TimeSpan.FromSeconds(5));
        CancellationToken cancelToken = cts.Token;

        try
        {
            using (var fileStream = File.OpenRead(pathLocal))
            {
                // Put object
                PutObjectResponse response = await S3Client.PutObjectAsync(request, cancelToken);

                if (response.HttpStatusCode == HttpStatusCode.OK)
                {
                    return true;
                }

                Debug.Log("ErrorUploadAudio " + pathLocal + " to " + pathServer);
                Debug.Log("ErrorUploadAudio " + response.HttpStatusCode.ToString());
                //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorUploadAudio " + pathLocal + " to " + pathServer);
                //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorUploadAudio " + response.HttpStatusCode.ToString());

                return false;
            }
        }
        catch (AmazonS3Exception s3Exception)
        {
           //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorUploadAudio " + pathLocal + " to " + pathServer);
            //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorUploadAudio Erreur Amazon S3 : " + s3Exception.Message);
            Debug.Log("Erreur Amazon S3 : " + s3Exception.Message);
            // Traitez sp�cifiquement les erreurs li�es � Amazon S3 ici, si n�cessaire.
        }
        catch (OperationCanceledException cancelException)
        {
            //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorUploadAudio " + pathLocal + " to " + pathServer);
            //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorUploadAudio Annulation de la requ�te : " + cancelException.Message);
            Debug.Log("Annulation de la requ�te : " + cancelException.Message);
            // Handle cancellation exception here, if necessary.
        }
        catch (Exception e)
        {
            //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorUploadAudio " + pathLocal + " to " + pathServer);
            //HM_Log.AppendTextToFile("HM_Log.txt", "ErrorUploadAudio Echec envoi, exception : " + e);
            Debug.Log("Echec envoi, exception: " + e);
        }

        return false;
    }




    public async Task<HM_ReturnInfo> S3List_v2(string pathServer)
    {
        HM_ReturnInfo returnInfo = new HM_ReturnInfo();

        if (S3Client == null || accessKeyS3 == null || secretKeyS3 == null)
        {
            returnInfo.code = -101;
            returnInfo.message = "Acc�s AWS S3 non initialis�";
            return returnInfo;
        }

        ListObjectsRequest listRequest = new ListObjectsRequest
        {
            BucketName = bucketNameS3,
            Prefix = pathServer
        };

        CancellationTokenSource cts = new CancellationTokenSource(TimeSpan.FromSeconds(5));
        CancellationToken cancelToken = cts.Token;

        try
        {
            ListObjectsResponse listResponse = await S3Client.ListObjectsAsync(listRequest, cancelToken);
            returnInfo.code = listResponse.S3Objects.Count;
            returnInfo.message = "";
            return returnInfo;
        }
        catch (AmazonS3Exception s3Exception)
        {
            returnInfo.code = -102;
            returnInfo.message = "Erreur Amazon S3 : " + s3Exception.Message;
            return returnInfo;
        }
        catch (OperationCanceledException cancelException)
        {
            returnInfo.code = -103;
            returnInfo.message = "Annulation de la requ�te : " + cancelException.Message;
            return returnInfo;
        }
        catch (Exception e)
        {
            returnInfo.code = -104;
            returnInfo.message = "Echec envoi, exception: " + e;
            return returnInfo;
        }
    }


    public async Task<HM_ReturnInfo> S3Upload_v2(string pathLocal, string pathServer)
    {
        HM_ReturnInfo returnInfo = new HM_ReturnInfo();

        if (accessKeyS3 == null || secretKeyS3 == null)
        {
            returnInfo.code = -101;
            returnInfo.message = "Acc�s AWS S3 non initialis�";
            return returnInfo;
        }

        // Create a PutObject request
        PutObjectRequest request = new PutObjectRequest
        {
            BucketName = bucketNameS3,
            Key = pathServer,
            FilePath = pathLocal
        };

        CancellationTokenSource cts = new CancellationTokenSource(TimeSpan.FromSeconds(5));
        CancellationToken cancelToken = cts.Token;

        try
        {
            using (var fileStream = File.OpenRead(pathLocal))
            {
                // Put object
                PutObjectResponse response = await S3Client.PutObjectAsync(request, cancelToken);

                if (response.HttpStatusCode == HttpStatusCode.OK)
                {
                    returnInfo.code = 0;
                    returnInfo.message = "";
                }
                else
                {
                    returnInfo.code = -105;
                    returnInfo.message = "S3Upload error " + response.HttpStatusCode.ToString();
                }

                return returnInfo;
            }
        }
        catch (AmazonS3Exception s3Exception)
        {
            returnInfo.code = -102;
            returnInfo.message = "Erreur Amazon S3 : " + s3Exception.Message;
            return returnInfo;
        }
        catch (OperationCanceledException cancelException)
        {
            returnInfo.code = -103;
            returnInfo.message = "Annulation de la requ�te : " + cancelException.Message;
            return returnInfo;
        }
        catch (Exception e)
        {
            returnInfo.code = -104;
            returnInfo.message = "Echec envoi, exception: " + e;
            return returnInfo;
        }
    }

    public void SwitchEnv(ENV newEnv)
    {
        switch(newEnv)
        {
            case ENV.DEVELOP:
                PlayerPrefs.SetString("HM_ENV","DEVELOP");
                debugEnv = ENV.DEVELOP;
                currentENV = ENV.DEVELOP;
                backURL = backURL_Develop;
                IdentityPool = IdentityPool_Develop;
                AppClientID = AppClientID_Develop;
                userPoolId = userPoolId_Develop;
                break;
            case ENV.PREPROD:
                PlayerPrefs.SetString("HM_ENV","PREPROD");
                debugEnv = ENV.PREPROD;
                currentENV = ENV.PREPROD;
                backURL = backURL_Preprod;
                IdentityPool = IdentityPool_Preprod;
                AppClientID = AppClientID_Preprod;
                userPoolId = userPoolId_Preprod;
                break;
            case ENV.PROD:
                PlayerPrefs.SetString("HM_ENV","PROD");
                debugEnv = ENV.PROD;
                currentENV = ENV.PROD;
                backURL = backURL_Prod;
                IdentityPool = IdentityPool_Prod;
                AppClientID = AppClientID_Prod;
                userPoolId = userPoolId_Prod;
                break;
        }

        //reset acc�s
        foreach(HM_TeacherAccount t in HM_DataController.model.teacherAccounts)
        {
            t.dataAWS._idToken = null;
            t.dataAWS._accessToken = null;
            t.dataAWS._refreshToken = null;
            t.dataAWS.identityId = null;
        }

        //relance sc�ne
        Scene scene = SceneManager.GetActiveScene(); 
        SceneManager.LoadScene(scene.name);
    }

    // Start is called before the first frame update
    void Start()
    {
        providerAWS = new AmazonCognitoIdentityProviderClient(new Amazon.Runtime.AnonymousAWSCredentials(), Region);
        
    }

    public void ForceTokenRefresh(System.Action<bool> callback = null)
    {
        refreshCallback = callback;

        timeLastCheck = Time.realtimeSinceStartup - 60;//permet de forcer le 1er check

        foreach (HM_TeacherAccount t in HM_DataController.model.teacherAccounts)
        {
            t.dataAWS.refreshTime = DateTime.Now.AddMinutes(-50);//permet de forcer le 1er refresh
        }
        forceRefresh = true;
    }

    public void DeleteRefreshCallback()
    {
        refreshCallback = null;
    }

    void Update()
    {
        //refresh token : update chaque minute
        if (forceRefresh || Time.realtimeSinceStartup - timeLastCheck >= 60)
        {
            timeLastCheck = Time.realtimeSinceStartup;

            foreach (HM_TeacherAccount t in HM_DataController.model.teacherAccounts)
            {
                //refresh n�cessaire si possible 
                if (t.dataAWS._refreshToken != null && t.dataAWS._refreshToken.Length > 0 && Application.internetReachability != NetworkReachability.NotReachable)
                {
                    if(forceRefresh || (t.dataAWS.refreshTime.AddMinutes(50).CompareTo(DateTime.Now) < 0))//on refresh � minima toutes les 50 minutes apr�s lancement
                    {
                        t.dataAWS.refreshTime = DateTime.Now;
                        TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
                        Task.Run(async () =>
                        {
                            tcs.SetResult(await RefreshSession(t));
                        });

                        // ConfigureAwait must be true to get unity main thread context
                        tcs.Task.ConfigureAwait(true).GetAwaiter().OnCompleted(() =>
                        {
                            if (tcs.Task.Result)
                            {
                                //Debug.Log("AutoRefresh Success");
                                HM_DataController.model.Save();

                                if(refreshCallback != null)
                                {
                                    refreshCallback.Invoke(true);
                                }
                            }
                            else
                            {
                                Debug.Log("AutoRefresh Error account "+t.teacherCode);

                                //authentification � nouveau n�cessaire dans ce cas l�, on invalide les token
                                t.dataAWS._idToken = null;
                                t.dataAWS._accessToken = null;
                                t.dataAWS._refreshToken = null;
                                t.dataAWS.identityId = null;
                                HM_DataController.model.Save();

                                if (refreshCallback != null)
                                {
                                    refreshCallback.Invoke(false);
                                }
                            }
                        });
                    }
                }
            }

            forceRefresh = false;
        }

        //gestion bluetooth
        if (blt_canCheck && blt_shouldCheck && Time.realtimeSinceStartup - blt_checkLastTime > blt_checkTimeInterval)
        {
            blt_shouldCheck = false;
            BLT_Initialize(false);
        }
    }

    public void CompareVersions()
    {
        if (majorVersionCurrent <= -1 || majorVersionMin <= -1)
        {
            Debug.Log("Impossible de comparer les num�ros de version");
            return;
        }

        if (majorVersionMin > majorVersionCurrent)
        {
            badVersion = true;
        }
        else if (majorVersionMin == majorVersionCurrent)
        {
            if (minorVersionMin > minorVersionCurrent)
            {
                badVersion = true;
            }
            else if (minorVersionMin == minorVersionCurrent)
            {
                if (patchVersionMin > patchVersionCurrent)
                {
                    badVersion = true;
                }
            }
        }
    }


    public void GetData(HM_DataAWS dataAWS, string uri, System.Action<long, string> callback, System.Action<string, long, string> callbackERROR = null, string fileName = "", bool saveFile = false, bool sign = true, string service = null)
    {
        StartCoroutine(GetRequest(dataAWS, uri, callback, callbackERROR, fileName, saveFile, sign, service));
    }

    IEnumerator GetRequest(HM_DataAWS dataAWS, string uri, System.Action<long, string> callback = null, System.Action<string, long, string> callbackERROR = null, string fileName = "", bool saveFile = false, bool sign = true, string service = null)
    {
        UnityWebRequest webRequest = new UnityWebRequest();
        webRequest.url = uri;
        webRequest.method = UnityWebRequest.kHttpVerbGET;

        //s�curit� on sauvegarde les donn�es courantes
        HM_DataController.model.Save();

        //Debug.Log(webRequest.url);

        if (sign)
        {
            webRequest.SetRequestHeader("Authorization", "Bearer " + dataAWS._accessToken);
            /*Dictionary<string, string> headers = new Dictionary<string, string>();

            try
            {
                var signer = new AWS4RequestSigner(data_aws._accessKey, data_aws._secretKey);

                if(service != null)
                {
                    signer.SignWebRequest(webRequest, null, headers, service, data_aws._region);
                }
                else
                {
                    signer.SignWebRequest(webRequest, null, headers, data_aws._service, data_aws._region);
                }

                foreach (KeyValuePair<string, string> pair in headers)
                {
                    webRequest.SetRequestHeader(pair.Key, pair.Value);
                }
                //Debug.Log("SIGN GET OK");
            }
            catch (Exception e)
            {
                Debug.Log("Echec signature, exception: " + e);
            }*/
        }

        webRequest.downloadHandler = new DownloadHandlerBuffer();
        webRequest.SetRequestHeader("Accept", "application/json");
        webRequest.SetRequestHeader("Content-Type", "application/json");
        webRequest.timeout = 60;

        webRequest.disposeUploadHandlerOnDispose = true;
        webRequest.disposeDownloadHandlerOnDispose = true;

        yield return webRequest.SendWebRequest();

        switch (webRequest.result)
        {
            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.DataProcessingError:
            case UnityWebRequest.Result.ProtocolError:
                string error = "GetRequest error" + webRequest.url + " / " + webRequest.responseCode + " / " + webRequest.error;
                if (webRequest.downloadHandler != null)
                {
                    Debug.Log(error + " / " + webRequest.downloadHandler.text);
                }
                if (callbackERROR != null) { callbackERROR.Invoke(webRequest.url, webRequest.responseCode, webRequest.error); }
                break;
            case UnityWebRequest.Result.Success:
                /*Debug.Log("TEST");
                string test1 = webRequest.GetResponseHeader("last-modified");
                Debug.Log(uri);
                Debug.Log(test1);*/
                if (saveFile)
                {
                    string savePath = string.Format("{0}/{1}", Application.persistentDataPath, fileName);
                    /*Debug.Log("save to " + savePath);
                    Debug.Log(uri);
                    Debug.Log(webRequest.downloadHandler.data);*/
                    System.IO.File.WriteAllBytes(savePath, webRequest.downloadHandler.data);
                }
                if (callback != null) { callback.Invoke(webRequest.responseCode, webRequest.downloadHandler.text); }
                break;
        }

        webRequest.Dispose();

        /*using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    //Debug.LogError(pages[page] + ": Error: " + webRequest.error);
                    if (callbackERROR != null) { callbackERROR.Invoke(webRequest.url, webRequest.responseCode, webRequest.error); }
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    //Debug.LogError(pages[page] + ": HTTP Error: " + webRequest.error);
                    if (callbackERROR != null) { callbackERROR.Invoke(webRequest.url, webRequest.responseCode, webRequest.error); }
                    break;
                case UnityWebRequest.Result.Success:
                    //Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                    if (saveFile)
                    {
                        string savePath = string.Format("{0}/{1}", Application.persistentDataPath, fileName);
                        //Debug.Log("save to " + savePath);
                        System.IO.File.WriteAllBytes(savePath, webRequest.downloadHandler.data);
                    }
                    if (callback != null) { callback.Invoke(webRequest.responseCode, webRequest.downloadHandler.text); }
                    break;
            }
        }*/
    }

    public void PostData(HM_DataAWS dataAWS, string uri, string body, string info, System.Action<long, string, string> callback = null, System.Action<string, long, string, string> callbackERROR = null, bool sign = true)
    {
        StartCoroutine(PostRequest(dataAWS, uri, body, info, callback, callbackERROR, sign));
    }

    IEnumerator PostRequest(HM_DataAWS dataAWS, string uri, string body, string info, System.Action<long, string, string> callback = null, System.Action<string, long, string, string> callbackERROR = null, bool sign = true)
    {
        UnityWebRequest webRequest = new UnityWebRequest(uri);
        webRequest.url = uri;
        webRequest.method = UnityWebRequest.kHttpVerbPOST;

        if (sign)
        {
            webRequest.SetRequestHeader("Authorization", "Bearer " + dataAWS._accessToken);
            //webRequest.SetRequestHeader("Authorization", "Bearer "+dataAWS._idToken); 
            /*Dictionary<string, string> headers = new Dictionary<string, string>();

            try
            {
                var signer = new AWS4RequestSigner(data_aws._accessKey, data_aws._secretKey);

                signer.SignWebRequest(webRequest, body, headers, data_aws._service, data_aws._region);

                foreach (KeyValuePair<string, string> pair in headers)
                {
                    webRequest.SetRequestHeader(pair.Key, pair.Value);
                }
                //Debug.Log("SIGN POST OK");
            }
            catch (Exception e)
            {
                Debug.Log("Echec signature, exception: " + e);
            }*/
        }

        webRequest.downloadHandler = new DownloadHandlerBuffer();
        webRequest.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(body));
        webRequest.SetRequestHeader("Accept", "application/json");
        webRequest.SetRequestHeader("Content-Type", "application/json");
        webRequest.timeout = 60;

        webRequest.disposeUploadHandlerOnDispose = true;
        webRequest.disposeDownloadHandlerOnDispose = true;

        //Debug.Log(webRequest.GetRequestHeader("Authorization"));

        yield return webRequest.SendWebRequest();

        switch (webRequest.result)
        {
            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.DataProcessingError:
            case UnityWebRequest.Result.ProtocolError:
                string error = "PostRequest error" + webRequest.url + " / " + webRequest.responseCode + " / " + webRequest.error;
                if (webRequest.downloadHandler != null)
                {
                    Debug.Log(error + " / " + webRequest.downloadHandler.text);
                }
                if (callbackERROR != null) { callbackERROR.Invoke(webRequest.url, webRequest.responseCode, webRequest.error, info); }
                break;
            case UnityWebRequest.Result.Success:
                if (callback != null) { callback.Invoke(webRequest.responseCode, webRequest.downloadHandler.text, info); }
                break;
        }

        webRequest.Dispose();
    }


    public void PutData(HM_DataAWS dataAWS, string uri, string body, System.Action<long, string> callback, System.Action<string, long, string> callbackERROR = null, bool sign = true)
    {
        StartCoroutine(PutRequest(dataAWS, uri, body, callback, callbackERROR, sign));
    }

    IEnumerator PutRequest(HM_DataAWS dataAWS, string uri, string body, System.Action<long, string> callback = null, System.Action<string, long, string> callbackERROR = null, bool sign = true)
    {
        UnityWebRequest webRequest = new UnityWebRequest();
        webRequest.url = uri;
        webRequest.method = UnityWebRequest.kHttpVerbPUT;

        if (sign)
        {
            webRequest.SetRequestHeader("Authorization", "Bearer " + dataAWS._accessToken);
            /*Dictionary<string, string> headers = new Dictionary<string, string>();

            try
            {
                var signer = new AWS4RequestSigner(data_aws._accessKey, data_aws._secretKey);

                signer.SignWebRequest(webRequest, body, headers, data_aws._service, data_aws._region);

                foreach (KeyValuePair<string, string> pair in headers)
                {
                    webRequest.SetRequestHeader(pair.Key, pair.Value);
                }
                //Debug.Log("SIGN PUT OK");
            }
            catch (Exception e)
            {
                Debug.Log("Echec signature, exception: " + e);
            }*/
        }

        webRequest.downloadHandler = new DownloadHandlerBuffer();
        webRequest.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(body));
        webRequest.SetRequestHeader("Accept", "application/json");
        webRequest.SetRequestHeader("Content-Type", "application/json");
        webRequest.timeout = 60;

        webRequest.disposeUploadHandlerOnDispose = true;
        webRequest.disposeDownloadHandlerOnDispose = true;

        yield return webRequest.SendWebRequest();

        switch (webRequest.result)
        {
            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.DataProcessingError:
            case UnityWebRequest.Result.ProtocolError:
                string error = "PutRequest error" + webRequest.url + " / " + webRequest.responseCode + " / " + webRequest.error;
                if (webRequest.downloadHandler != null)
                {
                    Debug.Log(error + " / " + webRequest.downloadHandler.text);
                }
                if (callbackERROR != null) { callbackERROR.Invoke(webRequest.url, webRequest.responseCode, webRequest.error); }
                break;
            case UnityWebRequest.Result.Success:
                if (callback != null) { callback.Invoke(webRequest.responseCode, webRequest.downloadHandler.text); }
                break;
        }

        webRequest.Dispose();
    }

    public async Task<bool> RefreshSession(HM_TeacherAccount t)
    {
        Debug.Log("RefreshSession "+t.teacherCode);

        DateTime issued = DateTime.Now;

        if (t.dataAWS._refreshToken != null && t.dataAWS._refreshToken.Length > 0)
        {
            try
            {
                CognitoUserPool userPool = new CognitoUserPool(userPoolId, AppClientID, providerAWS);

                // apparently the username field can be left blank for a token refresh request
                CognitoUser user = new CognitoUser("", AppClientID, userPool, providerAWS);

                // The "Refresh token expiration (days)" (Cognito->UserPool->General Settings->App clients->Show Details) is the
                // amount of time since the last login that you can use the refresh token to get new tokens. After that period the refresh
                // will fail Using DateTime.Now.AddHours(1) is a workaround for https://github.com/aws/aws-sdk-net-extensions-cognito/issues/24
                user.SessionTokens = new CognitoUserSession(
                   t.dataAWS._idToken,
                   t.dataAWS._accessToken,
                   t.dataAWS._refreshToken,
                   issued,
                   DateTime.Now.AddDays(30)); // TODO: need to investigate further. 
                                              // It was my understanding that this should be set to when your refresh token expires...

                // Attempt refresh token call
                AuthFlowResponse authFlowResponse = await user.StartWithRefreshTokenAuthAsync(new InitiateRefreshTokenAuthRequest
                {
                    AuthFlowType = AuthFlowType.REFRESH_TOKEN_AUTH
                })
                .ConfigureAwait(false);

                //Debug.Log("User token successfully updated!");

                t.dataAWS._refreshToken = authFlowResponse.AuthenticationResult.RefreshToken;
                t.dataAWS._accessToken = authFlowResponse.AuthenticationResult.AccessToken;
                t.dataAWS._idToken = authFlowResponse.AuthenticationResult.IdToken;
                //Debug.Log("_refreshToken " + data_aws._refreshToken);
                //Debug.Log("_accessToken " + data_aws._accessToken);
                //Debug.Log("_idToken " + data_aws._idToken);

                // update credentials with the latest access token
                /*CognitoAWSCredentials _cognitoAWSCredentials = user.GetCognitoAWSCredentials(IdentityPool, Region);

                data_aws._accessKey = _cognitoAWSCredentials.GetCredentials().AccessKey;
                data_aws._secretKey = _cognitoAWSCredentials.GetCredentials().SecretKey;
                data_aws._securityToken = _cognitoAWSCredentials.GetCredentials().Token;*/
                //Debug.Log("_accessKey " + data_aws._accessKey);
                //Debug.Log("_secretKey " + data_aws._secretKey);
                //Debug.Log("_securityToken " + data_aws._securityToken);

                t.dataAWS.refreshTime = DateTime.Now;

                return true;
            }
            catch (NotAuthorizedException ne)
            {
                // https://docs.aws.amazon.com/cognito/latest/developerguide/amazon-cognito-user-pools-using-tokens-with-identity-providers.html
                // refresh tokens will expire - user must login manually every x days (see user pool -> app clients -> details)
                Debug.Log("NotAuthorizedException: " + ne);
            }
            catch (WebException webEx)
            {
                // we get a web exception when we cant connect to aws - means we are offline
                Debug.Log("WebException: " + webEx);
            }
            catch (Exception ex)
            {
                Debug.Log("Exception: " + ex);
            }
        }
        return false;
    }

    public int OnDataSynchro(string data)
    {
        //Debug.Log(data);
        //les donn�es contiennent un jeu de donn�e retour

        HM_Log.ReplaceTextFile("HM_SyncDown1.txt", data, true);

        //jeu de donn�es pour test
        /*TextAsset mytxtData = (TextAsset)Resources.Load("testSynchroBackJSON");
        data = mytxtData.text;*/

        try
        {
            //ajout des ent�tes manquantes pour d�s�rialisation automatique
            JObject o = JObject.Parse(data);

            JArray arrayProgression = (JArray)o["usersProgressions"];
            foreach (JObject o1 in arrayProgression)
            {
                if (((string)o1["activityId"]).Equals("834"))
                {
                    var property = new JProperty("$type", "HM_DataSynchro_UserProgression_Evasion, Assembly-CSharp");
                    var properties = o1.Properties().ToList();
                    properties.Insert(0, property);
                    o1.RemoveAll();
                    foreach (var prop in properties)
                    {
                        o1.Add(prop);
                    }
                }
                else if (((string)o1["activityId"]).Equals("835"))
                {
                    var property = new JProperty("$type", "HM_DataSynchro_UserProgression_Elargir, Assembly-CSharp");
                    var properties = o1.Properties().ToList();
                    properties.Insert(0, property);
                    o1.RemoveAll();
                    foreach (var prop in properties)
                    {
                        o1.Add(prop);
                    }
                }
                else if (((string)o1["activityId"]).Equals("836"))
                {
                    var property = new JProperty("$type", "HM_DataSynchro_UserProgression_Luciole, Assembly-CSharp");
                    var properties = o1.Properties().ToList();
                    properties.Insert(0, property);
                    o1.RemoveAll();
                    foreach (var prop in properties)
                    {
                        o1.Add(prop);
                    }
                }
            }

            JArray arraySettings = (JArray)o["usersSettings"];
            foreach (JObject o1 in arraySettings)
            {
                if (((string)o1["activityId"]).Equals("834"))
                {
                    var property = new JProperty("$type", "HM_DataSynchro_UserSettings_Evasion, Assembly-CSharp");
                    var properties = o1.Properties().ToList();
                    properties.Insert(0, property);
                    o1.RemoveAll();
                    foreach (var prop in properties)
                    {
                        o1.Add(prop);
                    }
                }
                else if (((string)o1["activityId"]).Equals("835"))
                {
                    var property = new JProperty("$type", "HM_DataSynchro_UserSettings_Elargir, Assembly-CSharp");
                    var properties = o1.Properties().ToList();
                    properties.Insert(0, property);
                    o1.RemoveAll();
                    foreach (var prop in properties)
                    {
                        o1.Add(prop);
                    }
                }
                else if (((string)o1["activityId"]).Equals("836"))
                {
                    var property = new JProperty("$type", "HM_DataSynchro_UserSettings_Luciole, Assembly-CSharp");
                    var properties = o1.Properties().ToList();
                    properties.Insert(0, property);
                    o1.RemoveAll();
                    foreach (var prop in properties)
                    {
                        o1.Add(prop);
                    }
                }
            }

            JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto };
            HM_DataSynchro newData = JsonConvert.DeserializeObject<HM_DataSynchro>(o.ToString(Formatting.None), settings);

            //Debug.Log(o.ToString(Formatting.None));

            HM_Log.ReplaceTextFile("HM_SyncDown2.txt", o.ToString(Formatting.None), true);

            //v�rification des donn�es � traiter en fonction des dates de r�vision

            string dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'";


            foreach (HM_TeacherAccount t in HM_DataController.model.teacherAccounts)
            {
                if (t.teacherCode.ToUpper().Equals(newData.teacherCode.ToUpper()))
                {
                    foreach (HM_DataSynchro_User us in newData.users)
                    {
                        bool foundUser = false;

                        //donn�es user
                        foreach (HM_ChildUser u in t.childUsers)
                        {
                            if (u.userIconID == us.userIconID && u.loginCode.Equals(us.loginCode))
                            {
                                foundUser = true;
                                u.isArchived = us.isArchived;
                                //Debug.Log(us.userIconID + " / "+ us.loginCode + " / "+ us.isArchived);

                                if (!u.userID.Equals(us.userID))//conflit, il faut consid�rer que l'userID re�u est ma�tre sur celui local qu'on remplace
                                {
                                    u.userID = us.userID;
                                }

                                if (us.revisionDate != null && us.revisionDate.Length > 0)
                                {
                                    DateTime t1 = DateTime.ParseExact(u.revisionDate, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
                                    DateTime t2 = DateTime.Parse(us.revisionDate).ToUniversalTime();
                                    int compareDateResult = DateTime.Compare(t1, t2);
                                    if (compareDateResult < 0)//nouvelle date re�u plus r�cente que les donn�es locales, on charge les nouvelles donn�es
                                    {
                                        u.name = us.name;
                                        u.surname = us.surname;
                                        //u.loginCode = us.loginCode;//ne devrait jamais changer
                                        //u.userIconID = us.userIconID;//ne devrait jamais changer
                                        u.schoolGrade = us.schoolGrade;
                                        u.deviceColor = us.deviceColor;
                                        u.revisionDate = us.revisionDate;
                                        u.activeUser = true;
                                    }
                                }
                            }
                        }

                        if(!foundUser && us.isArchived == false)//nouvel utilisateur provenant de la plateforme, on le cr�e tant que ce n'est pas un profil archiv�
                        {
                            //si ce nouvel utilisateur fait partie d'un lot g�n�rique qui est disponible
                            if (us.genericGroup >= 0)
                            {
                                bool inUsed = false;
                                foreach (HM_TeacherAccount t2 in HM_DataController.model.teacherAccounts)
                                {
                                    if (t2.teacherCode.ToUpper().Equals(t.teacherCode.ToUpper()))
                                        continue;

                                    foreach(int id in t2.genericGroupsUsed)
                                    {
                                        if(id == us.genericGroup)
                                        {
                                            inUsed = true;
                                            break;
                                        }
                                    }
                                }

                                if(inUsed)
                                {
                                    throw new Exception("GROUP_IN_USED");
                                }
                                else
                                {
                                    //on ajoute le groupe au prof
                                    t.genericGroupsUsed.Add(us.genericGroup);

                                    for (int j = 1; j <= 40; j++)
                                    {
                                        HM_ChildUser childUser = new HM_ChildUser();

                                        childUser.userID = System.Guid.NewGuid().ToString();
                                        childUser.userIconID = j;
                                        childUser.genericGroup = us.genericGroup;
                                        childUser.isGeneric = true;
                                        childUser.fromGeneric = true;
                                        childUser.activeUser = false;
                                        childUser.isArchived = false;
                                        childUser.revisionDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

                                        childUser.schoolGrade = "CP";

                                        childUser.loginCode = HM_ChildCodes.childCodes[us.genericGroup - 1, j - 1].ToString();

                                        //pour le compte re�u en question, on modifie les donn�es
                                        if (childUser.userIconID == us.userIconID && childUser.loginCode.Equals(us.loginCode))
                                        {
                                            childUser.userID = us.userID;
                                            childUser.name = us.name;
                                            childUser.surname = us.surname;
                                            childUser.fromGeneric = false;
                                            childUser.schoolGrade = us.schoolGrade;
                                            childUser.deviceColor = us.deviceColor;
                                            childUser.activeUser = true;
                                            childUser.revisionDate = us.revisionDate;
                                        }

                                        t.childUsers.Add(childUser);
                                    }
                                }
                            }
                            else
                            {
                                HM_ChildUser childUser = new HM_ChildUser();

                                childUser.userID = us.userID;
                                childUser.userIconID = us.userIconID;
                                childUser.name = us.name;
                                childUser.surname = us.surname;
                                childUser.deviceColor = us.deviceColor;
                                childUser.isGeneric = false;
                                childUser.fromGeneric = false;
                                childUser.activeUser = true;
                                childUser.isArchived = false;
                                childUser.genericGroup = -1;
                                childUser.schoolGrade = us.schoolGrade;
                                childUser.loginCode = us.loginCode;
                                if (us.revisionDate != null && us.revisionDate.Length > 0)
                                {
                                    DateTime ttemp = DateTime.Parse(us.revisionDate).ToUniversalTime();
                                    childUser.revisionDate = ttemp.ToString(dateFormat);
                                }
                                else
                                {
                                    childUser.revisionDate = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssK");
                                }

                                t.childUsers.Add(childUser);
                            }

                        }
                    }

                    foreach (HM_ChildUser u in t.childUsers)
                    {
                        //progression
                        foreach (HM_DataSynchro_UserProgression usp in newData.usersProgressions)
                        {
                            if (u.activeUser && u.userID.Equals(usp.userID) && usp.revisionDate != null && usp.revisionDate.Length > 0)
                            {
                                if (usp.activityId == 834)
                                {
                                    HM_DataSynchro_UserProgression_Evasion uspEvasion = (HM_DataSynchro_UserProgression_Evasion)usp;

                                    int nombrePagesTotalLocal = 0;
                                    if(u.appEvasionUserData.progression.parsevalCastle != null && u.appEvasionUserData.progression.parsevalCastle.pages != null)
                                    {
                                        nombrePagesTotalLocal += u.appEvasionUserData.progression.parsevalCastle.pages.Count;
                                    }
                                    if (u.appEvasionUserData.progression.parsevalGhost != null && u.appEvasionUserData.progression.parsevalGhost.pages != null)
                                    {
                                        nombrePagesTotalLocal += u.appEvasionUserData.progression.parsevalGhost.pages.Count;
                                    }
                                    if (u.appEvasionUserData.progression.parsevalRunner != null && u.appEvasionUserData.progression.parsevalRunner.pages != null)
                                    {
                                        nombrePagesTotalLocal += u.appEvasionUserData.progression.parsevalRunner.pages.Count;
                                    }
                                    if (u.appEvasionUserData.progression.parsevalTower != null && u.appEvasionUserData.progression.parsevalTower.pages != null)
                                    {
                                        nombrePagesTotalLocal += u.appEvasionUserData.progression.parsevalTower.pages.Count;
                                    }

                                    int nombrePagesTotalServer = 0;
                                    if (uspEvasion.appData.parsevalCastle != null && uspEvasion.appData.parsevalCastle.pages != null)
                                    {
                                        nombrePagesTotalServer += uspEvasion.appData.parsevalCastle.pages.Count;
                                    }
                                    if (uspEvasion.appData.parsevalGhost != null && uspEvasion.appData.parsevalGhost.pages != null)
                                    {
                                        nombrePagesTotalServer += uspEvasion.appData.parsevalGhost.pages.Count;
                                    }
                                    if (uspEvasion.appData.parsevalRunner != null && uspEvasion.appData.parsevalRunner.pages != null)
                                    {
                                        nombrePagesTotalServer += uspEvasion.appData.parsevalRunner.pages.Count;
                                    }
                                    if (uspEvasion.appData.parsevalTower != null && uspEvasion.appData.parsevalTower.pages != null)
                                    {
                                        nombrePagesTotalServer += uspEvasion.appData.parsevalTower.pages.Count;
                                    }

                                    //on enregistre en local les donn�es serveur si le nombe de pages total est sup�rieur c�t� serveur
                                    if (nombrePagesTotalServer > nombrePagesTotalLocal)
                                    {
                                        u.appEvasionUserData.progression = ((HM_DataSynchro_UserProgression_Evasion)usp).appData;
                                        DateTime t2 = DateTime.Parse(usp.revisionDate).ToUniversalTime();
                                        u.appEvasionUserData.progression.dateUpdate = t2.ToString(dateFormat);
                                    }
                                    else if(nombrePagesTotalServer == nombrePagesTotalLocal)
                                    {
                                        //comparaison revisionDate
                                        DateTime t1 = DateTime.ParseExact(u.appEvasionUserData.progression.dateUpdate, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
                                        DateTime t2 = DateTime.Parse(usp.revisionDate).ToUniversalTime();
                                        int compareDateResult = DateTime.Compare(t1, t2);
                                        if (compareDateResult < 0)//nouvelle date re�u plus r�cente que les donn�es locales, on charge les nouvelles donn�es
                                        {
                                            u.appEvasionUserData.progression = ((HM_DataSynchro_UserProgression_Evasion)usp).appData;
                                            u.appEvasionUserData.progression.dateUpdate = t2.ToString(dateFormat);
                                        }
                                    }

                                }
                                else if (usp.activityId == 835)
                                {
                                    HM_DataSynchro_UserProgression_Elargir uspElargir = (HM_DataSynchro_UserProgression_Elargir)usp;

                                    int nombreExercicesTotalLocal = 0;
                                    if(u.appElargirUserData.progression.texts != null)
                                    {
                                        foreach (HM_AppElargirUserProgression_Text text in u.appElargirUserData.progression.texts)
                                        {
                                            if(text.exercices != null)
                                            {
                                                nombreExercicesTotalLocal += text.exercices.Count;
                                            }
                                        }
                                    }

                                    int nombreExercicesTotalServer = 0;
                                    if (uspElargir.appData.texts != null)
                                    {
                                        foreach (HM_AppElargirUserProgression_Text text in uspElargir.appData.texts)
                                        {
                                            if (text.exercices != null)
                                            {
                                                nombreExercicesTotalServer += text.exercices.Count;
                                            }
                                        }
                                    }

                                    //on enregistre en local les donn�es serveur si le nombe d'exercices total est sup�rieur c�t� serveur
                                    if (nombreExercicesTotalServer > nombreExercicesTotalLocal)
                                    {
                                        u.appElargirUserData.progression = ((HM_DataSynchro_UserProgression_Elargir)usp).appData;
                                        DateTime t2 = DateTime.Parse(usp.revisionDate).ToUniversalTime();
                                        u.appElargirUserData.progression.dateUpdate = t2.ToString(dateFormat);
                                    }
                                    else if (nombreExercicesTotalServer == nombreExercicesTotalLocal)
                                    {
                                        //comparaison revisionDate
                                        DateTime t1 = DateTime.ParseExact(u.appElargirUserData.progression.dateUpdate, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
                                        DateTime t2 = DateTime.Parse(usp.revisionDate).ToUniversalTime();
                                        int compareDateResult = DateTime.Compare(t1, t2);
                                        if (compareDateResult < 0)//nouvelle date re�u plus r�cente que les donn�es locales, on charge les nouvelles donn�es
                                        {
                                            u.appElargirUserData.progression = ((HM_DataSynchro_UserProgression_Elargir)usp).appData;
                                            u.appElargirUserData.progression.dateUpdate = t2.ToString(dateFormat);
                                        }
                                    }
                                }
                                else if (usp.activityId == 836)
                                {
                                    HM_DataSynchro_UserProgression_Luciole uspLuciole = (HM_DataSynchro_UserProgression_Luciole)usp;

                                    int nombreActivityTotalLocal = 0;

                                    if (u.appLucioleUserData.progression.lucioleParameters.scoremanager != null)
                                    {
                                        nombreActivityTotalLocal = u.appLucioleUserData.progression.lucioleParameters.scoremanager.Count;
                                    }

                                    int nombreActivityTotalServer = 0;

                                    if (uspLuciole.appData.lucioleParameters.scoremanager != null)
                                    {
                                        nombreActivityTotalServer = uspLuciole.appData.lucioleParameters.scoremanager.Count;
                                    }

                                    //on enregistre en local les donn�es serveur si le nombe d'exercices total est sup�rieur c�t� serveur
                                    if (nombreActivityTotalServer > nombreActivityTotalLocal)
                                    {
                                        u.appLucioleUserData.progression = ((HM_DataSynchro_UserProgression_Luciole)usp).appData;
                                        DateTime t2 = DateTime.Parse(usp.revisionDate).ToUniversalTime();
                                        u.appLucioleUserData.progression.dateUpdate = t2.ToString(dateFormat);
                                    }
                                    else if (nombreActivityTotalServer == nombreActivityTotalLocal)
                                    {
                                        //comparaison revisionDate
                                        DateTime t1 = DateTime.ParseExact(u.appLucioleUserData.progression.dateUpdate, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
                                        DateTime t2 = DateTime.Parse(usp.revisionDate).ToUniversalTime();
                                        int compareDateResult = DateTime.Compare(t1, t2);
                                        if (compareDateResult < 0)//nouvelle date re�u plus r�cente que les donn�es locales, on charge les nouvelles donn�es
                                        {
                                            u.appLucioleUserData.progression = ((HM_DataSynchro_UserProgression_Luciole)usp).appData;
                                            u.appLucioleUserData.progression.dateUpdate = t2.ToString(dateFormat);
                                        }
                                    }
                                }
                            }
                        }

                        //settings
                        foreach (HM_DataSynchro_UserSettings uss in newData.usersSettings)
                        {
                            if (u.activeUser && u.userID.Equals(uss.userID) && uss.revisionDate != null && uss.revisionDate.Length > 0)
                            {
                                if (uss.activityId == 834)
                                {
                                    DateTime t1 = DateTime.ParseExact(u.appEvasionUserData.settings.revisionDate, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
                                    DateTime t2 = DateTime.Parse(uss.revisionDate).ToUniversalTime();
                                    int compareDateResult = DateTime.Compare(t1, t2);
                                    if (compareDateResult < 0)//nouvelle date re�u plus r�cente que les donn�es locales, on charge les nouvelles donn�es
                                    {
                                        u.appEvasionUserData.settings = ((HM_DataSynchro_UserSettings_Evasion)uss).appData;
                                        u.appEvasionUserData.settings.revisionDate = t2.ToString(dateFormat);
                                    }
                                }
                                else if (uss.activityId == 835)
                                {
                                    DateTime t1 = DateTime.ParseExact(u.appElargirUserData.settings.revisionDate, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
                                    DateTime t2 = DateTime.Parse(uss.revisionDate).ToUniversalTime();
                                    int compareDateResult = DateTime.Compare(t1, t2);
                                    if (compareDateResult < 0)//nouvelle date re�u plus r�cente que les donn�es locales, on charge les nouvelles donn�es
                                    {
                                        u.appElargirUserData.settings = ((HM_DataSynchro_UserSettings_Elargir)uss).appData;
                                        u.appElargirUserData.settings.revisionDate = t2.ToString(dateFormat);
                                    }
                                }
                                else if (uss.activityId == 836)
                                {
                                    DateTime t1 = DateTime.ParseExact(u.appLucioleUserData.settings.revisionDate, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
                                    DateTime t2 = DateTime.Parse(uss.revisionDate).ToUniversalTime();
                                    int compareDateResult = DateTime.Compare(t1, t2);
                                    if (compareDateResult < 0)//nouvelle date re�u plus r�cente que les donn�es locales, on charge les nouvelles donn�es
                                    {
                                        u.appLucioleUserData.settings = ((HM_DataSynchro_UserSettings_Luciole)uss).appData;
                                        u.appLucioleUserData.settings.revisionDate = t2.ToString(dateFormat);
                                    }
                                }
                            }
                        }
                    }

                    //suppression des results qui ont �t� (normalement) envoy�s
                    foreach (HM_ChildUser u in t.childUsers)
                    {
                        if (u.activeUser)
                        {
                            u.appElargirUserData.results.Clear();
                            u.appEvasionUserData.results.Clear();
                            u.appLucioleUserData.results.Clear();
                        }
                    }

                    t.dateLastSynchro = DateTime.Parse(newData.lastSynchronization).ToUniversalTime().ToString("d MMM yyyy H'h'mm");
                }

            }

            HM_DataController.model.lastSynchronization = DateTime.Parse(newData.lastSynchronization).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");

            HM_DataController.model.Save();

            return 0;
        }
        catch (Exception e)
        {
            //souci d�tect� au traitement des donn�es, on recharge la derni�re sauvegarde
            HM_Model.Load();

            HM_Scripts.optionPanel.UpdateData();

            HM_Log.AppendTextToFile("HM_Log.txt", e.Message);
            Debug.Log("Error new data synchro : " + e);

            if (e.Message.Equals("GROUP_IN_USED"))
                return 2;

            return 1;
        }
    }

    //gestion bluetooth
    public Networking bluetoothNetworking;

    [HideInInspector]
    public bool blt_initialized = false;

    private bool blt_canCheck = false;
    private bool blt_shouldCheck = false;
    private float blt_checkTimeInterval = 3.0f;
    private float blt_checkLastTime = 0.0f;
    private bool blt_sendingData = false;

    private int nbrChunks = -2;
    private List<byte[]> dataBack = new List<byte[]>();

    public void BLT_Initialize(bool showErrorMessage = true)
    {
        instance = this;

        if (blt_initialized)
        {
            return;
        }

        deviceID = "Client" + UnityEngine.Random.Range(1000000, 9999999);
        //Debug.Log("BLT : "+deviceID);

        blt_canCheck = true;

        bluetoothNetworking.Initialize((error) =>
        {
            HM_Scripts.contentProfil.bluetoothIconOn.SetActive(false);
            HM_Scripts.contentProfil.bluetoothIconOff.SetActive(true);
            blt_checkLastTime = Time.realtimeSinceStartup;
            BLT_DeInitialize();

            if(blt_canCheck)
                blt_shouldCheck = true;

            if (showErrorMessage)
            {
                string msg = error + " / Bluetooth activation will be checked again every " + blt_checkTimeInterval + " seconds";
                Debug.LogWarning(msg);
            }
        }, 
        (message) =>
        {
            Debug.Log(message);
        }, 
        () =>
        {
            blt_initialized = true;
            blt_shouldCheck = false;
            blt_sendingData = false;

            //d�marrage mode client (peripheral)
            bluetoothNetworking.StartClient("HmBleNetwork", deviceID, () =>
            {
                HM_Scripts.contentProfil.bluetoothIconOn.SetActive(true);
                HM_Scripts.contentProfil.bluetoothIconOff.SetActive(false);

                bluetoothNetworking.StatusMessage = "Started advertising";

                Debug.Log("BLE Client started");
            },
            (clientName, characteristic, bytes) =>
            {
                //Debug.Log("Data received from Server : " + characteristic);

                string str = Encoding.ASCII.GetString(bytes);

                //Debug.Log("Data : " + str);

                //Debug.Log("DataSize : " + bytes.Length);

                if (str.Split("|").Length == 2 && str.Split("|")[0].Equals("SEND_DATA") && !blt_sendingData)
                {
                    //Debug.Log(str.Split("|")[1]);
                    HM_DataSynchro newDataSynchro = new HM_DataSynchro();
                    int synchroResult = newDataSynchro.LoadFromAppData(str.Split("|")[1]);

                    if (synchroResult == 0 || synchroResult == -2)
                    {
                        HM_Scripts.popupSynchro.OpenContent();
                        blt_sendingData = true;

                        string zippedString = Zip(JsonConvert.SerializeObject(newDataSynchro));

                        byte[] bytesToSend = Encoding.ASCII.GetBytes(zippedString);
                        Debug.Log("Start sending " + bytesToSend.Length + " bytes (MTU : " + bluetoothNetworking._mtu + " bytes)");
                        bluetoothNetworking.SendFromClient(bytesToSend, () =>
                        {
                            nbrChunks = -1;
                            dataBack = new List<byte[]>();
                            blt_sendingData = false;
                        });
                    }
                }
                else if(str.Split("|").Length == 2 && str.Split("|")[0].Equals("NO_DATA_BACK"))
                {
                    nbrChunks = -2;
                    HM_Scripts.popupSynchro.OnSuccessMessage();
                }
                else if(nbrChunks != -2)//normalement donn�e de chunk data retour synchro
                {
                    if (nbrChunks == -1)
                    {
                        nbrChunks = (bytes[2] << 8) | bytes[3];
                    }

                    int _currentChunkIndex = (bytes[0] << 8) | bytes[1];

                    dataBack.Add(bytes);

                    if (dataBack.Count == nbrChunks)
                    {
                        string data = RecomposeData();

                        nbrChunks = -2;

                        int result = OnDataSynchro(data);

                        if (result == 0)
                        {
                            HM_Scripts.popupSynchro.OnSuccessMessage();
                        }
                        else
                        {
                            HM_Scripts.popupSynchro.OnErrorMessage("Une erreur est survenue pendant la synchronisation");
                        }
                    }
                }
            });
        });
    }

    public string RecomposeData()
    {
        //on remet le tableau dans l'ordre
        //data = data.OrderBy(chunk => ((chunk[0] << 8) | chunk[1])).ToList();

        //on doit enlever les ent�tes

        List<byte[]> fixedData = new List<byte[]>();
        foreach (byte[] chunk in dataBack)
        {
            byte[] newData = new byte[chunk.Length - 4];
            Array.Copy(chunk, 4, newData, 0, chunk.Length - 4);
            fixedData.Add(newData);
        }

        int totalLength = fixedData.Sum(a => a.Length);
        byte[] finalData = new byte[totalLength];
        int currentIndex = 0;
        foreach (byte[] chunk in fixedData)
        {
            Array.Copy(chunk, 0, finalData, currentIndex, chunk.Length);
            currentIndex += chunk.Length;
        }

        string compressedString = Encoding.ASCII.GetString(finalData);

        return UnZip(compressedString);
    }

    public void BLT_DeInitialize(bool force = false)
    {
        if (force)
            blt_canCheck = false;

        blt_shouldCheck = false;
        if(blt_initialized)
        {
            blt_initialized = false;
            if(blt_sendingData)
            {
                HM_Scripts.popupSynchro.OnErrorMessage("Une erreur est survenue pendant le transfert de donn�es");
            }
            blt_sendingData = false;
            bluetoothNetworking.StopClient(() =>
            {
                Debug.Log("BLE Client stopped");
            });
        }
    }

    public static string Zip(string input)
    {
        byte[] compressedData;
        using (var outputStream = new MemoryStream())
        {
            using (var gzipStream = new GZipStream(outputStream, CompressionMode.Compress))
            {
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);
                gzipStream.Write(inputBytes, 0, inputBytes.Length);
            }
            compressedData = outputStream.ToArray();
        }

        return Convert.ToBase64String(compressedData);
    }

    public static string UnZip(string input)
    {
        byte[] compressedData = Convert.FromBase64String(input);
        using (var inputStream = new MemoryStream(compressedData))
        using (var gzipStream = new GZipStream(inputStream, CompressionMode.Decompress))
        using (var output = new MemoryStream())
        {
            gzipStream.CopyTo(output);
            byte[] outputBytes = output.ToArray();
            return Encoding.UTF8.GetString(outputBytes);
        }
    }

}
