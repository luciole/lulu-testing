using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using UnityEngine.SceneManagement;

public class HM_OptionPanelController : MonoBehaviour
{
    public Button optionButton;
    public CanvasGroup optionContentCG;
    [HideInInspector]
    public bool isButtonVisible = true;
    [HideInInspector]
    public bool isOpen = false;

    public Button backMenuButton;
    public Button teacherAccountButton;
    public Button disconnectButton;

    public GameObject accountInfoTemplate;


    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    public void Init()
    {
        UpdateData();

        //optionButton.targetGraphic.color = new Color32(65, 63, 65, 255);
        //optionButton.transform.GetChild(0).GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        optionContentCG.transform.localScale = new Vector3(0, 0, 0);
        optionContentCG.alpha = 0;
        optionContentCG.interactable = false;
        optionContentCG.blocksRaycasts = false;
        isOpen = false;

        HM_Scripts.optionPanel = this;

        //HideButton(true);

        /*switch (HM_DataController.model.currentColorID)
        {
            case 1:
                deviceColor.color = new Color32(250, 133, 189, 255);
                break;
            case 2:
                deviceColor.color = new Color32(184, 78, 193, 255);
                break;
            case 3:
                deviceColor.color = new Color32(99, 116, 197, 255);
                break;
            case 4:
                deviceColor.color = new Color32(55, 189, 249, 255);
                break;
            case 5:
                deviceColor.color = new Color32(57, 206, 222, 255);
                break;
            case 6:
                deviceColor.color = new Color32(50, 177, 78, 255);
                break;
            case 7:
                deviceColor.color = new Color32(212, 226, 92, 255);
                break;
            case 8:
                deviceColor.color = new Color32(251, 202, 62, 255);
                break;
            case 9:
                deviceColor.color = new Color32(150, 115, 105, 255);
                break;
            case 10:
                deviceColor.color = new Color32(123, 151, 161, 255);
                break;
        }

        if(HM_DataController.currentChildUser != null)
        {
            childUserIcon.gameObject.SetActive(true);
            childUserIcon.sprite = HM_Scripts.pluginController.childAvatars[HM_DataController.currentChildUser.userIconID-1];
             
            childUserName.text = HM_DataController.currentChildUser.name + " " + HM_DataController.currentChildUser.surname;
        }
        else
        {
            childUserIcon.gameObject.SetActive(false);
            childUserName.text = "";
        }*/
    }

    public void UpdateData()
    {
       /* if (dateSynchro.Length > 0)
        {
            HM_Scripts.optionPanel.infoSynchro.text = dateSynchro;
        }
        else
        {
            HM_Scripts.optionPanel.infoSynchro.text = "Jamais";
        }*/

        for(int i = accountInfoTemplate.transform.parent.childCount -1; i>=1; i--)
        {
            DestroyImmediate(accountInfoTemplate.transform.parent.GetChild(i).gameObject);
        }

        int cptProf = 0;

        foreach (HM_TeacherAccount t in HM_DataController.model.teacherAccounts)
        {
            cptProf++;
            GameObject newGo = GameObject.Instantiate(accountInfoTemplate, accountInfoTemplate.transform.parent, false);

            int cptEleve = 0;
            foreach (HM_ChildUser c in t.childUsers)
            {
                if (c.activeUser && c.isArchived == false)
                {
                    cptEleve++;
                }
            }

            newGo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = t.teacherCode.Substring(0,3)+"***";

            if (t.dataAWS._idToken != null && t.dataAWS._idToken.Length > 0)
            {
                newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Mot de passe renseign�";
                newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().color = new Color32(22,128,7,255);
            }
            else
            {
                newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Mot de passe non renseign�";
                newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().color = new Color32(205, 69, 0, 255);
            }

            if (t.dateLastSynchro == null || t.dateLastSynchro.Length == 0)
            {
                newGo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Derni�re synchro : Jamais";
            }
            else
            {
                newGo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Sync : "+t.dateLastSynchro;
            }

            if (cptEleve <= 1)
            {
                newGo.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = "" + cptEleve + " profil �l�ve actif";
            }
            else
            {
                newGo.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = "" + cptEleve + " profils �l�ves actifs";
            }

            newGo.SetActive(true);
        }

        /*if (cptProf == 1)
        {
            HM_Scripts.optionPanel.infoProf.text = "1 compte enseignant�e";
        }
        else
        {
            HM_Scripts.optionPanel.infoProf.text = "" + cptProf + " comptes enseignant�e�s";
        }

        if (cptEleve == 1)
        {
            HM_Scripts.optionPanel.infoEleve.text = "1 profil �l�ve actif";
        }
        else
        {
            HM_Scripts.optionPanel.infoEleve.text = "" + cptEleve + " profils �l�ves actifs";
        }*/

    }

    public void HideButton(bool immediate = false)
    {
        if (isOpen)
            ClosePanel();

        isButtonVisible = false;
        optionButton.GetComponent<CanvasGroup>().interactable = false;
        if (immediate)
        {
            optionButton.GetComponent<CanvasGroup>().alpha = 0;
            optionButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(-75, 75);
        }
        else
        {
            optionButton.GetComponent<CanvasGroup>().interactable = false;
            optionButton.GetComponent<CanvasGroup>().DOFade(0, 0.2f);
            optionButton.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-75, 75), 0.2f).SetEase(Ease.InSine).SetUpdate(true);
        }
    }

    public void ShowButton(bool immediate = false)
    {
        isButtonVisible = true;
        if (immediate)
        {
            optionButton.GetComponent<CanvasGroup>().alpha = 1;
            optionButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
            optionButton.GetComponent<CanvasGroup>().interactable = true;
        }
        else
        {
            optionButton.GetComponent<CanvasGroup>().DOFade(1, 0.2f);
            optionButton.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, 0), 0.2f).SetEase(Ease.InSine).SetUpdate(true).OnComplete(() =>
            {
                optionButton.GetComponent<CanvasGroup>().interactable = true;
            });
        }
    }

    public void OnSwitchPanelOption()
    {
        if(isOpen)
        {
            ClosePanel();
        }
        else
        {
            OpenPanel();
        }
    }

    public void OpenPanel()
    {
        optionButton.interactable = false;
        optionContentCG.interactable = false;
        optionContentCG.blocksRaycasts = false;
        /*optionButton.GetComponent<Button>().targetGraphic.color = new Color32(255, 255, 255, 255);
        optionButton.transform.GetChild(0).GetComponent<Image>().color = new Color32(65, 63, 65, 255);*/
        optionContentCG.DOFade(1,0.15f);
        optionContentCG.transform.DOScale(new Vector3(1, 1, 1), 0.15f).SetEase(Ease.InSine).SetUpdate(true).OnComplete(() =>
        {
            optionContentCG.interactable = true;
            optionContentCG.blocksRaycasts = true;
            optionButton.interactable = true;
        });

        isOpen = true;
    }

    public void ClosePanel()
    {
        optionButton.interactable = false;
        optionContentCG.interactable = false;
        optionContentCG.blocksRaycasts = false;
        optionContentCG.DOFade(0, 0.15f);
        optionContentCG.transform.DOScale(new Vector3(0, 0, 0), 0.15f).SetEase(Ease.InSine).SetUpdate(true).OnComplete(() =>
        {
            optionButton.interactable = true;
            //optionButton.targetGraphic.color = new Color32(65, 63, 65, 255);
            //optionButton.transform.GetChild(0).GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        });

        isOpen = false;
    }

    public void OnBackMenu()
    {
        OnSwitchPanelOption();
        HM_Content.currentMainContent.CloseContent(HM_Scripts.contentProfil);
    }

    public void OnTeacherAccount()
    {
        ClosePanel();
        if (HM_DataController.currentChildUser != null)
        {
            HM_PluginController.OpenTeacherAdmin();
        }
        else
        {
            HM_Content.currentMainContent.CloseContent(HM_Scripts.contentCodeTeacher);
        }
    }
    public void OnGoEleve()
    {
        OnSwitchPanelOption();
        HM_DataController.currentTeacherAccount = null;
        HM_DataController.newTeacherAccountTemp = null;
        HM_PluginController.navContext = HM_PluginController.NAV_CONTEXT.NONE;
        HM_Content.currentMainContent.CloseContent(HM_Scripts.contentProfil);
    }

    public void OnGestionLettre()
    {
        OnSwitchPanelOption();
        HM_DataController.currentTeacherAccount = null;
        HM_DataController.newTeacherAccountTemp = null;
        HM_PluginController.navContext = HM_PluginController.NAV_CONTEXT.LETTRE;
        HM_Content.currentMainContent.CloseContent(HM_Scripts.contentCodeTeacher);
        //HM_Scripts.pluginController.StartPlugin(HM_Scripts.popupAlertDisconnect);
    }

    public void OnGestionLot()
    {
        OnSwitchPanelOption();
        HM_DataController.currentTeacherAccount = null;
        HM_DataController.newTeacherAccountTemp = null;
        HM_PluginController.navContext = HM_PluginController.NAV_CONTEXT.LOT;
        HM_Content.currentMainContent.CloseContent(HM_Scripts.contentCodeTeacher);
        //HM_Scripts.pluginController.StartPlugin(HM_Scripts.popupAlertDisconnect);
    }

    public void OnGestionProf()
    {
        OnSwitchPanelOption();
        HM_DataController.currentTeacherAccount = null;
        HM_DataController.newTeacherAccountTemp = null;
        HM_PluginController.navContext = HM_PluginController.NAV_CONTEXT.NEW_TEACHER;
        HM_Content.currentMainContent.CloseContent(HM_Scripts.contentCodeTeacher);
        //HM_Scripts.pluginController.StartPlugin(HM_Scripts.popupAlertDisconnect);
    }
    public void OnSyncrho()
    {
        OnSwitchPanelOption();
        HM_DataController.currentTeacherAccount = null;
        HM_DataController.newTeacherAccountTemp = null;
        HM_PluginController.navContext = HM_PluginController.NAV_CONTEXT.SYNCHRO;
        HM_Content.currentMainContent.CloseContent(HM_Scripts.contentCodeTeacher);
        //HM_Scripts.pluginController.StartPlugin(HM_Scripts.popupAlertDisconnect);
    }
}

