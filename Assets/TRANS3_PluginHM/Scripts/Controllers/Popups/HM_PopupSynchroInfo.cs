using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HM_PopupSynchroInfo : HM_Content
{
    public Button backButton;

    public Button confirmButton;

    public override int InitContent()
    {
        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(() =>
        {
            this.CloseContent();
        });

        confirmButton.onClick.RemoveAllListeners();
        confirmButton.onClick.AddListener(() =>
        {
            if(HM_PluginController.navContext  == HM_PluginController.NAV_CONTEXT.LETTRE)
            {
                CloseContent(HM_Scripts.contentDeviceColor);
            }
            else if (HM_PluginController.navContext == HM_PluginController.NAV_CONTEXT.LOT)
            {
                CloseContent(HM_Scripts.contentGroupsChoice);
            }
            else if (HM_PluginController.navContext == HM_PluginController.NAV_CONTEXT.NEW_TEACHER)
            {
                HM_DataController.model.teacherAccounts.Add(HM_DataController.newTeacherAccountTemp);

                HM_DataController.model.Save();

                HM_Scripts.optionPanel.UpdateData();

                CloseContent(HM_Scripts.contentProfil);
            }
            else if (HM_PluginController.navContext == HM_PluginController.NAV_CONTEXT.SYNCHRO)
            {
                CloseContent(HM_Scripts.contentProfil);
            }
            else
            {
                CloseContent(HM_Scripts.contentProfil);
            }
        });

        return 0;
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }

    public override void OnOpenedContent()
    {

    }


}
