using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HM_PopupAfterDissocier : HM_Content
{
    public Button confirmButton;

    public override int InitContent()
    {
        confirmButton.onClick.RemoveAllListeners();
        confirmButton.onClick.AddListener(() =>
        {
            Application.Quit();
        });
        return 0;
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }

    public override void OnOpenedContent()
    {
        return;
    }
}
