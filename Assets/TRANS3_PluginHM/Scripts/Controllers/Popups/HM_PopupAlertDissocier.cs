using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HM_PopupAlertDissocier : HM_Content
{
    public Button backButton;

    public Button confirmButton;

    public override int InitContent()
    {
        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(() =>
        {
            this.CloseContent(HM_Scripts.contentProfil);
        });

        confirmButton.onClick.RemoveAllListeners();
        confirmButton.onClick.AddListener(() =>
        {
            foreach(HM_TeacherAccount teacherAccount in HM_DataController.model.teacherAccounts)
            {
                if(teacherAccount == HM_DataController.currentTeacherAccount)
                {
                    HM_DataController.model.teacherAccounts.Remove(teacherAccount);
                    break;
                }
            }

            if(HM_DataController.model.teacherAccounts.Count == 0)
            {
                HM_DataController.model.currentColorID = -1;
            }

            HM_DataController.model.Save();

            HM_DataController.currentTeacherAccount = null;

            CloseContent(HM_Scripts.popupAfterDissocier);
        });

        return 0;
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }

    public override void OnOpenedContent()
    {

    }


}
