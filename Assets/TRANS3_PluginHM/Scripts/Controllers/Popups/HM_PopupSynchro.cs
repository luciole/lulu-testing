using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using UnityEngine.SceneManagement;

public class HM_PopupSynchro : HM_Content
{
    public Tween loadingTween = null;
    public Image loadingImage;

    public TextMeshProUGUI successText;
    public TextMeshProUGUI errorText;

    public override int InitContent()
    {
        loadingImage.gameObject.SetActive(true);
        successText.gameObject.SetActive(false);
        errorText.gameObject.SetActive(false);
        if (loadingTween == null)
        {
            loadingTween = loadingImage.transform.DOLocalRotate(new Vector3(0, 0, -360), 1.5f, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart).SetUpdate(true);
        }
        return 0;
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }

    public override void OnOpenedContent()
    {
        return;
    }

    public void OnSuccessMessage()
    {
        loadingImage.gameObject.SetActive(false);
        successText.gameObject.SetActive(true);

        HM_Model.Load();

        HM_Scripts.optionPanel.UpdateData();

        DOVirtual.DelayedCall(2.0f, () =>
        {
            this.CloseContent(HM_Scripts.contentProfil);
        });
    }

    public void OnErrorMessage(string msg)
    {
        loadingImage.gameObject.SetActive(false);
        errorText.text = msg;
        errorText.gameObject.SetActive(true);

        DOVirtual.DelayedCall(2.0f, () =>
        {
            this.CloseContent();
        });
    }
}
