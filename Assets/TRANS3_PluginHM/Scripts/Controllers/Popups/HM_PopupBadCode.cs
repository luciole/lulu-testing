using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using UnityEngine.UI;

//popup sp�cifique � HM_ContentCode pour afficher une erreur de code non valide
public class HM_PopupBadCode : HM_Content
{
    public HM_ContentCode contentCode;
    public Button confirmButton;

    //Help button pour l'instant cach�, permettra � terme d'afficher une page d'information
    public Button helpButton;

    public override int InitContent()
    {
        confirmButton.onClick.RemoveAllListeners();
        confirmButton.onClick.AddListener(()=>
        {
            CloseContent();
        });
        return 0;
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        //remise � z�ro du contenu principal de demande de code lors de la fermeture de la popup
        contentCode.InitContent();
        return 0;
    }

    public override void OnOpenedContent()
    {
        return;
    }
}
