using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Text;
using System.Net;
using System.IO;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System.Linq;
using System;

public class HM_DebugPanel : MonoBehaviour
{
    public Button buttonSwitchPanel;
    public TextMeshProUGUI debugText;

    private CanvasGroup canvasGroup;

    private int cptButton = 0;
    private float lastTimeButton = 0;

    public Button buttonEnv1;
    public Button buttonEnv2;
    public Button buttonEnv3;
    public Button buttonLoadSave;

    // Start is called before the first frame update
    void Awake()
    {
        Application.logMessageReceived += HandleLog;
        canvasGroup = GetComponent<CanvasGroup>();

        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;

#if UNITY_EDITOR
        buttonEnv1?.gameObject.SetActive(false);
        buttonEnv2?.gameObject.SetActive(false);
        buttonEnv3?.gameObject.SetActive(false);
#endif
    }

    // Update is called once per frame
    void Update()
    {
        if (cptButton > 0 && Time.realtimeSinceStartup - lastTimeButton > 2.0f)
        {
            cptButton = 0;
        }
    }

    public void OnButtonClick()
    {
        cptButton++;
        lastTimeButton = Time.realtimeSinceStartup;

        if (cptButton == 10)
        {
            SwitchPanel();
            cptButton = 0;
        }
    }

    public void OnLoadSave()
    {
        string filePath = System.IO.Path.Combine(Application.persistentDataPath, "HM_saveData2.json");
        if (System.IO.File.Exists(filePath))
        {
            int result = HM_Model.Load(System.IO.File.ReadAllText(filePath));

            if(result == 0)
            {
                Debug.Log("Chargement r�ussi");
                HM_DataController.model.Save();
            }
            else
            {
                Debug.Log("Erreur dans le chargement du fichier");
            }
        }
        else
        {
            Debug.Log("Pas de fichier � charger");
        }
    }

    public void SwitchPanel()
    {
        if (canvasGroup.alpha == 0)
        {
            canvasGroup.alpha = 1;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;
        }
        else
        {
            canvasGroup.alpha = 0;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        }
    }

    public void HandleLog(string logString, string stackTrace, LogType type)
    {
        if (type == LogType.Log || type == LogType.Error)
        {
            debugText.text += logString + "\n";
        }
    }
    public void CleanLog()
    {
        debugText.text = "";
    }

    public void DebugEnvDevelop()
    {
        HM_NetworkController.instance.SwitchEnv(HM_NetworkController.ENV.DEVELOP);
    }
    public void DebugEnvPreprod()
    {
        HM_NetworkController.instance.SwitchEnv(HM_NetworkController.ENV.PREPROD);
    }
    public void DebugEnvProd()
    {
        HM_NetworkController.instance.SwitchEnv(HM_NetworkController.ENV.PROD);
    }
}