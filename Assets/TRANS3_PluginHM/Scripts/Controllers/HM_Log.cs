using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public static class HM_Log
{
    public static void ReplaceTextFile(string fileName, string content, bool withDate = false)
    {
        try
        {
            // Chemin complet du fichier
            string filePath = System.IO.Path.Combine(Application.persistentDataPath, fileName);

            // Cr�er ou remplacer le fichier texte
            if(withDate)
            { 
                File.WriteAllText(filePath, DateTime.UtcNow.ToString("yyyy-MM-ddTHH-mm-ssK") + " | " + content);
            }
            else
            {
                File.WriteAllText(filePath, content);
            }
        }
        catch(Exception e)
        {
            Debug.LogError("ReplaceTextFile Error / "+fileName + " / " + content + " / " + e.Message);
        }
    }

    public static void AppendTextToFile(string fileName, string content)
    {
        try
        {
            // Chemin complet du fichier
            string filePath = System.IO.Path.Combine(Application.persistentDataPath, fileName);

            // Ajouter le contenu au fichier existant (ou le cr�er s'il n'existe pas)
            using (StreamWriter sw = File.AppendText(filePath))
            {
                sw.WriteLine(DateTime.UtcNow.ToString("yyyy-MM-ddTHH-mm-ssK") + " | " + content);
            }
        }
        catch (Exception e)
        {
            Debug.LogError("AppendTextToFile Error / " + fileName + " / " + content + " / " + e.Message);
        }
    }
}
