﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class HM_ContentTeacherAccount : HM_Content
{
    public GameObject buttonLiberer;
    public Image deviceColor;

    public Transform itemTemplate;

    public TextMeshProUGUI otherUserTxt;

    public TextMeshProUGUI dateLastSynchroTxt;

    public override int InitContent()
    {
        /*if (pluginController.sceneStart)
        {
            HM_Scripts.optionPanel.backMenuButton.gameObject.SetActive(true);
            HM_Scripts.optionPanel.teacherAccountButton.gameObject.SetActive(false);
            HM_Scripts.optionPanel.disconnectButton.gameObject.SetActive(false);

            if (!HM_Scripts.optionPanel.isButtonVisible)
            {
                HM_Scripts.optionPanel.ShowButton();
            }
        }*/

        if(HM_DataController.currentTeacherAccount.dateLastSynchro == null || HM_DataController.currentTeacherAccount.dateLastSynchro.Length == 0)
        {
            dateLastSynchroTxt.text = "Jamais";
        }
        else
        {
            string dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'";
            DateTime t1 = DateTime.ParseExact(HM_DataController.currentTeacherAccount.dateLastSynchro, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
            DateTime t2 = DateTime.UtcNow;
            TimeSpan Ts = t2 - t1;
            if (Ts.TotalDays < 1)
            {
                if (Ts.TotalHours >= 1)
                {
                    if (Ts.TotalHours < 2)
                    {
                        dateLastSynchroTxt.text = "Il y a 1 heure";
                    }
                    else
                    {
                        dateLastSynchroTxt.text = "Il y a " + (int)Ts.TotalHours + " heures";
                    }
                }
                else if (Ts.TotalMinutes >= 1)
                {
                    if (Ts.TotalMinutes < 2)
                    {
                        dateLastSynchroTxt.text = "Il y a 1 minute";
                    }
                    else
                    {
                        dateLastSynchroTxt.text = "Il y a " + (int)Ts.TotalMinutes + " minutes";
                    }
                }
                else 
                {
                    if (Ts.TotalSeconds < 2)
                    {
                        dateLastSynchroTxt.text = "Il y a 1 seconde";
                    }
                    else
                    {
                        dateLastSynchroTxt.text = "Il y a " + (int)Ts.TotalSeconds + " secondes";
                    }
                }
            }
            else
            {
                dateLastSynchroTxt.text = t1.ToString("MM/dd/yyyy");
            }
        }

        switch (HM_DataController.model.currentColorID)
        {
            case 1:
                deviceColor.color = new Color32(246, 115, 172, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "A";
                break;
            case 2:
                deviceColor.color = new Color32(155, 47, 174, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "B";
                break;
            case 3:
                deviceColor.color = new Color32(64, 84, 178, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "C";
                break;
            case 4:
                deviceColor.color = new Color32(29, 170, 241, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "D";
                break;
            case 5:
                deviceColor.color = new Color32(31, 188, 210, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "E";
                break;
            case 6:
                deviceColor.color = new Color32(80, 174, 85, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "F";
                break;
            case 7:
                deviceColor.color = new Color32(205, 218, 73, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "G";
                break;
            case 8:
                deviceColor.color = new Color32(253, 192, 47, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "H";
                break;
            case 9:
                deviceColor.color = new Color32(164, 125, 112, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "I";
                break;
            case 10:
                deviceColor.color = new Color32(97, 125, 138, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "J";
                break;
            case 11:
                deviceColor.color = new Color32(255, 140, 120, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "K";
                break;
            case 12:
                deviceColor.color = new Color32(0, 121, 135, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "L";
                break;
            case 13:
                deviceColor.color = new Color32(30, 223, 145, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "M";
                break;
            case 14:
                deviceColor.color = new Color32(201, 130, 255, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "N";
                break;
            case 15:
                deviceColor.color = new Color32(255, 49, 49, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "O";
                break;
            case 16:
                deviceColor.color = new Color32(68, 74, 255, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "P";
                break;
            case 17:
                deviceColor.color = new Color32(255, 110, 13, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Q";
                break;
            case 18:
                deviceColor.color = new Color32(84, 67, 59, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "R";
                break;
            case 19:
                deviceColor.color = new Color32(37, 96, 58, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "S";
                break;
            case 20:
                deviceColor.color = new Color32(36, 28, 110, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "T";
                break;
            case 21:
                deviceColor.color = new Color32(36, 28, 110, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "U";
                break;
            case 22:
                deviceColor.color = new Color32(36, 28, 110, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "V";
                break;
            case 23:
                deviceColor.color = new Color32(36, 28, 110, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "W";
                break;
            case 24:
                deviceColor.color = new Color32(36, 28, 110, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "X";
                break;
            case 25:
                deviceColor.color = new Color32(36, 28, 110, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Y";
                break;
            case 26:
                deviceColor.color = new Color32(36, 28, 110, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Z";
                break;
            case 27:
                deviceColor.color = new Color32(36, 28, 110, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Δ";
                break;
            case 28:
                deviceColor.color = new Color32(36, 28, 110, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Θ";
                break;
            case 29:
                deviceColor.color = new Color32(36, 28, 110, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Ξ";
                break;
            case 30:
                deviceColor.color = new Color32(36, 28, 110, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Φ";
                break;
            case 31:
                deviceColor.color = new Color32(36, 28, 110, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Ψ";
                break;
            case 32:
                deviceColor.color = new Color32(36, 28, 110, 255);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Ω";
                break;
            case -1:
                deviceColor.color = new Color32(255, 255, 255, 0);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "-";
                break;
            default:
                deviceColor.color = new Color32(255, 255, 255, 0);
                deviceColor.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "-";
                break;
        }

        List<GameObject> destroyList = new List<GameObject>();
        for (int i=0; i<itemTemplate.parent.childCount; i++)
        {
            if(itemTemplate.parent.GetChild(i).gameObject.activeSelf)
            {
                destroyList.Add(itemTemplate.parent.GetChild(i).gameObject);
            }
        }
        foreach(GameObject go in destroyList)
        {
            DestroyImmediate(go);
        }

        int cptGroupActive = 0;
        int cptGroupInactive = 0;

        GameObject newGo2 = GameObject.Instantiate(itemTemplate.gameObject, itemTemplate.parent, false);
        newGo2.SetActive(true);
        int cptUser2 = 0;
        //Debug.Log(HM_DataController.currentTeacherAccount.childUsers.Count);
        foreach (HM_ChildUser child in HM_DataController.currentTeacherAccount.childUsers)
        {
            //Debug.Log(child.loginCode);
            if (!child.isGeneric)
            {
                cptUser2++;
            }
        }
        newGo2.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Créé en ligne";
        if(cptUser2 == 0)
        {
            newGo2.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Aucun élève";
        }
        else if (cptUser2 == 1)
        {
            newGo2.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "1 élève";
        }
        else
        {
            newGo2.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "" + cptUser2 + " élèves";
        }

        HM_DataController.currentTeacherAccount.genericGroupsUsed.Sort();
        foreach (int groupID in HM_DataController.currentTeacherAccount.genericGroupsUsed)
        {
            GameObject newGo = GameObject.Instantiate(itemTemplate.gameObject, itemTemplate.parent, false);
            newGo.SetActive(true);

            switch(groupID)
            {
                case 1:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CP - Lot1";
                    break;
                case 2:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CP - Lot2";
                    break;
                case 3:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CP - Lot3";
                    break;
                case 4:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CE1 - Lot1";
                    break;
                case 5:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CE1 - Lot2";
                    break;
                case 6:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CE1 - Lot3";
                    break;
                case 7:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CE2 - Lot1";
                    break;
                case 8:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CE2 - Lot2";
                    break;
                case 9:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CE2 - Lot3";
                    break;
                case 10:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CM1 - Lot1";
                    break;
                case 11:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CM1 - Lot2";
                    break;
                case 12:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CM1 - Lot3";
                    break;
                case 13:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CM2 - Lot1";
                    break;
                case 14:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CM2 - Lot2";
                    break;
                case 15:
                    newGo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CM2 - Lot3";
                    break;
            }


            int cptUser = 0;
            foreach (HM_ChildUser child in HM_DataController.currentTeacherAccount.childUsers)
            {
                if (child.genericGroup == groupID && child.activeUser)
                {
                    cptUser++;
                }
            }

            if (cptUser == 0)
            {
                cptGroupInactive++;
                newGo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Aucun élève";
            }
            else if(cptUser == 1)
            {
                cptGroupActive++;
                newGo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "1 élève";
            }
            else
            {
                cptGroupActive++;
                newGo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = ""+cptUser+" élèves";
            }
        }

        if (cptGroupInactive > 0)
        {
            buttonLiberer.SetActive(true);
        }
        else
        {
            buttonLiberer.SetActive(false);
        }
        /*if (cptGroupActive > 0 && cptGroupInactive > 0)
        {
            buttonLiberer.SetActive(true);
        }
        else
        {
            buttonLiberer.SetActive(false);
        }*/

        if (HM_DataController.model.teacherAccounts.Count == 1)
        {
            otherUserTxt.text = "";
        }
        else if (HM_DataController.model.teacherAccounts.Count == 2)
        {
            otherUserTxt.text = "( Périphérique utilisé par 1 autre enseignant )";
        }
        else
        {
            otherUserTxt.text = "( Périphérique utilisé par " + (HM_DataController.model.teacherAccounts.Count-1) + " autres enseignants )";
        }

        //pluginController.colorDeviceInfo.gameObject.SetActive(false);

        return 0;
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }

    public override void OnOpenedContent()
    {
    } 

    public void OnEditColor()
    {
        CloseContent(HM_Scripts.contentDeviceColor);
    }

    public void OnDissocier()
    {
        HM_Scripts.popupAlertDissocier.OpenContent();
    }

    public void OnAjouter()
    {
        CloseContent(HM_Scripts.contentGroupsChoice);
    }

    public void OnLiberer()
    {
        List<int> groupsToDelete = new List<int>();
        List<HM_ChildUser> childsToDelete = new List<HM_ChildUser>();

        foreach (int groupID in HM_DataController.currentTeacherAccount.genericGroupsUsed)
        {
            bool used = false;
            foreach (HM_ChildUser child in HM_DataController.currentTeacherAccount.childUsers)
            {
                if(child.genericGroup == groupID && child.activeUser)
                {
                    used = true;
                    break;
                }
            }

            if(!used)
            {
                groupsToDelete.Add(groupID);
            }
        }

        foreach (int groupID in groupsToDelete)
        {
            foreach (HM_ChildUser child in HM_DataController.currentTeacherAccount.childUsers)
            {
                if (child.genericGroup == groupID)
                {
                    childsToDelete.Add(child);
                }
            }
            HM_DataController.currentTeacherAccount.genericGroupsUsed.Remove(groupID);
        }

        foreach (HM_ChildUser child in childsToDelete)
        {
            HM_DataController.currentTeacherAccount.childUsers.Remove(child);
        }

        HM_DataController.model.Save();

        InitContent();
    }
}
