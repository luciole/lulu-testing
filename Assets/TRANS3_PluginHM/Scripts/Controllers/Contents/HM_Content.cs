using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//classe abstraite g�rant l'affichage de contenu pour le Plugin HM
public abstract class HM_Content : MonoBehaviour
{
    public static CanvasGroup contentBackgroundCG = null;
    public static CanvasGroup popupBackgroundCG = null;

    public static HM_Content currentMainContent = null;
    public static HM_Content currentPopupContent = null;

    public HM_PluginController pluginController = null;

    //chaque contenu est forc�ment li� � un canvas group qui est sur le gameobject
    private CanvasGroup mainCanvasGroup_cg;

    //vitesse des animations
    private float transitionSpeed = 0.3f;

    public enum CONTENT_TYPE
    {
        MAIN,
        POPUP,
    }

    public CONTENT_TYPE contentType = CONTENT_TYPE.MAIN;

    public void Awake()
    {
        //r�cup�ration du canvas group principal du contenu (composant obligatoire)
        mainCanvasGroup_cg = GetComponent<CanvasGroup>();

        //d�sactivation de l'ensemble des contenus au lancement, c'est au controller de g�rer l'activation
        mainCanvasGroup_cg.alpha = 0;
        mainCanvasGroup_cg.interactable = false;
        mainCanvasGroup_cg.blocksRaycasts = false;
    }

    //appel avant affichage contenu
    public abstract int InitContent();

    //appel lorsque l'animation d'affichage est termin�e
    public abstract void OnOpenedContent();

    //lance l'affichage du contenu
    public void OpenContent()
    {
        //on ne devrait jamais demander � une seconde popup d'en remplacer une d�j� pr�sente
        if (contentType == CONTENT_TYPE.POPUP && currentPopupContent != null)
        {
            Debug.LogWarning("Popup " + this.GetType().Name + " tente de s'ouvrir alors que " + currentPopupContent.GetType().Name + " d�j� ouverte. Non autoris� !");
            return;
        }

        //on ne devrait jamais tenter d'ouvrir un nouveau contenu principal tant qu'une popup est active
        if (contentType == CONTENT_TYPE.MAIN && currentPopupContent != null)
        {
            Debug.LogWarning("MainContent " + this.GetType().Name + " tente de s'ouvrir alors que Popup " + currentPopupContent.GetType().Name + " encore ouverte. Non autoris� !");
            return;
        }

        if(HM_Scripts.optionPanel != null  && HM_Scripts.optionPanel.isOpen)
        {
            HM_Scripts.optionPanel.ClosePanel();
        }

        //callback pr�-affichage
        int initResult = InitContent();

        //si renvoit d'une erreur � l'initialisation, on arr�te tout
        if (initResult < 0)
        {
            Debug.LogError("InitContent " + this.GetType().Name + " - ERROR " + initResult);
            return;
        }

        //si un contenu principal existait d�j�, on doit le fermer et faire la transition
        if (contentType == CONTENT_TYPE.MAIN && currentMainContent != null)
        {
            CloseContent(this);
            return;
        }

        //si il s'agit d'un 1er affichage, l'on doit activer aussi le background
        if (contentBackgroundCG == null || contentBackgroundCG.alpha == 0)
        {
            //pr�caution de recherche du background si n'a pas �t� g�r� par le controller
            if (contentBackgroundCG == null)
            {
                contentBackgroundCG = GameObject.FindGameObjectsWithTag("HM_ContentBackground")[0].GetComponent<CanvasGroup>();
            }

            //animation background
            contentBackgroundCG.DOFade(1.0f, transitionSpeed).SetEase(Ease.InOutSine).SetUpdate(true).OnComplete(() =>
            {
                contentBackgroundCG.interactable = true;
                contentBackgroundCG.blocksRaycasts = true;
            });
        }

        switch (contentType)
        {
            case CONTENT_TYPE.MAIN:
                currentMainContent = this;
                break;
            case CONTENT_TYPE.POPUP:
                currentPopupContent = this;
                break;
        }

        if (contentType == CONTENT_TYPE.POPUP)
        {
            //si popup, on assure de d�sactiver l'interraction du contenu principal
            if(currentMainContent != null)
                currentMainContent.mainCanvasGroup_cg.interactable = false;

            //pr�caution de recherche du background si n'a pas �t� g�r� par le controller
            if (popupBackgroundCG == null)
            {
                popupBackgroundCG = GameObject.FindGameObjectsWithTag("HM_PopupBackground")[0].GetComponent<CanvasGroup>();
            }

            //animation background
            popupBackgroundCG.DOFade(1.0f, transitionSpeed).SetEase(Ease.InOutSine).SetUpdate(true).OnComplete(() =>
            {
                popupBackgroundCG.interactable = true;
                popupBackgroundCG.blocksRaycasts = true;
            });
        }

        //animation affichage du contenu demand�
        mainCanvasGroup_cg.DOFade(1.0f, transitionSpeed).SetEase(Ease.InOutSine).SetUpdate(true).OnComplete(() =>
        {
            //activation interaction contenu
            mainCanvasGroup_cg.interactable = true;
            mainCanvasGroup_cg.blocksRaycasts = true;

            //callback post-affichage
            OnOpenedContent();
        });

    }

    //appel � la fin d'animation de fermeture du contenu
    public abstract int OnClosedContent(HM_Content nextContent = null);

    //lance la fermeture du contenu, et potentiellement transition vers un autre
    public void CloseContent(HM_Content nextContent = null)
    {
        //on ne devrait jamais tenter d'ouvrir un nouveau contenu principal tant qu'une popup est active
        if (contentType == CONTENT_TYPE.MAIN && currentPopupContent != null)
        {
            Debug.LogWarning("Demande de transition de contenu MAIN " + currentMainContent.GetType().Name + " alors que contenu POPUP " + currentPopupContent.GetType().Name + " encore active. Non autoris� !");
            return;
        }

        //on essaye de demander la transition d'un contenu principal vers un contenu de type Popup. Non autoris� !
        if (contentType == CONTENT_TYPE.MAIN && nextContent != null && nextContent.contentType == CONTENT_TYPE.POPUP)
        {
            Debug.LogWarning("Demande de transition de contenu MAIN " + currentMainContent.GetType().Name + " vers contenu POPUP " + nextContent.GetType().Name + ". Non autoris� !");
            return;
        }

        if (HM_Scripts.optionPanel != null && HM_Scripts.optionPanel.isOpen)
        {
            HM_Scripts.optionPanel.ClosePanel();
        }

        //d�sactivation des interraction possible du contenu
        mainCanvasGroup_cg.interactable = false;
        mainCanvasGroup_cg.blocksRaycasts = false;

        if (contentType == CONTENT_TYPE.POPUP && (nextContent == null || nextContent.contentType == CONTENT_TYPE.MAIN))
        {
            //si on ferme une popup et qu'on n'en ouvre pas une autre, l'on doit enlever le background Popup
            if(currentMainContent != null)
                currentMainContent.mainCanvasGroup_cg.interactable = false;

            //pr�caution de recherche du background si n'a pas �t� g�r� par le controller
            if (popupBackgroundCG == null)
            {
                popupBackgroundCG = GameObject.FindGameObjectsWithTag("HM_PopupBackground")[0].GetComponent<CanvasGroup>();
            }

            //animation background
            popupBackgroundCG.DOFade(0.0f, transitionSpeed).SetEase(Ease.InOutSine).SetUpdate(true).OnComplete(() =>
            {
                popupBackgroundCG.interactable = false;
                popupBackgroundCG.blocksRaycasts = false;
            });
        }
        
        
        if((contentType == CONTENT_TYPE.POPUP && currentMainContent == null) || (contentType == CONTENT_TYPE.MAIN && nextContent == null))
        {
            //si on ferme une contenu sans transition vers un nouveau, l'on doit fermer le bacground du plugin et le d�sactiver

            //pr�caution de recherche du background si n'a pas �t� g�r� par le controller
            if (contentBackgroundCG == null)
            {
                contentBackgroundCG = GameObject.FindGameObjectsWithTag("HM_ContentBackground")[0].GetComponent<CanvasGroup>();
            }

            //animation background
            contentBackgroundCG.DOFade(0.0f, transitionSpeed).SetEase(Ease.InOutSine).SetUpdate(true).OnComplete(() =>
            {
                contentBackgroundCG.interactable = false;
                contentBackgroundCG.blocksRaycasts = false;

                //d�sactivation du plugin HM
                pluginController.StopPlugin();
            });
        }

        //animation fermeture du contenu demand�
        mainCanvasGroup_cg.DOFade(0.0f, transitionSpeed).SetEase(Ease.InOutSine).SetUpdate(true).OnComplete(() =>
        {
            //si traitement sp�cifique pas de transition
            if(OnClosedContent(nextContent) < 0)
            {
                return;
            }

            if (contentType == CONTENT_TYPE.MAIN)
            {
                currentMainContent = null;

                //on lance le prochain contenu si il existe
                if (nextContent != null)
                    nextContent.OpenContent();
            }
            else
            {
                currentPopupContent = null;

                //on lance le prochain contenu si il existe
                if (nextContent != null)
                {
                    if (nextContent.contentType == CONTENT_TYPE.POPUP)
                    {
                        nextContent.OpenContent();
                    }
                    else if (nextContent.contentType == CONTENT_TYPE.MAIN)
                    {
                        //si on ferme une popup tout en demandant un nouveau contenu principal, alors l'on doit lancer la fermeture du contenu principal actuel
                        if (currentMainContent != null)
                            currentMainContent.CloseContent(nextContent);
                        else
                            nextContent.OpenContent();
                    }
                }
                else
                {
                    //si l'on a ferm� une popup sans transition, on r�active l'interaction du contenu principal actuel
                    if (currentMainContent != null)
                        currentMainContent.mainCanvasGroup_cg.interactable = true;
                }
            }
        });
    }
}
