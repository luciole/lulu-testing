using Amazon.CognitoIdentity;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Extensions.CognitoAuthentication;
using DG.Tweening;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HM_ContentLoginAWS : HM_Content
{
    public TMP_InputField textCode;
    public Image loadingImage;
    public Tween loadingTween = null;
    public TMP_InputField passwordIF;
    public TextMeshProUGUI title;
    public TextMeshProUGUI info;
    public Button buttonLogin;
    public Button buttonSkip;
    public Button buttonBack;
    public TextMeshProUGUI textCodeInfo;

    private HM_TeacherAccount currentTeacherAccount = null;

    [HideInInspector]
    public int navID = 0;

    public override int InitContent()
    {
        info.text = "";
        passwordIF.text = "";
        textCode.text = "";

        if (loadingTween == null)
        {
            loadingImage.transform.eulerAngles = Vector3.zero;
            loadingTween = loadingImage.transform.DOLocalRotate(new Vector3(0, 0, -360), 1.5f, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart).SetUpdate(true);
        }

        loadingImage.enabled = false;

        currentTeacherAccount = null;

        if (HM_DataController.currentTeacherAccount != null)
        {
            currentTeacherAccount = HM_DataController.currentTeacherAccount;
            //buttonBack.gameObject.SetActive(true);
            title.text = "Mot de passe";
        }
        else if (HM_DataController.newTeacherAccountTemp != null)
        {
            currentTeacherAccount = HM_DataController.newTeacherAccountTemp;
            //buttonBack.gameObject.SetActive(false);
            title.text = "Mot de passe";
        }

        if (currentTeacherAccount == null)
        {
            //ne devrait jamais arriver
            Debug.LogError("Pas de compte professeur trouv� pour initialiser le login");
            buttonSkip.interactable = true;
            buttonBack.interactable = true;
        }
        else
        {
            textCode.text = currentTeacherAccount.teacherCode.ToUpper();

            textCodeInfo.text = "Votre identifiant : <b>"+ currentTeacherAccount.teacherCode.ToUpper().Substring(0,3) + " " + currentTeacherAccount.teacherCode.ToUpper().Substring(3, 3) + "</b>";
            buttonLogin.interactable = true;
            buttonSkip.interactable = true;
            buttonBack.interactable = true;
            passwordIF.interactable = true;
        }

#if UNITY_ANDROID //&& !UNITY_EDITOR
        keyboardOpen = false;
#endif
        return 0;
    }

    public override void OnOpenedContent()
    {

    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }

#if UNITY_ANDROID //&& !UNITY_EDITOR
    private bool keyboardOpen = false;
#endif
    private void Update()
    {
#if UNITY_ANDROID //&& !UNITY_EDITOR
        if (!keyboardOpen && TouchScreenKeyboard.visible == true)
        {
            keyboardOpen = true;

            this.gameObject.transform.DOLocalMoveY(90, 0.2f).SetEase(Ease.InOutSine);
        }
        else if (keyboardOpen && TouchScreenKeyboard.visible == false)
        {
            keyboardOpen = false;

            this.gameObject.transform.DOLocalMoveY(0, 0.2f).SetEase(Ease.InOutSine);
        }
#endif
    }

    public void OnButtonSkipClick()
    {
        loadingTween.Kill();
        loadingTween = null;

        //todo popup info
        HM_Scripts.popupSynchroInfo.OpenContent();

        /*if (HM_DataController.currentTeacherAccount != null)
        {
            CloseContent(HM_Scripts.contentTeacherAccount);
        }
        else if (HM_DataController.newTeacherAccountTemp != null)
        {
            if (HM_DataController.model.teacherAccounts.Count > 0)
            {
                CloseContent(HM_Scripts.contentGroupsChoice);
            }
            else
            {
                CloseContent(HM_Scripts.contentDeviceColor);
            }
        }
        else
        {
            //ne devrait jamais arriver, on renvoit sur la page de code prof
            CloseContent(HM_Scripts.contentCodeTeacher);
        }*/
    }
    public void OnButtonBackClick()
    {
        loadingTween.Kill();
        loadingTween = null;
        HM_DataController.currentTeacherAccount = null;
        CloseContent(HM_Scripts.contentProfil);
    }

    public async void OnButtonLoginClick()
    {
        if(Application.internetReachability == NetworkReachability.NotReachable)
        {
            buttonLogin.interactable = true;
            buttonSkip.interactable = true;
            buttonBack.interactable = true;
            passwordIF.interactable = true;
            passwordIF.text = "";
            info.text = "";
            info.color = Color.red;
            HM_Scripts.popupWifiAlert.OpenContent();
            Debug.Log("Login InterfaceEnseignant error");
            return;
        }

        buttonLogin.interactable = false;
        buttonSkip.interactable = false;
        buttonBack.interactable = false;
        passwordIF.interactable = false;
        loadingImage.enabled = true;
        info.text = "";

        bool successfulLogin = await this.Login(currentTeacherAccount.teacherCode, passwordIF.text);

        loadingImage.enabled = false;
        if (successfulLogin)
        {
            loadingTween.Kill();
            loadingTween = null;
            info.text = "Authentification r�ussie";
            info.color = new Color32(55,203,139,255);
            HM_DataController.model.Save();
            Debug.Log("Login InterfaceEnseignant success");

            if (HM_DataController.newTeacherAccountTemp != null)
            {
                HM_DataController.model.teacherAccounts.Add(HM_DataController.newTeacherAccountTemp);

                HM_DataController.model.Save();

                HM_Scripts.optionPanel.UpdateData();
            }

            DOVirtual.DelayedCall(3.0f, () =>
            {
                CloseContent(HM_Scripts.contentSynchroAWS);


                /*if (HM_DataController.currentTeacherAccount != null)
                {
                    CloseContent(HM_Scripts.contentSynchroAWS);
                }
                else if (HM_DataController.model.teacherAccounts.Count == 0)
                {
                    CloseContent(HM_Scripts.contentDeviceColor);
                }
                else
                {
                    CloseContent(HM_Scripts.contentGroupsChoice);
                }*/
            });
        }
        else
        {
            buttonLogin.interactable = true;
            buttonSkip.interactable = true;
            buttonBack.interactable = true;
            passwordIF.interactable = true;
            //passwordIF.text = "";
            info.text = "Le mot de passe ne correspond pas � l'identifiant";
            info.color = Color.red;
            Debug.Log("Login InterfaceEnseignant error");
        }
    }

    public async Task<bool> Login(string identifiant, string password)
    {

        CognitoUserPool userPool = new CognitoUserPool(HM_NetworkController.userPoolId, HM_NetworkController.AppClientID, HM_NetworkController.providerAWS);
        currentTeacherAccount.dataAWS._user = new CognitoUser(identifiant, HM_NetworkController.AppClientID, userPool, HM_NetworkController.providerAWS);

        InitiateSrpAuthRequest authRequest = new InitiateSrpAuthRequest()
        {
            Password = password
        };

        try
        {
            AuthFlowResponse authFlowResponse = await currentTeacherAccount.dataAWS._user.StartWithSrpAuthAsync(authRequest).ConfigureAwait(false);

            //userID � priori inutile pour l'instant
            //currentTeacherAccount.dataAWS._userid = await GetUserIdFromProvider(authFlowResponse.AuthenticationResult.AccessToken);

            currentTeacherAccount.dataAWS._idToken = authFlowResponse.AuthenticationResult.IdToken;
            currentTeacherAccount.dataAWS._accessToken = authFlowResponse.AuthenticationResult.AccessToken;
            currentTeacherAccount.dataAWS._refreshToken = authFlowResponse.AuthenticationResult.RefreshToken;

            //Debug.Log(currentTeacherAccount.dataAWS._idToken);

            currentTeacherAccount.dataAWS.refreshTime = DateTime.Now;

            CognitoAWSCredentials _cognitoAWSCredentials = currentTeacherAccount.dataAWS._user.GetCognitoAWSCredentials(HM_NetworkController.IdentityPool, HM_NetworkController.Region);
            currentTeacherAccount.dataAWS.identityId = _cognitoAWSCredentials.GetIdentityId();

            /*currentTeacherAccount.dataAWS._accessKey = _cognitoAWSCredentials.GetCredentials().AccessKey;
            currentTeacherAccount.dataAWS._secretKey = _cognitoAWSCredentials.GetCredentials().SecretKey;
            currentTeacherAccount.dataAWS._securityToken = _cognitoAWSCredentials.GetCredentials().Token;*/

            //NetworkManager.sessionAWS = authFlowResponse.SessionID;

            return true;
        }
        catch (Exception e)
        {
            Debug.Log("Login failed, exception: " + e);
            return false;
        }
    }

    /*private async Task<string> GetUserIdFromProvider(string accessToken)
    {
        // Debug.Log("Getting user's id...");
        string subId = "";

        Task<GetUserResponse> responseTask =
           HM_NetworkController.providerAWS.GetUserAsync(new GetUserRequest
           {
               AccessToken = accessToken
           });

        GetUserResponse responseObject = await responseTask;

        // set the user id
        foreach (var attribute in responseObject.UserAttributes)
        {
            if (attribute.Name == "sub")
            {
                subId = attribute.Value;
                break;
            }
        }

        return subId;
    }*/

    /*public void TestRequestGET()
    {
        HM_NetworkController.instance.GetData(currentTeacherAccount.dataAWS, HM_NetworkController.backURL + "devices", CallbackSuccess, CallbackError, null, false, true, null);
    }

    public void TestRequestPOST()
    {
        HM_NetworkController.instance.PostData(currentTeacherAccount.dataAWS, HM_NetworkController.backURL + "sync", JsonConvert.SerializeObject(currentTeacherAccount.dataAWS), CallbackSuccess, CallbackError, true);
    }

    public void CallbackSuccess(long code, string data)
    {
        Debug.Log("Request success : " + code);
        Debug.Log("Request data : " + data);
    }

    public void CallbackError(string url, long code, string error)
    {
        Debug.Log("Request error : " + url + " / " + code + " / " + error);
    }*/

    
}
