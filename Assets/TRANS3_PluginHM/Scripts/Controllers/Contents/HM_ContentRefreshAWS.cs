using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HM_ContentRefreshAWS : HM_Content
{
    public HM_NetworkController networkController;
    private Tween loadingTween = null;
    public Image loadingImage;

    public override int InitContent()
    {
        loadingImage.gameObject.SetActive(true);
        if (loadingTween == null)
        {
            loadingImage.transform.eulerAngles = Vector3.zero;
            loadingTween = loadingImage.transform.DOLocalRotate(new Vector3(0, 0, -360), 1.5f, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart).SetUpdate(true);
        }

        return 0;
    }

    public override void OnOpenedContent()
    {
        networkController.ForceTokenRefresh(OnTokenRefresh);
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        loadingTween.Kill();
        loadingTween = null;
        loadingImage.gameObject.SetActive(false);
        return 0;
    }

    public void OnTokenRefresh(bool valid)
    {
        networkController.DeleteRefreshCallback();
        this.CloseContent(HM_Scripts.contentSynchroAWS);
    }

}
