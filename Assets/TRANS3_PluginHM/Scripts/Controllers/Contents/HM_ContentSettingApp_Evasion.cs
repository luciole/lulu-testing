using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HM_ContentSettingApp_Evasion : HM_Content
{
    public static int contextID = 0;
    //HM_AppEvasionUserSettings settingsTemp = null;

    public ScrollRect scrollRectContent;

    public Image childUserIcon;
    public TextMeshProUGUI childUserName;

    public override int InitContent()
    {
        //pluginController.colorDeviceInfo.enabled = true;

        /*if (pluginController.sceneStart && HM_Scripts.optionPanel.isButtonVisible)
        {
            HM_Scripts.optionPanel.HideButton();
        }*/

        childUserIcon.sprite = HM_Scripts.pluginController.childAvatars[HM_DataController.currentChildUser.userIconID - 1];

        childUserName.text = HM_DataController.currentChildUser.name + " " + HM_DataController.currentChildUser.surname;

        scrollRectContent.verticalNormalizedPosition = 1;

        //settingsTemp = new HM_AppEvasionUserSettings(HM_DataController.currentChildUser.appEvasionUserData.settings);

        return 0;
    }

    public override void OnOpenedContent()
    {
        return;
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }
    public void OnClickButtonBack()
    {
        CloseContent();
    }

    public void OnClickButtonConfirm()
    {
        //HM_DataController.currentChildUser.appEvasionrUserData.settings.CopyContent(settingsTemp);
        //HM_DataController.model.Save();

        pluginController.onAppSettingEnd?.Invoke();

        CloseContent();
    }
}
