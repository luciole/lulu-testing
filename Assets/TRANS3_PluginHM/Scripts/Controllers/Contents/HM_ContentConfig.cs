using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HM_ContentConfig : HM_Content
{

    public override int InitContent()
    {
        

        return 0;
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        
        return 0;
    }

    public override void OnOpenedContent()
    {

    }

    public void OnSynchro()
    {
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            CloseContent(HM_Scripts.contentLoginAWS);
        }
        else
        {
            HM_Scripts.popupWifiAlert.OpenContent();
        }
    }

    public void OnAppCompagnon()
    {
        CloseContent(HM_Scripts.contentSynchroBluetooth);
    }

    public void OnGenerique()
    {
        CloseContent(HM_Scripts.contentGroupsChoice);
    }
}
