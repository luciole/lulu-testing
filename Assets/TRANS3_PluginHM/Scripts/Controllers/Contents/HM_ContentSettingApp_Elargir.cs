using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HM_ContentSettingApp_Elargir : HM_Content
{
    public static int contextID = 0;

    HM_AppElargirUserSettings settingsTemp = null;

    public ScrollRect scrollRectContent;

    public Image childUserIcon;
    public TextMeshProUGUI childUserName;

    public TMP_Dropdown parcours_input;
    public Slider soundReturn_input;
    public Slider readingSpeed_input;
    public Slider readingSpeedObjective_input;
    public TMP_Dropdown nextText_input;
    public TMP_Dropdown difficultyMode_input;
    public Toggle autoDifficulty_input;
    public Toggle highlightedDifficultWords_input;
    public Toggle highlightedLinkWords_input;
    public Toggle highlightedBreathTime_input;
    public TMP_Dropdown breathMinLevel_input;
    public TMP_Dropdown freeReadingTime_input;
    public TMP_Dropdown discoveryTime_input;
    public TMP_Dropdown evaluationTime_input;

    public GameObject nextTextGO;

    private List<string> texts = new List<string>();

    public override int InitContent()
    {
        //pluginController.colorDeviceInfo.enabled = true;

        /*if (pluginController.sceneStart && HM_Scripts.optionPanel.isButtonVisible)
        {
            HM_Scripts.optionPanel.HideButton();
        }*/

        /*if (HM_DataController.currentChildUser == null)
       {
           HM_DataController.currentChildUser = HM_DataController.model.teacherAccounts[0].childUsers[0];
       }*/

        childUserIcon.sprite = HM_Scripts.pluginController.childAvatars[HM_DataController.currentChildUser.userIconID - 1];

        childUserName.text = HM_DataController.currentChildUser.name + " " + HM_DataController.currentChildUser.surname;

        scrollRectContent.verticalNormalizedPosition = 1;

        settingsTemp = new HM_AppElargirUserSettings(HM_DataController.currentChildUser.appElargirUserData.settings);

        highlightedDifficultWords_input.isOn = settingsTemp.highlightDifficultWordsAvailabilityInDiscoveryMode;
        if (highlightedDifficultWords_input.isOn)
            highlightedDifficultWords_input.targetGraphic.GetComponent<Image>().sprite = highlightedDifficultWords_input.graphic.GetComponent<Image>().sprite;
        else
            highlightedDifficultWords_input.targetGraphic.GetComponent<Image>().sprite = highlightedDifficultWords_input.GetComponent<Image>().sprite;

        highlightedLinkWords_input.isOn = settingsTemp.wordLinksAvailabilityInDiscoveryMode;
        if (highlightedLinkWords_input.isOn)
            highlightedLinkWords_input.targetGraphic.GetComponent<Image>().sprite = highlightedLinkWords_input.graphic.GetComponent<Image>().sprite;
        else
            highlightedLinkWords_input.targetGraphic.GetComponent<Image>().sprite = highlightedLinkWords_input.GetComponent<Image>().sprite;

        highlightedBreathTime_input.isOn = settingsTemp.highlightBreathAvailabilityInDiscoveryMode;
        if (highlightedBreathTime_input.isOn)
            highlightedBreathTime_input.targetGraphic.GetComponent<Image>().sprite = highlightedBreathTime_input.graphic.GetComponent<Image>().sprite;
        else
            highlightedBreathTime_input.targetGraphic.GetComponent<Image>().sprite = highlightedBreathTime_input.GetComponent<Image>().sprite;

        breathMinLevel_input.value = settingsTemp.karaokeHighlightBreathMinLevel - 1;

        autoDifficulty_input.isOn = settingsTemp.autoDifficultyChange;
        if (autoDifficulty_input.isOn)
            autoDifficulty_input.targetGraphic.GetComponent<Image>().sprite = autoDifficulty_input.graphic.GetComponent<Image>().sprite;
        else
            autoDifficulty_input.targetGraphic.GetComponent<Image>().sprite = autoDifficulty_input.GetComponent<Image>().sprite;

        switch (settingsTemp.discoveryTime)
        {
            case 120:
                discoveryTime_input.value = 0;
                break;
            case 180:
                discoveryTime_input.value = 1;
                break;
            case 240:
                discoveryTime_input.value = 2;
                break;
            case 300:
                discoveryTime_input.value = 3;
                break;
            case 360:
                discoveryTime_input.value = 4;
                break;
            case 420:
                discoveryTime_input.value = 5;
                break;
            case 480:
                discoveryTime_input.value = 6;
                break;
            default:
                discoveryTime_input.value = 0;
                break;

        }

        switch (settingsTemp.evaluationTime)
        {
            case 300:
                evaluationTime_input.value = 0;
                break;
            case 360:
                evaluationTime_input.value = 1;
                break;
            case 420:
                evaluationTime_input.value = 2;
                break;
            case 480:
                evaluationTime_input.value = 3;
                break;
            case 540:
                evaluationTime_input.value = 4;
                break;
            case 600:
                evaluationTime_input.value = 5;
                break;
            case 660:
                evaluationTime_input.value = 6;
                break;
            case 720:
                evaluationTime_input.value = 7;
                break;
            case 780:
                evaluationTime_input.value = 8;
                break;
            case 840:
                evaluationTime_input.value = 9;
                break;
            case 900:
                evaluationTime_input.value = 10;
                break;
            default:
                evaluationTime_input.value = 0;
                break;

        }


        switch (settingsTemp.freeReadingTime)
        {
            case 60:
                freeReadingTime_input.value = 0;
                break;
            case 120:
                freeReadingTime_input.value = 1;
                break;
            case 180:
                freeReadingTime_input.value = 2;
                break;
            default:
                freeReadingTime_input.value = 1;
                break;
        }

        if (texts.Count == 0)
        {
            foreach (TMP_Dropdown.OptionData od in nextText_input.options)
            {
                texts.Add(od.text);
            }
        }

        if (contextID == 0)
        {
            parcours_input.options.Clear();
            parcours_input.options.Add(new TMPro.TMP_Dropdown.OptionData("CE1"));
            parcours_input.value = 0;

            nextTextGO.SetActive(true);

            //gestion nextText

            //v�rification texte en cours
            int idText = 1;
            if (HM_DataController.currentChildUser.appElargirUserData.progression.texts.Count > 0)
            {
                idText = HM_DataController.currentChildUser.appElargirUserData.progression.texts[HM_DataController.currentChildUser.appElargirUserData.progression.texts.Count - 1].idText;
                idText += 2;//offset gestion 
            }

            nextText_input.options.Clear();
            nextText_input.options.Add(new TMPro.TMP_Dropdown.OptionData(texts[0]));
            for (int i = 1; i < texts.Count; i++)
            {
                if (idText <= i)
                {
                    nextText_input.options.Add(new TMPro.TMP_Dropdown.OptionData(texts[i]));
                }
            }
        }
        else
        {
            parcours_input.options.Clear();
            parcours_input.options.Add(new TMPro.TMP_Dropdown.OptionData("CE1"));
            parcours_input.options.Add(new TMPro.TMP_Dropdown.OptionData("Custom #1"));
            parcours_input.options.Add(new TMPro.TMP_Dropdown.OptionData("Custom #2"));
            parcours_input.options.Add(new TMPro.TMP_Dropdown.OptionData("Custom #3"));

            if (settingsTemp.parcours.Equals("CE1"))
            {
                parcours_input.value = 0;
            }
            else if (settingsTemp.parcours.Equals("Custom #1"))
            {
                parcours_input.value = 1;
            }
            else if (settingsTemp.parcours.Equals("Custom #2"))
            {
                parcours_input.value = 2;
            }
            else if (settingsTemp.parcours.Equals("Custom #3"))
            {
                parcours_input.value = 3;
            }

            nextTextGO.SetActive(false);
        }


        readingSpeed_input.value = settingsTemp.readingSpeed;

        readingSpeedObjective_input.value = settingsTemp.readingSpeedObjective;

        difficultyMode_input.value = settingsTemp.difficultyMode;

        nextText_input.value = 0;//par d�faut on remet toujours � z�ro
        //nextSession_input.value = 0;//par d�faut on remet toujours � z�ro

        soundReturn_input.value = settingsTemp.soundReturn;
        return 0;
    }

    public override void OnOpenedContent()
    {
        return;
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }

    public void OnClickButtonBack()
    {
        pluginController.onAppSettingCancel?.Invoke();

        CloseContent();
    }

    public void OnClickButtonConfirm()
    {
        settingsTemp.revisionDate = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssK");

        HM_DataController.currentChildUser.appElargirUserData.settings.CopyContent(settingsTemp);
        HM_DataController.model.Save();

        pluginController.onAppSettingEnd?.Invoke();

        CloseContent();
    }

    public void OnParcoursChange()
    {
        switch (parcours_input.value)
        {
            case 0:
                settingsTemp.parcours = "CE1";
                break;
            case 1:
                settingsTemp.parcours = "Custom #1";
                break;
            case 2:
                settingsTemp.parcours = "Custom #2";
                break;
            case 3:
                settingsTemp.parcours = "Custom #3";
                break;
        }
    }

    public void OnSoundReturnChange()
    {
        settingsTemp.soundReturn = (int)soundReturn_input.value;
    }

    public void OnReadingSpeedChange()
    {
        settingsTemp.readingSpeed = (int)readingSpeed_input.value;
    }

    public void OnReadingSpeedObjectiveChange()
    {
        settingsTemp.readingSpeedObjective = (int)readingSpeedObjective_input.value;
    }

    public void OnNextTextChange()
    {
        settingsTemp.nextText = nextText_input.value - 2 + (texts.Count - (nextText_input.options.Count));
    }

    public void OnDifficultyModeChange()
    {
        settingsTemp.difficultyMode = difficultyMode_input.value;
    }

    public void OnAutoDifficultyChange()
    {
        settingsTemp.autoDifficultyChange = autoDifficulty_input.isOn;

        if (autoDifficulty_input.isOn)
            autoDifficulty_input.targetGraphic.GetComponent<Image>().sprite = autoDifficulty_input.graphic.GetComponent<Image>().sprite;
        else
            autoDifficulty_input.targetGraphic.GetComponent<Image>().sprite = autoDifficulty_input.GetComponent<Image>().sprite;
    }

    public void OnHighlightedDifficultWordsChange()
    {
        settingsTemp.highlightDifficultWordsAvailabilityInDiscoveryMode = highlightedDifficultWords_input.isOn;

        if (highlightedDifficultWords_input.isOn)
            highlightedDifficultWords_input.targetGraphic.GetComponent<Image>().sprite = highlightedDifficultWords_input.graphic.GetComponent<Image>().sprite;
        else
            highlightedDifficultWords_input.targetGraphic.GetComponent<Image>().sprite = highlightedDifficultWords_input.GetComponent<Image>().sprite;
    }

    public void OnHighlightedLinkWordsChange()
    {
        settingsTemp.wordLinksAvailabilityInDiscoveryMode = highlightedLinkWords_input.isOn;

        if (highlightedLinkWords_input.isOn)
            highlightedLinkWords_input.targetGraphic.GetComponent<Image>().sprite = highlightedLinkWords_input.graphic.GetComponent<Image>().sprite;
        else
            highlightedLinkWords_input.targetGraphic.GetComponent<Image>().sprite = highlightedLinkWords_input.GetComponent<Image>().sprite;
    }

    public void OnHighlightedBreathTimeChange()
    {
        settingsTemp.highlightBreathAvailabilityInDiscoveryMode = highlightedBreathTime_input.isOn;

        if (highlightedBreathTime_input.isOn)
            highlightedBreathTime_input.targetGraphic.GetComponent<Image>().sprite = highlightedBreathTime_input.graphic.GetComponent<Image>().sprite;
        else
            highlightedBreathTime_input.targetGraphic.GetComponent<Image>().sprite = highlightedBreathTime_input.GetComponent<Image>().sprite;
    }

    public void OnBreathMinLevelChange()
    {
        settingsTemp.karaokeHighlightBreathMinLevel = breathMinLevel_input.value + 1;
    }

    public void OnFreeReadingTimeChange()
    {
        switch (freeReadingTime_input.value)
        {
            case 0:
                settingsTemp.freeReadingTime = 60;
                break;
            case 1:
                settingsTemp.freeReadingTime = 120;
                break;
            case 2:
                settingsTemp.freeReadingTime = 180;
                break;
        }
    }

    public void OnDiscoveryTimeChange()
    {
        switch(discoveryTime_input.value)
        {
            case 0:
                settingsTemp.discoveryTime = 120;
                break;
            case 1:
                settingsTemp.discoveryTime = 180;
                break;
            case 2:
                settingsTemp.discoveryTime = 240;
                break;
            case 3:
                settingsTemp.discoveryTime = 300;
                break;
            case 4:
                settingsTemp.discoveryTime = 360;
                break;
            case 5:
                settingsTemp.discoveryTime = 420;
                break;
            case 6:
                settingsTemp.discoveryTime = 480;
                break;
        }
    }

    public void OnEvaluationTimeChange()
    {
        switch (evaluationTime_input.value)
        {
            case 0:
                settingsTemp.evaluationTime = 300;
                break;
            case 1:
                settingsTemp.evaluationTime = 360;
                break;
            case 2:
                settingsTemp.evaluationTime = 420;
                break;
            case 3:
                settingsTemp.evaluationTime = 480;
                break;
            case 4:
                settingsTemp.evaluationTime = 540;
                break;
            case 5:
                settingsTemp.evaluationTime = 600;
                break;
            case 6:
                settingsTemp.evaluationTime = 660;
                break;
            case 7:
                settingsTemp.evaluationTime = 720;
                break;
            case 8:
                settingsTemp.evaluationTime = 780;
                break;
            case 9:
                settingsTemp.evaluationTime = 840;
                break;
            case 10:
                settingsTemp.evaluationTime = 900;
                break;
        }
    }

}
