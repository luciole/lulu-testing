using DG.Tweening;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public struct HM_SyncroAccountInfo
{
    public string teacherCode;
    public bool synchronized;
    public long errorCode;
    public string errorMessage;
}

public class HM_ContentSynchroAWS : HM_Content
{
    private Tween loadingTween = null;
    public Image loadingImage;

    public TextMeshProUGUI successText;
    public TextMeshProUGUI errorText;

    public Button buttonReload;
    public Button buttonOK;

    public GameObject imageOK;
    public GameObject imageERROR;

    private int nbrSynchro = 0;
    private int nbrSynchroMax = 0;
    private bool hasError = false;
    private long errorMaxValue = 0;
    private long errorCode = 0;
    private List<HM_SyncroAccountInfo> accountsInfo = new List<HM_SyncroAccountInfo>();
    private List<string> allAccounts = new List<string>();
    private List<string> successSynchroAccounts = new List<string>();
    private List<string> errorSynchroAccounts = new List<string>();

    private List<string> deactivateToken = new List<string>();

    [HideInInspector]
    public static float timeCheckUpload = -1; 

    [HideInInspector]
    public int navID = 0;

    private bool changeScreen = false;

    public override int InitContent()
    {
        imageOK.SetActive(false);
        imageERROR.SetActive(false);
        buttonReload.interactable = false;
        buttonOK.interactable = false;
        loadingImage.gameObject.SetActive(true);
        successText.gameObject.SetActive(false);
        errorText.gameObject.SetActive(false);
        nbrSynchro = 0;
        nbrSynchroMax = 0;
        hasError = false;
        errorMaxValue = 0;
        errorCode = 0;

        if (loadingTween == null)
        {
            loadingImage.transform.eulerAngles = Vector3.zero;
            loadingTween = loadingImage.transform.DOLocalRotate(new Vector3(0, 0, -360), 1.5f, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart).SetUpdate(true);
        }

        if (HM_PluginController.navContext == HM_PluginController.NAV_CONTEXT.NONE)
        {
            buttonReload.gameObject.SetActive(false);
            buttonOK.gameObject.SetActive(false);
        }
        else
        {
            buttonReload.gameObject.SetActive(true);
            buttonOK.gameObject.SetActive(true);
        }

        return 0;
    }

    public void LaunchNewSynchro(bool init = false)
    {
        switch (HM_Scripts.pluginController.appID)
        {
            case APP_ID.ELARGIR:
                //tente d'envoyer les donn�es Audio Elargir en attente (si il y en a)
                HM_NetworkController.instance.SendAudioData();
                break;
        }

        deactivateToken = new List<string>();
        accountsInfo = new List<HM_SyncroAccountInfo>();

        allAccounts = new List<string>();
        successSynchroAccounts = new List<string>();
        errorSynchroAccounts = new List<string>();

        if (HM_PluginController.navContext == HM_PluginController.NAV_CONTEXT.NONE)
        {
            buttonReload.gameObject.SetActive(false);
            buttonOK.gameObject.SetActive(false);
        }
        else
        {
            buttonReload.gameObject.SetActive(true);
            buttonOK.gameObject.SetActive(true);
        }

        if (loadingTween == null)
        {
            loadingImage.transform.eulerAngles = Vector3.zero;
            loadingTween = loadingImage.transform.DOLocalRotate(new Vector3(0, 0, -360), 1.5f, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart).SetUpdate(true);
        }

        imageOK.SetActive(false);
        imageERROR.SetActive(false);
        buttonReload.interactable = false;
        buttonOK.interactable = false;
        loadingImage.gameObject.SetActive(true);
        successText.gameObject.SetActive(false);
        errorText.gameObject.SetActive(false);
        nbrSynchro = 0;
        nbrSynchroMax = 0;
        hasError = false;

        foreach (HM_TeacherAccount t in HM_DataController.model.teacherAccounts)
        {
            allAccounts.Add(t.teacherCode);

            if (t.dataAWS._idToken != null && t.dataAWS._idToken.Length > 0)
            {
                nbrSynchro++;
            }
            else
            {
                HM_SyncroAccountInfo account = new HM_SyncroAccountInfo();
                account.teacherCode = t.teacherCode;
                account.synchronized = false;
                account.errorCode = 0;
                account.errorMessage = "Mot de passe non renseign�";
                accountsInfo.Add(account);
            }
        }
        nbrSynchroMax = nbrSynchro;
        errorMaxValue = 0;

        changeScreen = false;
        if (nbrSynchro == 0)
        {
            if(init)
            {
                changeScreen = true;
            }
            else
            {
                this.CloseContent(HM_Scripts.contentProfil);
            }
        }
        else
        {
            foreach (HM_TeacherAccount t in HM_DataController.model.teacherAccounts)
            {
                if (t.dataAWS._idToken != null && t.dataAWS._idToken.Length > 0)
                {
                    OnRequestSynchro(t);
                }
            }
        }

    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }

    public override void OnOpenedContent()
    {
        LaunchNewSynchro(true);

        if (changeScreen)
        {
            changeScreen = false;
            this.CloseContent(HM_Scripts.contentProfil);
        }
        return;
    }

    public void OnRequestSynchro(HM_TeacherAccount teacherAccount)
    {
        HM_DataSynchro newDataSynchro = new HM_DataSynchro();

        int synchroResult = newDataSynchro.LoadFromAppData(teacherAccount.teacherCode);

        HM_Log.ReplaceTextFile("HM_SyncUp.txt", JsonConvert.SerializeObject(newDataSynchro), true);
        //Debug.Log(JsonConvert.SerializeObject(newDataSynchro));

        HM_NetworkController.instance.PostData(teacherAccount.dataAWS, HM_NetworkController.backURL + "sync", JsonConvert.SerializeObject(newDataSynchro), teacherAccount.teacherCode, CallbackSynchroSuccess, CallbackError, true);
    }

    public void CallbackSynchroSuccess(long code, string data, string info)
    {
        int result = HM_NetworkController.instance.OnDataSynchro(data);

        if (result == 0)
        {
            successSynchroAccounts.Add(info);

            nbrSynchro--;

            HM_SyncroAccountInfo account = new HM_SyncroAccountInfo();
            account.teacherCode = info;
            account.synchronized = true;
            account.errorCode = -1;
            account.errorMessage = "";
            accountsInfo.Add(account);

            HM_Scripts.optionPanel.UpdateData();

        }
        else
        {
            errorSynchroAccounts.Add(info);

            hasError = true;
            if (result > errorMaxValue)
            {
                errorMaxValue = result;
            }

            nbrSynchro--;

            if (result == 1)
            {
                HM_SyncroAccountInfo account = new HM_SyncroAccountInfo();
                account.teacherCode = info;
                account.synchronized = false;
                account.errorCode = 1;
                account.errorMessage = "Chargement donn�es";
                accountsInfo.Add(account);
            }
            else if (result == 2)
            {
                HM_SyncroAccountInfo account = new HM_SyncroAccountInfo();
                account.teacherCode = info;
                account.synchronized = false;
                account.errorCode = 2;
                account.errorMessage = "Conflit lot identifiants";
                accountsInfo.Add(account);

                JObject o = JObject.Parse(data);

                string teacherCode = o["teacherCode"].Value<string>();

                deactivateToken.Add(teacherCode);
            }
        }

        //fin des synchronisations
        if (nbrSynchro == 0)
        {
            pluginController.onSynchroEnd?.Invoke(allAccounts, successSynchroAccounts, errorSynchroAccounts);

            if (hasError)
            {
                if (errorMaxValue == 2)
                {
                    StartCoroutine(CheckTimeUploadTrace(false, "Un conflit a �t� d�t�ct� entre deux comptes utilisant le m�me lot d'identifiants.\n Code erreur : 2"));
                    //OnErrorMessage("Un conflit a �t� d�t�ct� entre deux comptes utilisant le m�me lot d'identifiants.\n Code erreur : 2");
                }
                else if (errorMaxValue == 422)
                {
                    StartCoroutine(CheckTimeUploadTrace(false, "L'application n�cessite d'�tre mis � jour pour se synchroniser.\n Code erreur : " + errorMaxValue));
                    //OnErrorMessage("L'application n�cessite d'�tre mis � jour pour se synchroniser.\n Code erreur : " + errorMaxValue);
                }
                else
                {
                    StartCoroutine(CheckTimeUploadTrace(false, "Un ou plusieurs comptes n'ont pas pu se synchroniser.\n Code erreur : " + errorMaxValue));
                    //OnErrorMessage("Un ou plusieurs comptes n'ont pas pu se synchroniser.\n Code erreur : "+ errorMaxValue);
                }
            }
            else
            {
                StartCoroutine(CheckTimeUploadTrace(true,""));
                //OnSuccessMessage();
            }
        }
    }

    public void CallbackError(string url, long code, string error, string info)
    {
        HM_Log.AppendTextToFile("HM_Log.txt", url+ " / "+ code+" / "+ error);
        hasError = true;
        errorCode = code;

        nbrSynchro--;

        HM_SyncroAccountInfo account = new HM_SyncroAccountInfo();
        account.teacherCode = info;
        account.synchronized = false;
        account.errorCode = code;
        account.errorMessage = "";
        accountsInfo.Add(account);

        deactivateToken.Add(account.teacherCode);

        if (code > errorMaxValue)
        {
            errorMaxValue = code;
        }

        if (nbrSynchro == 0)
        {
            if (errorMaxValue == 2)
            {
                StartCoroutine(CheckTimeUploadTrace(false, "Un conflit a �t� d�t�ct� entre deux comptes utilisant le m�me lot d'identifiants.\n Code erreur : 2"));
                //OnErrorMessage("Un conflit a �t� d�t�ct� entre deux comptes utilisant le m�me lot d'identifiants.\n Code erreur : 2");
            }
            else if (errorMaxValue == 422)
            {
                StartCoroutine(CheckTimeUploadTrace(false, "L'application n�cessite d'�tre mis � jour pour se synchroniser.\n Code erreur : " + errorMaxValue));
                //OnErrorMessage("L'application n�cessite d'�tre mis � jour pour se synchroniser.\n Code erreur : " + errorMaxValue);
            }
            else
            {
                StartCoroutine(CheckTimeUploadTrace(false, "Un ou plusieurs comptes n'ont pas pu se synchroniser.\n Code erreur : " + errorMaxValue));
                //OnErrorMessage("Un ou plusieurs comptes n'ont pas pu se synchroniser.\n Code erreur : "+ errorMaxValue);
            }
        }
    }

    public void OnSuccessMessage()
    {
        loadingImage.gameObject.SetActive(false);
        successText.gameObject.SetActive(true);
        loadingTween.Kill();
        loadingTween = null;

        imageOK.SetActive(true);

        buttonOK.interactable = true;
        buttonReload.interactable = true;

        if (HM_PluginController.navContext == HM_PluginController.NAV_CONTEXT.NONE)
        {
            DOVirtual.DelayedCall(4.0f, () =>
            {
                this.CloseContent(HM_Scripts.contentProfil);
            });
        }
    }

    public void OnErrorMessage(string msg)
    {
        loadingImage.gameObject.SetActive(false);
        errorText.text = msg;
        errorText.gameObject.SetActive(true);
        loadingTween.Kill(true);
        loadingTween = null;

        imageERROR.SetActive(true);

        buttonReload.gameObject.SetActive(true);
        buttonOK.gameObject.SetActive(true);

        buttonOK.interactable = true;
        buttonReload.interactable = true;

        /*DOVirtual.DelayedCall(4.0f, () =>
        {
            if (HM_DataController.currentTeacherAccount != null)
            {
                this.CloseContent(HM_Scripts.contentTeacherAccount);
            }
            else
            {
                this.CloseContent(HM_Scripts.contentProfil);
            }
        });*/
    }

    public void OnGoNext()
    {
        foreach (string s in deactivateToken)
        {
            foreach (HM_TeacherAccount t in HM_DataController.model.teacherAccounts)
            {
                if (t.teacherCode.ToUpper().Equals(s.ToUpper()))
                {
                    if (t.dataAWS._idToken != null && t.dataAWS._idToken.Length > 0)
                    {
                        t.dataAWS._idToken = null;
                        t.dataAWS._accessToken = null;
                        t.dataAWS._refreshToken = null;
                        t.dataAWS.identityId = null;
                        HM_DataController.model.Save();
                        break;
                    }
                }
            }
        }

        if (HM_PluginController.navContext == HM_PluginController.NAV_CONTEXT.LOT)
        {
            this.CloseContent(HM_Scripts.contentGroupsChoice);
        }
        else if (HM_PluginController.navContext == HM_PluginController.NAV_CONTEXT.LETTRE)
        {
            this.CloseContent(HM_Scripts.contentDeviceColor);
        }
        else if(HM_PluginController.navContext == HM_PluginController.NAV_CONTEXT.NEW_TEACHER)
        {
            this.CloseContent(HM_Scripts.contentProfil);
        }
        else if (HM_PluginController.navContext == HM_PluginController.NAV_CONTEXT.SYNCHRO)
        {
            this.CloseContent(HM_Scripts.contentProfil);
        }
        else
        {
            this.CloseContent(HM_Scripts.contentProfil);
        }
    }

    public IEnumerator CheckTimeUploadTrace(bool success, string errorMsg)
    {
        timeCheckUpload = Time.realtimeSinceStartup;

        while (Time.realtimeSinceStartup - timeCheckUpload < 1.0f)
        {
            yield return new WaitForSeconds(0.1f);
        }

        timeCheckUpload = -1;

        if (success)
        {
            UnityMainThreadDispatcher.Instance().Enqueue(OnSuccessMessage);
        }
        else
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => OnErrorMessage(errorMsg));
        }

        yield return null;
    }
}
