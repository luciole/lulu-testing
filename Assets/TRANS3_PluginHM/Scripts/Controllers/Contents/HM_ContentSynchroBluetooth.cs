using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HM_ContentSynchroBluetooth : HM_Content
{
    public override int InitContent()
    {
        HM_DataController.model.teacherAccounts.Add(HM_DataController.newTeacherAccountTemp);

        HM_DataController.model.Save();

        HM_Scripts.optionPanel.UpdateData();

#if UNITY_ANDROID
        //on prepare la connexion Bluetooth que pour la sc�ne de d�marrage
        if (HM_DataController.currentChildUser == null && pluginController.HasAndroidBluetoothPermissions())
        {
            if (!HM_NetworkController.instance.blt_initialized)
            {
                HM_NetworkController.instance.BLT_Initialize();
            }
            else
            {
                HM_Scripts.contentProfil.bluetoothIconOn.SetActive(true);
            }
        }
#endif
        return 0;
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }

    public override void OnOpenedContent()
    {
    }

    public void OnSkipContent()
    {
        CloseContent(HM_Scripts.contentProfil);
    }
}
