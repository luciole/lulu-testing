using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class HM_ContentBonjour : HM_Content
{
    public TextMeshProUGUI childName;

    public override int InitContent()
    {
        PlayerPrefs.Save();

        if (pluginController.sceneStart && HM_Scripts.optionPanel.isButtonVisible)
        {
            HM_Scripts.optionPanel.HideButton();
        }

        if (HM_DataController.currentChildUser != null)
        {
            HM_AppLucioleUserResult.taskId_session = System.Guid.NewGuid().ToString();
            HM_AppElargirUserResult.taskId_session = System.Guid.NewGuid().ToString();
            HM_AppEvasionUserResult.taskId_session = System.Guid.NewGuid().ToString();
            childName.text = HM_DataController.currentChildUser.name + " " + HM_DataController.currentChildUser.surname;
        }
        else
        {
            childName.text = "";
        }

        //pluginController.colorDeviceInfo.enabled = true;

        return 0;
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        if (pluginController.sceneNameAfterLogin.Length == 0)
        {
            Debug.LogError("Pas de sc�ne renseign�e pour suite apr�s Login");
        }
        else
        {
            SceneManager.LoadScene(pluginController.sceneNameAfterLogin);
        }
        return 0;
    }

    public override void OnOpenedContent()
    {
        DOVirtual.DelayedCall(2.0f, () =>
        {
            CloseContent();
        });
    }
}
