using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HM_ContentSettingApp_Luciole : HM_Content
{
    public static int contextID = 0;

    HM_AppLucioleUserSettings settingsTemp = null;

    public ScrollRect scrollRectContent;

    public Image childUserIcon;
    public TextMeshProUGUI childUserName;

    public TextMeshProUGUI levelInfo;

    public Toggle skipLevel_input;

    public Toggle qrCode_input;

    public TMP_Dropdown sessionTime_input;

    public TMP_Dropdown skipToMission_input;

    public override int InitContent()
    {
        //pluginController.colorDeviceInfo.enabled = true;

        /*if (pluginController.sceneStart && HM_Scripts.optionPanel.isButtonVisible)
        {
            HM_Scripts.optionPanel.HideButton();
        }*/

        if (HM_DataController.currentChildUser == null)
        {
            HM_DataController.currentChildUser = HM_DataController.model.teacherAccounts[0].childUsers[0];
        }

        childUserIcon.sprite = HM_Scripts.pluginController.childAvatars[HM_DataController.currentChildUser.userIconID - 1];

        childUserName.text = HM_DataController.currentChildUser.name + " " + HM_DataController.currentChildUser.surname;

        scrollRectContent.verticalNormalizedPosition = 1;

        HM_DataController.currentChildUser.appLucioleUserData.settings.skipLevel = false;//par d�faut toujours
        HM_DataController.currentChildUser.appLucioleUserData.settings.skipToMission = -1;//par d�faut toujours

        settingsTemp = new HM_AppLucioleUserSettings(HM_DataController.currentChildUser.appLucioleUserData.settings);

        levelInfo.text = settingsTemp.currentLevelInformation;

        sessionTime_input.value = settingsTemp.sessionTime;

        skipToMission_input.value = 0;

        skipLevel_input.isOn = settingsTemp.skipLevel;
        if (skipLevel_input.isOn)
            skipLevel_input.targetGraphic.GetComponent<Image>().sprite = skipLevel_input.graphic.GetComponent<Image>().sprite;
        else
            skipLevel_input.targetGraphic.GetComponent<Image>().sprite = skipLevel_input.GetComponent<Image>().sprite;

        qrCode_input.isOn = settingsTemp.QRCodeEnabled;
        if (qrCode_input.isOn)
            qrCode_input.targetGraphic.GetComponent<Image>().sprite = qrCode_input.graphic.GetComponent<Image>().sprite;
        else
            qrCode_input.targetGraphic.GetComponent<Image>().sprite = qrCode_input.GetComponent<Image>().sprite;

        return 0;
    }

    public override void OnOpenedContent()
    {
        return;
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }
    public void OnClickButtonBack()
    {
        pluginController.onAppSettingCancel?.Invoke();

        CloseContent();
    }

    public void OnClickButtonConfirm()
    {
        settingsTemp.revisionDate = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssK");

        HM_DataController.currentChildUser.appLucioleUserData.settings.CopyContent(settingsTemp);
        HM_DataController.model.Save();

        pluginController.onAppSettingEnd?.Invoke();

        CloseContent();
    }


    public void OnSkipLevelChange()
    {
        settingsTemp.skipLevel = skipLevel_input.isOn;

        if (skipLevel_input.isOn)
            skipLevel_input.targetGraphic.GetComponent<Image>().sprite = skipLevel_input.graphic.GetComponent<Image>().sprite;
        else
            skipLevel_input.targetGraphic.GetComponent<Image>().sprite = skipLevel_input.GetComponent<Image>().sprite;
    }


    public void OnQRCodeChange()
    {
        settingsTemp.QRCodeEnabled = qrCode_input.isOn;

        if (qrCode_input.isOn)
            qrCode_input.targetGraphic.GetComponent<Image>().sprite = qrCode_input.graphic.GetComponent<Image>().sprite;
        else
            qrCode_input.targetGraphic.GetComponent<Image>().sprite = qrCode_input.GetComponent<Image>().sprite;
    }

    public void OnSessionTimeChange()
    {
        settingsTemp.sessionTime = sessionTime_input.value;
    }

    public void OnSkipMissionChange()
    {
        settingsTemp.skipToMission = skipToMission_input.value - 1;
    }


}
