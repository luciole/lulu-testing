﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HM_ContentDeviceColor : HM_Content
{
    public Button colorPreviousPage_btn;
    public Button colorNextPage_btn;

    public TextMeshProUGUI titleText;
    public TextMeshProUGUI subtitleText;
    public TextMeshProUGUI nbrComptesText;

    public RectTransform colorsPages_rtf;
    public List<Button> colorIcons_btns;

    public Button confirm_btn;

    private int currentPageColors = 0;
    private int nombrePages = 1;
    private int itemsPerPage = 32;
    private int currentColorID = -1;

    [HideInInspector]
    public int navID = 0;

    public override int InitContent()
    {

        /*if (pluginController.sceneStart && HM_Scripts.optionPanel.isButtonVisible)
        {
            HM_Scripts.optionPanel.HideButton();
        }*/

        //remise à zéro de tous les paramètres du contenu

        if (HM_DataController.currentTeacherAccount == null)
        {
            titleText.text = "Lettre étiquette";
            if (HM_DataController.model.teacherAccounts.Count <= 0)
            {
                HM_DataController.model.currentColorID = -1;
                HM_DataController.model.Save();
                //subtitleText.text = "Choisir une lettre non encore utilisée sur d'autres périphériques\n(coller une étiquette en évidence avec cette lettre pour guider les élèves)";
                //nbrComptesText.text = "";
            }
            else if (HM_DataController.model.teacherAccounts.Count == 1)
            {
                //subtitleText.text = "Choisir une lettre non encore utilisée sur d'autres périphériques\n(coller une étiquette en évidence avec cette lettre pour guider les élèves)";
                //nbrComptesText.text = "( Périphérique utilisé par 1 autre enseignant ) ";
            }
            else
            {
                //subtitleText.text = "Choisissez la lettre à associer à ce périphérique\n(attention cela affectera aussi les autres comptes professeurs utilisant l'application)";
                //nbrComptesText.text = "( Périphérique utilisé par "+(HM_DataController.model.teacherAccounts.Count) +" autres enseignants ) ";
            }
        }
        else
        {
            titleText.text = "Lettre étiquette";
            if (HM_DataController.model.teacherAccounts.Count <= 1)
            {
                //subtitleText.text = "Choisissez la lettre à associer à ce périphérique";
                //nbrComptesText.text = "";
            }
            else if (HM_DataController.model.teacherAccounts.Count == 2)
            {
                //subtitleText.text = "Choisissez la lettre à associer à ce périphérique\n(attention cela affectera aussi les autres comptes professeurs utilisant l'application)";
                //nbrComptesText.text = "( Périphérique utilisé par 1 autre enseignant ) ";
            }
            else
            {
                //subtitleText.text = "Choisissez la lettre à associer à ce périphérique\n(attention cela affectera aussi les autres comptes professeurs utilisant l'application)";
                //nbrComptesText.text = "( Périphérique utilisé par " + (HM_DataController.model.teacherAccounts.Count-1) + " autres enseignants ) ";
            }
        }

        for (int i = 0; i < nombrePages * itemsPerPage; i++)
        {
            colorIcons_btns[i].GetComponent<Image>().color = new Color32(184, 229, 255, 0);
        }

        currentPageColors = 0;
        for (int i = currentPageColors * itemsPerPage; i < currentPageColors * itemsPerPage + itemsPerPage; i++)
        {
            colorIcons_btns[i].interactable = true;
        }

        colorsPages_rtf.anchoredPosition = Vector2.zero;
        colorPreviousPage_btn.interactable = false;
        colorNextPage_btn.interactable = true;

        if(HM_DataController.model.currentColorID == -1)
        {
            currentColorID = -1;
            confirm_btn.interactable = false;
        }
        else
        {
            currentColorID = HM_DataController.model.currentColorID;
            colorIcons_btns[currentColorID - 1].GetComponent<Image>().color = new Color32(184, 229, 255, 255);
            confirm_btn.interactable = true;
        }

        //pluginController.colorDeviceInfo.enabled = false;

        return 0;
    }

    public override void OnOpenedContent()
    {
        return;
    }

    //navigation d'une page à une autre
    public void OnClickNavPage(int value)
    {
        //on invalide le choix profil fait si il existe
        if (currentColorID != -1)
        {
            colorIcons_btns[currentColorID - 1].GetComponent<Image>().color = new Color32(184, 229, 255, 0);
        }
        currentColorID = -1;
        confirm_btn.interactable = false;

        //désactivation interaction page courante
        for (int i = currentPageColors * itemsPerPage; i < currentPageColors * itemsPerPage + itemsPerPage; i++)
        {
            colorIcons_btns[i].interactable = false;
        }

        if (value == 1)//next page
        {
            currentPageColors++;

            if (currentPageColors == nombrePages-1)
                colorNextPage_btn.interactable = false;
            else
                colorNextPage_btn.interactable = true;

            colorPreviousPage_btn.interactable = true;
        }
        else//previous page
        {
            currentPageColors--;

            if (currentPageColors == 0)
                colorPreviousPage_btn.interactable = false;
            else
                colorPreviousPage_btn.interactable = true;

            colorNextPage_btn.interactable = true;
        }

        //animation de transition
        colorsPages_rtf.DOAnchorPosX(-currentPageColors * 544, 0.5f).SetEase(Ease.InOutSine).SetUpdate(true);

        //activation interaction nouvelle page
        for (int i = currentPageColors * itemsPerPage; i < currentPageColors * itemsPerPage + itemsPerPage; i++)
        {
            colorIcons_btns[i].interactable = true;
        }
    }

    public void OnClickProfil(int id)
    {
        //surbrillance profil choisi

        if (currentColorID != -1)
        {
            colorIcons_btns[currentColorID - 1].GetComponent<Image>().color = new Color32(184, 229, 255, 0);
        }

        currentColorID = id;

        colorIcons_btns[id - 1].GetComponent<Image>().color = new Color32(184, 229, 255, 255);

        //activation du bouton de confirmation
        confirm_btn.interactable = true;

        OnClickButtonConfirm();
    }

    public void OnClickButtonConfirm()
    {
        //information du profil spécifique au contexte

        HM_DataController.model.currentColorID = currentColorID;
        HM_DataController.model.Save();

        //HM_Scripts.optionPanel.Init();

        switch (HM_DataController.model.currentColorID)
        {
            case 1:
                pluginController.colorDeviceInfo.color = new Color32(246, 115, 172, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "A";
                break;
            case 2:
                pluginController.colorDeviceInfo.color = new Color32(155, 47, 174, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "B";
                break;
            case 3:
                pluginController.colorDeviceInfo.color = new Color32(64, 84, 178, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "C";
                break;
            case 4:
                pluginController.colorDeviceInfo.color = new Color32(29, 170, 241, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "D";
                break;
            case 5:
                pluginController.colorDeviceInfo.color = new Color32(31, 188, 210, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "E";
                break;
            case 6:
                pluginController.colorDeviceInfo.color = new Color32(80, 174, 85, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "F";
                break;
            case 7:
                pluginController.colorDeviceInfo.color = new Color32(205, 218, 73, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "G";
                break;
            case 8:
                pluginController.colorDeviceInfo.color = new Color32(253, 192, 47, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "H";
                break;
            case 9:
                pluginController.colorDeviceInfo.color = new Color32(164, 125, 112, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "I";
                break;
            case 10:
                pluginController.colorDeviceInfo.color = new Color32(97, 125, 138, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "J";
                break;
            case 11:
                pluginController.colorDeviceInfo.color = new Color32(255, 140, 120, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "K";
                break;
            case 12:
                pluginController.colorDeviceInfo.color = new Color32(0, 121, 135, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "L";
                break;
            case 13:
                pluginController.colorDeviceInfo.color = new Color32(30, 223, 145, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "M";
                break;
            case 14:
                pluginController.colorDeviceInfo.color = new Color32(201, 130, 255, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "N";
                break;
            case 15:
                pluginController.colorDeviceInfo.color = new Color32(255, 49, 49, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "O";
                break;
            case 16:
                pluginController.colorDeviceInfo.color = new Color32(68, 74, 255, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "P";
                break;
            case 17:
                pluginController.colorDeviceInfo.color = new Color32(255, 110, 13, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Q";
                break;
            case 18:
                pluginController.colorDeviceInfo.color = new Color32(84, 67, 59, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "R";
                break;
            case 19:
                pluginController.colorDeviceInfo.color = new Color32(37, 96, 58, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "S";
                break;
            case 20:
                pluginController.colorDeviceInfo.color = new Color32(36, 28, 110, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "T";
                break;
            case 21:
                pluginController.colorDeviceInfo.color = new Color32(168, 94, 94, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "U";
                break;
            case 22:
                pluginController.colorDeviceInfo.color = new Color32(128, 94, 168, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "V";
                break;
            case 23:
                pluginController.colorDeviceInfo.color = new Color32(126, 74, 74, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "W";
                break;
            case 24:
                pluginController.colorDeviceInfo.color = new Color32(131, 183, 151, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "X";
                break;
            case 25:
                pluginController.colorDeviceInfo.color = new Color32(226, 132, 74, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Y";
                break;
            case 26:
                pluginController.colorDeviceInfo.color = new Color32(255, 68, 212, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Z";
                break;
            case 27:
                pluginController.colorDeviceInfo.color = new Color32(127, 47, 235, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Δ";
                break;
            case 28:
                pluginController.colorDeviceInfo.color = new Color32(95, 95, 95, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Θ";
                break;
            case 29:
                pluginController.colorDeviceInfo.color = new Color32(97, 158, 196, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Ξ";
                break;
            case 30:
                pluginController.colorDeviceInfo.color = new Color32(214, 167, 113, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Φ";
                break;
            case 31:
                pluginController.colorDeviceInfo.color = new Color32(123, 53, 110, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Ψ";
                break;
            case 32:
                pluginController.colorDeviceInfo.color = new Color32(89, 96, 1, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Ω";
                break;
            case -1:
                pluginController.colorDeviceInfo.color = new Color32(200, 200, 200, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "-";
                break;
            default:
                pluginController.colorDeviceInfo.color = new Color32(200, 200, 200, 255);
                pluginController.colorDeviceInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "-";
                break;
        }


        if (HM_DataController.newTeacherAccountTemp != null)
        {
            CloseContent(HM_Scripts.contentConfig);
        }
        else
        {
            CloseContent(HM_Scripts.contentProfil);
        }
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }

}

