using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class HM_ContentGroupsChoice : HM_Content
{
    public Button groupsPreviousPage_btn;
    public Button groupsNextPage_btn;

    public RectTransform groupsPages_rtf;
    public List<Button> groupsIcons_btns;

    public Button back_btn;
    public Button confirm_btn;

    public TextMeshProUGUI nbrComptesText;

    private int currentPageProfils = 0;
    private int nombrePages = 1;
    private int itemsPerPage = 16;
    private List<int> currentGroups = new List<int>();
    private List<int> currentGroupsStart = new List<int>();
    private List<int> currentGroupsDelete = new List<int>();
    private List<int> currentGroupsActive = new List<int>();

    private HM_TeacherAccount currentTeacherAccount = null;

    [HideInInspector]
    public int navID = 0;

    public override int InitContent()
    {
        //pluginController.colorDeviceInfo.enabled = true;

        /*if (pluginController.sceneStart && HM_Scripts.optionPanel.isButtonVisible)
        {
            HM_Scripts.optionPanel.HideButton();
        }*/

        //remise � z�ro de tous les param�tres du contenu

        if (HM_DataController.newTeacherAccountTemp != null)
        {
            currentTeacherAccount = HM_DataController.newTeacherAccountTemp;
        }
        else if (HM_DataController.currentTeacherAccount != null)
        {
            currentTeacherAccount = HM_DataController.currentTeacherAccount;
        }

        for (int i = 0; i < groupsIcons_btns.Count; i++)
        {
            ColorBlock cb = new ColorBlock();
            cb.normalColor = new Color32(255, 255, 255, 255);
            cb.highlightedColor = new Color32(217, 217, 217, 255);
            cb.pressedColor = new Color32(166, 166, 166, 255);
            cb.selectedColor = new Color32(166, 166, 166, 255);
            cb.disabledColor = new Color32(255, 255, 255, 30);
            cb.colorMultiplier = 1;
            groupsIcons_btns[i].colors = cb;
            groupsIcons_btns[i].transform.GetChild(1).gameObject.SetActive(false);
        }

        currentGroups = new List<int>();
        currentGroupsStart = new List<int>();
        currentGroupsDelete = new List<int>();
        currentGroupsActive = new List<int>();

        currentPageProfils = 0;
        for (int i = currentPageProfils * itemsPerPage; i < (currentPageProfils * itemsPerPage + itemsPerPage) && (i < groupsIcons_btns.Count); i++)
        {
            groupsIcons_btns[i].interactable = true;
            groupsIcons_btns[i].transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = new Color32(97, 97, 97, 255);

            foreach(HM_TeacherAccount teacherAccount in HM_DataController.model.teacherAccounts)
            {
                if(teacherAccount.teacherCode.ToUpper().Equals(currentTeacherAccount.teacherCode.ToUpper()))
                {
                    foreach (HM_ChildUser child in teacherAccount.childUsers)
                    {
                        if(child.genericGroup >= 1)
                        {
                            if (!currentGroupsStart.Contains(child.genericGroup))
                                currentGroupsStart.Add(child.genericGroup);

                            if (!currentGroups.Contains(child.genericGroup))
                                currentGroups.Add(child.genericGroup);

                            if (child.activeUser && child.isArchived == false)
                            {
                                groupsIcons_btns[child.genericGroup - 1].transform.GetChild(1).gameObject.SetActive(true);
                                if (!currentGroupsActive.Contains(child.genericGroup))
                                    currentGroupsActive.Add(child.genericGroup);
                            }
                        }
                    }

                    foreach (int groupID in teacherAccount.genericGroupsUsed)
                    {
                        SelectChoice(groupID);
                    }
                }
                else
                {
                    foreach (int groupID in teacherAccount.genericGroupsUsed)
                    {
                        DeactivateChoice(groupID);
                    }
                }
            }
        }

        groupsPages_rtf.anchoredPosition = Vector2.zero;
        groupsPreviousPage_btn.interactable = false;
        groupsNextPage_btn.interactable = true;
        
        confirm_btn.interactable = true;
        //confirm_btn.interactable = false;

        if (HM_DataController.currentTeacherAccount != null)
        {
            //back_btn.gameObject.SetActive(true);

            if (HM_DataController.model.teacherAccounts.Count <= 1)
            {
                nbrComptesText.text = "";
            }
            else if (HM_DataController.model.teacherAccounts.Count == 2)
            {
                nbrComptesText.text = "( P�riph�rique utilis� par 1 autre enseignant ) ";
            }
            else
            {
                nbrComptesText.text = "( P�riph�rique utilis� par " + (HM_DataController.model.teacherAccounts.Count-1) + " autres enseignants ) ";
            }
        }
        else
        {
            //back_btn.gameObject.SetActive(false);

            if (HM_DataController.model.teacherAccounts.Count <= 0)
            {
                nbrComptesText.text = "";
            }
            else if (HM_DataController.model.teacherAccounts.Count == 1)
            {
                nbrComptesText.text = "( P�riph�rique utilis� par 1 autre enseignant ) ";
            }
            else
            {
                nbrComptesText.text = "( P�riph�rique utilis� par " + (HM_DataController.model.teacherAccounts.Count) + " autres enseignants ) ";
            }
        }

        return 0;
    }

    public override void OnOpenedContent()
    {
        return;
    }

    //navigation d'une page � une autre
    public void OnClickNavPage(int value)
    {
        //d�sactivation interaction page courante
        for (int i = currentPageProfils * itemsPerPage; i < groupsIcons_btns.Count && i < currentPageProfils * itemsPerPage + itemsPerPage; i++)
        {
            groupsIcons_btns[i].interactable = false;
        }

        if (value == 1)//next page
        {
            currentPageProfils++;

            if (currentPageProfils == nombrePages-1)
                groupsNextPage_btn.interactable = false;
            else
                groupsNextPage_btn.interactable = true;

            groupsPreviousPage_btn.interactable = true;
        }
        else//previous page
        {
            currentPageProfils--;

            if (currentPageProfils == 0)
                groupsPreviousPage_btn.interactable = false;
            else
                groupsPreviousPage_btn.interactable = true;

            groupsNextPage_btn.interactable = true;
        }

        //animation de transition
        groupsPages_rtf.DOAnchorPosX(-currentPageProfils * 544, 0.5f).SetEase(Ease.InOutSine).SetUpdate(true);

        //activation interaction nouvelle page
        for (int i = currentPageProfils * itemsPerPage; i < groupsIcons_btns.Count && i < currentPageProfils * itemsPerPage + itemsPerPage; i++)
        {
            groupsIcons_btns[i].interactable = true;

            foreach (HM_TeacherAccount teacherAccount in HM_DataController.model.teacherAccounts)
            {
                foreach (int groupID in teacherAccount.genericGroupsUsed)
                {
                    DeactivateChoice(groupID);
                }
            }
        }
    }

    public void SelectChoice(int id)
    {
        if (!currentGroups.Contains(id))
        {
            currentGroups.Add(id);
        }

        if (currentGroupsStart.Contains(id) && currentGroupsDelete.Contains(id))
        {
            currentGroupsDelete.Remove(id);
        }

        groupsIcons_btns[id-1].transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = new Color32(240, 240, 240, 255);

        ColorBlock cb = new ColorBlock();
        cb.normalColor = new Color32(102, 83, 181, 255);
        cb.highlightedColor = new Color32(86, 58, 200, 255);
        cb.pressedColor = new Color32(72, 32, 200, 255);
        cb.selectedColor = new Color32(102, 83, 181, 255);
        cb.disabledColor = new Color32(102, 83, 181, 30);
        cb.colorMultiplier = 1;
        groupsIcons_btns[id - 1].colors = cb;
    }
    public void UnselectChoice(int id)
    {
        currentGroups.Remove(id);

        if(currentGroupsStart.Contains(id) && !currentGroupsDelete.Contains(id))
        {
            currentGroupsDelete.Add(id);
        }
        
        groupsIcons_btns[id - 1].transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = new Color32(97, 97, 97, 255);

        ColorBlock cb = new ColorBlock();
        cb.normalColor = new Color32(255, 255, 255, 255);
        cb.highlightedColor = new Color32(217, 217, 217, 255);
        cb.pressedColor = new Color32(166, 166, 166, 255);
        cb.selectedColor = new Color32(166, 166, 166, 255);
        cb.disabledColor = new Color32(255, 255, 255, 30);
        cb.colorMultiplier = 1;
        groupsIcons_btns[id - 1].colors = cb;
    }
    public void DeactivateChoice(int id)
    {
        groupsIcons_btns[id - 1].interactable = false;
        groupsIcons_btns[id - 1].transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = new Color32(97, 97, 97, 80);
    }

    public void OnClickProfil(int id)
    {
        if(currentGroupsActive.Contains(id))
        {
            HM_Scripts.popupLotInfo.OpenContent();
            return;
        }

        if(currentGroups.Contains(id))
        {
            UnselectChoice(id);
        }
        else
        {
            SelectChoice(id);
        }

        /*if(currentGroupsID.Count > 0)
        {
            confirm_btn.interactable = true;
        }
        else
        {
            confirm_btn.interactable = false;
        }*/
    }
    public void OnClickButtonBack()
    {
        CloseContent(HM_Scripts.contentProfil);
    }

    public void OnClickButtonConfirm()
    {
        confirm_btn.interactable = false;

        //si on a retir� un compte qui �tait l� avant il faut supprimer les comptes �l�ves et retirer ce compte
        //va se baser sur currentGroupsDelete

        if (HM_DataController.currentTeacherAccount != null)
        {
            foreach (int i in currentGroupsDelete)
            {
                HM_DataController.currentTeacherAccount.childUsers.RemoveAll(a => a.genericGroup == i);
                HM_DataController.currentTeacherAccount.genericGroupsUsed.Remove(i);
            }
        }

        //gestion des comptes � rajouter
        foreach (int i in currentGroups)
        {
            if (currentGroupsStart.Contains(i))
                continue;

            if (HM_DataController.currentTeacherAccount == null)//cr�ation d'un nouveau compte
            {
                HM_DataController.newTeacherAccountTemp.genericGroupsUsed.Add(i);
            }
            else
            {
                HM_DataController.currentTeacherAccount.genericGroupsUsed.Add(i);
            }

            for (int j = 1; j <= 40; j++)
            {
                HM_ChildUser childUser = new HM_ChildUser();

                childUser.userID = System.Guid.NewGuid().ToString();
                childUser.userIconID = j;
                childUser.genericGroup = i;
                childUser.isGeneric = true;
                childUser.fromGeneric = true;
                childUser.revisionDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

                childUser.schoolGrade = "CP";
                /*if (1 <= i && i <= 3)
                {
                    childUser.schoolGrade = "CP";
                }
                else if (4 <= i && i <= 6)
                {
                    childUser.schoolGrade = "CE1";
                }
                else if (7 <= i && i <= 9)
                {
                    childUser.schoolGrade = "CE2";
                }
                else if (10 <= i && i <= 12)
                {
                    childUser.schoolGrade = "CM1";
                }
                else if (13 <= i && i <= 15)
                {
                    childUser.schoolGrade = "CM2";
                }*/

                childUser.loginCode = HM_ChildCodes.childCodes[i - 1, j - 1].ToString();

                if (HM_DataController.currentTeacherAccount == null)//cr�ation d'un nouveau compte
                {
                    HM_DataController.newTeacherAccountTemp.childUsers.Add(childUser);
                }
                else
                {
                    HM_DataController.currentTeacherAccount.childUsers.Add(childUser);
                }
            }
        }

        if (HM_DataController.currentTeacherAccount == null)//cr�ation d'un nouveau compte
        {
            HM_DataController.model.teacherAccounts.Add(HM_DataController.newTeacherAccountTemp);

            HM_DataController.model.Save();

            HM_Scripts.optionPanel.UpdateData();

            CloseContent(HM_Scripts.contentProfil);

            /*if(HM_DataController.newTeacherAccountTemp.dataAWS._idToken != null && HM_DataController.newTeacherAccountTemp.dataAWS._idToken.Length > 0)
            {
                HM_DataController.newTeacherAccountTemp = null;
                CloseContent(HM_Scripts.contentSynchroAWS);
            }
            else
            {
                HM_DataController.newTeacherAccountTemp = null;
                CloseContent(HM_Scripts.contentProfil);
            }*/
        }
        else
        {
            HM_DataController.model.Save();

            CloseContent(HM_Scripts.contentProfil);
        }
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }

}

