using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

//contenu principal permettant de rentrer un code � 6 chiffres
public class HM_ContentCode : HM_Content
{
    public enum CODE_TYPE
    {
        LOGIN_CHILD,
        LOGIN_TEACHER,
    }

    public CODE_TYPE codeType = CODE_TYPE.LOGIN_CHILD;

    public TextMeshProUGUI debugText;

    public TextMeshProUGUI titleText;
    public TextMeshProUGUI subtitleText;

    public static int profilID = -1;
    public Image profilImage;

    public Button codeBack_btn;

    public TextMeshProUGUI errorText;

    public TMP_InputField hiddenInputCode_if;
    public List<TMP_InputField> code_if;

    private string currentCode = "";

    public Transform panelError;
    private bool panelErrorOpen = false;

    public List<Button> inputButtons;

    [HideInInspector]
    public int navID = 0;

    public bool canInteract = true;

    public override int InitContent()
    {
        //pluginController.colorDeviceInfo.enabled = true;

        debugText.text = "DEBUG - les codes autoris�s ici sont : ";

        if (codeType == CODE_TYPE.LOGIN_CHILD)
        {
            //canInteract = false;
            canInteract = true;

            if (HM_DataController.currentChildUser != null)
            {
                titleText.text = "�valuation - �tape 2/2";
            }
            else
            {
                titleText.text = "�tape 2/2";
            }

            if (profilID == -1)
            {
                Debug.LogError("profil de type ChildUser inexistant");
                return -1;
            }

            profilImage.sprite = HM_Scripts.contentProfil.profilIcons_btns[profilID-1].targetGraphic.GetComponent<Image>().sprite;

            /*if (pluginController.sceneStart && HM_Scripts.optionPanel.isButtonVisible)
            {
                HM_Scripts.optionPanel.HideButton();
            }*/

            codeBack_btn.gameObject.SetActive(true);

            string code = "";
            foreach(HM_TeacherAccount t in HM_DataController.model.teacherAccounts)
            {
                foreach(int i in t.genericGroupsUsed)
                {
                    if (i < 10)
                    {
                        code = "0" + i + "00";
                    }
                    else
                    {
                        code = "" + i + "00";
                    }

                    if (profilID < 10)
                    {
                        code += "0" + profilID;
                    }
                    else
                    {
                        code += "" + profilID;
                    }

                    debugText.text += code + " , ";
                }
            }
        }
        else if (codeType == CODE_TYPE.LOGIN_TEACHER)
        {
            if (HM_DataController.currentTeacherAccount == null)
            {
                if (HM_DataController.model.teacherAccounts.Count == 0)
                {
                    titleText.text = "Bienvenue";
                }
                else
                {
                    titleText.text = "Identifiant professeur";
                }
                debugText.text += "1A1A1A , 2B2B2B , 3C3C3C, 4D4D4D";

                if (pluginController.sceneStart)
                {
                    /*HM_Scripts.optionPanel.backMenuButton.gameObject.SetActive(true);
                    HM_Scripts.optionPanel.teacherAccountButton.gameObject.SetActive(false);
                    HM_Scripts.optionPanel.disconnectButton.gameObject.SetActive(false);*/

                    /*if (HM_DataController.model.teacherAccounts.Count == 0)
                    {
                        if(HM_Scripts.optionPanel.isButtonVisible)
                        {
                            HM_Scripts.optionPanel.HideButton();
                        }
                    }
                    else
                    {
                        if (!HM_Scripts.optionPanel.isButtonVisible)
                        {
                            HM_Scripts.optionPanel.ShowButton();
                        }
                    }*/
                }

                codeBack_btn.gameObject.SetActive(false); 

                errorText.text = "Cet identifiant n'est pas reconnu";
            }
            else
            {
                titleText.text = "Identifiant professeur";
                //valeur par d�faut param�trage Luciole 
                if (pluginController.appID == APP_ID.LUCIOLE)
                {
                    HM_DataController.currentChildUser.appLucioleUserData.settings.skipLevel = false;//par d�faut toujours
                    HM_DataController.model.Save();
                }

                debugText.text += ""+HM_DataController.currentTeacherAccount.teacherCode;

                codeBack_btn.gameObject.SetActive(true);

                errorText.text = "Cet identifiant n'est pas celui associ� � cet enfant.";
            }
        }

        //remise � z�ro de tous les param�tres du contenu

        currentCode = "";

        if (hiddenInputCode_if != null)
            hiddenInputCode_if.text = "";

        if (codeType == CODE_TYPE.LOGIN_CHILD)
        {
            for (int i = 0; i < code_if.Count; i++)
            {
                code_if[i].text = "";
                code_if[i].textComponent.color = new Color32(3, 160, 201, 255);
            }
        }
        else
        {
            for (int i = 0; i < code_if.Count; i++)
            {
                code_if[i].text = "";
                code_if[i].textComponent.color = new Color32(102, 83, 181, 255);
            }
        }

        panelError.localPosition = new Vector2(0, 75);
        panelErrorOpen = false;
        errorText.gameObject.SetActive(false);

#if UNITY_ANDROID //&& !UNITY_EDITOR
        keyboardOpen = false;
#endif

        return 0;
    }
    public override void OnOpenedContent()
    {
        return;
    }

#if UNITY_ANDROID //&& !UNITY_EDITOR
    private bool keyboardOpen = false;
#endif
    private void Update()
    {
#if UNITY_ANDROID //&& !UNITY_EDITOR
        if(codeType == CODE_TYPE.LOGIN_TEACHER && !keyboardOpen && TouchScreenKeyboard.visible == true)
        {
            keyboardOpen = true;

            this.gameObject.transform.DOLocalMoveY(90, 0.2f).SetEase(Ease.InOutSine);
        }
        else if(codeType == CODE_TYPE.LOGIN_TEACHER && keyboardOpen && TouchScreenKeyboard.visible == false)
        {
            keyboardOpen = false;

            this.gameObject.transform.DOLocalMoveY(0, 0.2f).SetEase(Ease.InOutSine);
        }
#endif
    }

    public void OnClickButtonBack()
    {
        if (!canInteract)
            return;

        //redirection sp�cifique au contexte

        if (codeType == CODE_TYPE.LOGIN_CHILD)
        {
            CloseContent(HM_Scripts.contentProfil);
        }
        else if (codeType == CODE_TYPE.LOGIN_TEACHER)
        {
            pluginController.onAppSettingCancel?.Invoke();

            CloseContent();
        }
    }

    public void OnInputCodeSelect()
    {
        //on remet � z�ro le code en cas de nouvelle s�lection

        currentCode = "";

        foreach (TMP_InputField input in code_if)
        {
            input.text = "";
        }

        hiddenInputCode_if.text = "";
    }

    public void OnInputCodeChange()
    {
        if (panelErrorOpen)
        {
            panelErrorOpen = false;
            panelError.DOLocalMoveY(75.0f, 0.5f).SetEase(Ease.InSine).SetUpdate(true);
            OnCodeDelete(true);
        }

        //renseignement des cases avec chaque valeur

        for (int i = 0; i < code_if.Count; i++)
        {
            if (i < hiddenInputCode_if.text.Length)
            {
                code_if[i].text = "" + hiddenInputCode_if.text.ToUpper()[i];
            }
            else
            {
                code_if[i].text = "";
            }
        }

        if (hiddenInputCode_if.text.Length == code_if.Count)
        {
            //si le code est complet on peut le v�rifier

            currentCode = hiddenInputCode_if.text.ToUpper();
            CodeCheck();
        }
        else
        {
            //sinon on doit le d�sactiver

            currentCode = "";
        }
    }

    public void OnInputCodeChange(int value)
    {
        if (!canInteract)
            return;

        if (panelErrorOpen)
        {
            panelErrorOpen = false;
            panelError.DOLocalMoveY(75.0f, 0.5f).SetEase(Ease.InSine).SetUpdate(true);
            OnCodeDelete(true);
        }

        //renseignement des cases avec chaque valeur

        for (int i = 0; i < code_if.Count; i++)
        {
            if (code_if[i].text.Length == 0)
            {
                code_if[i].text = ""+value;
                break;
            }
        }

        if (code_if[code_if.Count-1].text.Length != 0)
        {
            currentCode = "";

            for (int i = 0; i < code_if.Count; i++)
            {
                currentCode += code_if[i].text;
            }

            CodeCheck();
        }
    }

    public void OnCodeDelete(bool all = false)
    {
        if (panelErrorOpen)
        {
            panelErrorOpen = false;
            panelError.DOLocalMoveY(75.0f, 0.5f).SetEase(Ease.InSine).SetUpdate(true);
            for (int i = 0; i < code_if.Count; i++)
            {
                code_if[i].textComponent.color = new Color32(3, 160, 201, 255);
            }
            OnCodeDelete();
            return;
        }
       
        if(codeType == CODE_TYPE.LOGIN_CHILD)
        {
            if(all)
            {
                for (int i = 0; i < code_if.Count; i++)
                {
                    code_if[i].text = "";
                    code_if[i].textComponent.color = new Color32(3, 160, 201, 255);
                }
            }
            else
            {
                for (int i = code_if.Count -1 ; i >= 0; i--)
                {
                    if(code_if[i].text.Length > 0)
                    {
                        code_if[i].text = "";
                        code_if[i].textComponent.color = new Color32(3, 160, 201, 255);
                        break;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < code_if.Count; i++)
            {
                code_if[i].text = "";
                code_if[i].textComponent.color = new Color32(102, 83, 181, 255);
            }
        }

        errorText.gameObject.SetActive(false);
        currentCode = "";
    }

    public void CodeCheck()
    {
        //v�rification de validit� du code rent�

        if (currentCode.Length != code_if.Count)
            return;

        if(codeType == CODE_TYPE.LOGIN_CHILD)
        {
            int errorBadDevice = -1;
            bool errorArchived = false;
            if (HM_DataController.currentChildUser != null)//si connexion profil secondaire celui-ci doit apartenir � celui du teacher account du profil principal, et en pas �tre le m�me que l'enfant courant
            {
                foreach (HM_ChildUser child in HM_DataController.currentTeacherAccount.childUsers)
                {
                    if (profilID == child.userIconID && profilID != HM_DataController.currentChildUser.userIconID && child.loginCode.Equals(currentCode) && child.isArchived == true)
                    {
                        errorArchived = true;
                    }

                    if (profilID == child.userIconID && child.isArchived == false && profilID != HM_DataController.currentChildUser.userIconID && child.loginCode.Equals(currentCode))
                    {
                        //Debug.Log("HM_Plugin : LOGIN OK");

                        HM_DataController.secondaryChildUser = child;
                        HM_DataController.secondaryChildUser.activeUser = true;
                        HM_DataController.model.Save();
                        pluginController.callbackSecondChildLogin?.Invoke();
                        CloseContent();

                        return;
                    }
                }
            }
            else//connexion profil principal
            {
                foreach (HM_TeacherAccount teacherAccount in HM_DataController.model.teacherAccounts)
                {
                    foreach (HM_ChildUser child in teacherAccount.childUsers)
                    {
                        if(profilID == child.userIconID && child.loginCode.Equals(currentCode) && child.deviceColor != HM_DataController.model.currentColorID)
                        {
                            errorBadDevice = child.deviceColor;
                        }

                        if (profilID == child.userIconID && child.loginCode.Equals(currentCode) && child.isArchived == true)
                        {
                            errorArchived = true;
                        }

                        if (profilID == child.userIconID && child.isArchived == false && child.loginCode.Equals(currentCode) && (child.deviceColor == -1 || child.deviceColor == HM_DataController.model.currentColorID))
                        {
                            //Debug.Log("HM_Plugin : LOGIN OK");

                            HM_DataController.currentChildUser = child;
                            HM_DataController.currentChildUser.activeUser = true;
                            HM_DataController.currentTeacherAccount = teacherAccount;
                            HM_DataController.model.Save();

#if UNITY_ANDROID
                            //on stoppe le plugin bluetooth si actif
                            if (HM_NetworkController.instance.blt_initialized)
                            {
                                HM_NetworkController.instance.BLT_DeInitialize(true);
                            }
#endif

                            CloseContent(HM_Scripts.contentBonjour);

                            return;
                        }
                    }
                }
            }

            if (errorArchived)
            {
                errorText.text = "Ce profil a �t� archiv�";
            }
            else if (errorBadDevice != -1)
            {
                errorText.text = "Ce code est actuellement � utiliser sur le p�riph�rique "+ (char)('A' + errorBadDevice - 1);
            }
            else
            {
                errorText.text = "Ce mot de passe ne convient pas ou le compte n'est pas synchronis�";
            }
            
            //error code
            foreach (Button b in inputButtons)
                b.interactable = false;

            for (int i = 0; i < code_if.Count; i++)
            {
                code_if[i].textComponent.color = new Color32(252, 126, 126, 255);
            }

            errorText.gameObject.SetActive(true);

            panelErrorOpen = true;
            panelError.DOLocalMoveY(0.0f, 0.5f).SetEase(Ease.OutSine).SetUpdate(true).OnComplete(() =>
            {
                foreach (Button b in inputButtons)
                    b.interactable = true;
            });
        }
        else if (codeType == CODE_TYPE.LOGIN_TEACHER)
        {
            EventSystem.current.SetSelectedGameObject(null);//en enl�ve le focus, ce qui va fermer le clavier

            if(HM_DataController.currentChildUser == null)//on v�rifie si le code est ok parmis l'ensemble des codes enseignant autoris�s
            {
                if (pluginController.CheckTeacherCodeValid(currentCode))
                {
                    if (HM_PluginController.navContext == HM_PluginController.NAV_CONTEXT.LOT)
                    {
                        foreach (HM_TeacherAccount teacherAccount in HM_DataController.model.teacherAccounts)
                        {
                            if (teacherAccount.teacherCode.ToUpper().Equals(currentCode.ToUpper()))
                            {
                                HM_DataController.currentTeacherAccount = teacherAccount;

                                if (Application.internetReachability != NetworkReachability.NotReachable && (HM_DataController.currentTeacherAccount.dataAWS._idToken == null || HM_DataController.currentTeacherAccount.dataAWS._idToken.Length == 0))
                                {
                                    CloseContent(HM_Scripts.contentLoginAWS);
                                }
                                else
                                {
                                    CloseContent(HM_Scripts.contentGroupsChoice);
                                }

                                return;
                            }
                        }

                        errorText.gameObject.SetActive(true);
                        return;
                    }
                    else if (HM_PluginController.navContext == HM_PluginController.NAV_CONTEXT.LETTRE)
                    {
                        foreach (HM_TeacherAccount teacherAccount in HM_DataController.model.teacherAccounts)
                        {
                            if (teacherAccount.teacherCode.ToUpper().Equals(currentCode.ToUpper()))
                            {
                                HM_DataController.currentTeacherAccount = teacherAccount;

                                CloseContent(HM_Scripts.contentDeviceColor);

                                return;
                            }
                        }

                        errorText.gameObject.SetActive(true);
                        return;
                    }
                    else if (HM_PluginController.navContext == HM_PluginController.NAV_CONTEXT.NEW_TEACHER)
                    {
                        foreach (HM_TeacherAccount teacherAccount in HM_DataController.model.teacherAccounts)
                        {
                            if (teacherAccount.teacherCode.ToUpper().Equals(currentCode.ToUpper()))
                            {
                                HM_DataController.currentTeacherAccount = teacherAccount;
                                HM_Scripts.popupAlertDissocier.OpenContent();
                                return;
                            }
                        }

                        HM_DataController.newTeacherAccountTemp = new HM_TeacherAccount();
                        HM_DataController.newTeacherAccountTemp.teacherCode = currentCode;

                        if (HM_DataController.model.teacherAccounts.Count == 0)
                        {
                            CloseContent(HM_Scripts.contentDeviceColor);
                        }
                        else
                        {
                            CloseContent(HM_Scripts.contentConfig);
                            /*if (Application.internetReachability != NetworkReachability.NotReachable)
                            {
                                CloseContent(HM_Scripts.contentLoginAWS);
                            }
                            else
                            {
                                CloseContent(HM_Scripts.contentGroupsChoice);
                            }*/
                        }

                        return;
                    }
                    else if (HM_PluginController.navContext == HM_PluginController.NAV_CONTEXT.SYNCHRO)
                    {
                        foreach (HM_TeacherAccount teacherAccount in HM_DataController.model.teacherAccounts)
                        {
                            if (teacherAccount.teacherCode.ToUpper().Equals(currentCode.ToUpper()))
                            {
                                HM_DataController.currentTeacherAccount = teacherAccount;

                                if (Application.internetReachability != NetworkReachability.NotReachable && HM_DataController.currentTeacherAccount.dataAWS._idToken != null && HM_DataController.currentTeacherAccount.dataAWS._idToken.Length > 0)
                                {
                                    CloseContent(HM_Scripts.contentSynchroAWS);
                                }
                                else
                                {
                                    CloseContent(HM_Scripts.contentLoginAWS);
                                }

                                return;
                            }
                        }

                        errorText.gameObject.SetActive(true);
                        return;
                    }
                        /*else
                        {
                            context = CONTEXT.NONE;
                            //si compte existant dans l'appareil, redirection page d'info du compte
                            foreach (HM_TeacherAccount teacherAccount in HM_DataController.model.teacherAccounts)
                            {
                                if (teacherAccount.teacherCode.Equals(currentCode))
                                {
                                    HM_DataController.currentTeacherAccount = teacherAccount;

                                    if (Application.internetReachability != NetworkReachability.NotReachable && HM_DataController.currentTeacherAccount.dataAWS._idToken == null || HM_DataController.currentTeacherAccount.dataAWS._idToken.Length == 0)
                                    {
                                        CloseContent(HM_Scripts.contentLoginAWS);
                                    }
                                    else
                                    {
                                        CloseContent(HM_Scripts.contentTeacherAccount);
                                    }

                                    return;
                                }
                            }
                        }


                        //sinon ajout d'un nouveau compte
                        HM_DataController.newTeacherAccountTemp = new HM_TeacherAccount();
                        HM_DataController.newTeacherAccountTemp.teacherCode = currentCode;

                        if(Application.internetReachability != NetworkReachability.NotReachable)
                        {
                            CloseContent(HM_Scripts.contentLoginAWS);
                        }
                        else
                        {
                            if (HM_DataController.model.teacherAccounts.Count == 0)
                            {
                                CloseContent(HM_Scripts.contentDeviceColor);
                            }
                            else
                            {
                                CloseContent(HM_Scripts.contentGroupsChoice);
                            }
                        }*/
                    }
            }
            else//on cherche � rentrer dans les settings que d'un teacher account en particulier
            {
                if (currentCode.Equals(HM_DataController.currentTeacherAccount.teacherCode))
                {
                    switch (pluginController.appID)
                    {
                        case APP_ID.NONE:
                            Debug.LogError("Pas de param�trage possible sans avoir indiqu� l'app concern�e");
                            break;
                        case APP_ID.LUCIOLE:
                            CloseContent(HM_Scripts.contentSettingApp_Luciole);
                            return;
                        case APP_ID.ELARGIR:
                            CloseContent(HM_Scripts.contentSettingApp_Elargir);
                            return;
                        case APP_ID.EVASION:
                            Debug.LogError("Pas de param�trage possible pour EVASION");
                            break;
                        case APP_ID.EVALULU:
                            Debug.LogError("Pas de param�trage possible pour EVALULU");
                            break;
                    }
                }
            }

            //error code
            for (int i = 0; i < code_if.Count; i++)
            {
                code_if[i].textComponent.color = new Color32(252, 126, 126, 255);
            }

            panelErrorOpen = true;
            panelError.DOLocalMoveY(0.0f, 0.5f).SetEase(Ease.OutSine).SetUpdate(true);

            errorText.gameObject.SetActive(true);


        }


    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }
}
