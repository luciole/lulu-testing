using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//contenu principal permettant de choisir un profil parmi ceux pr�sent
//pour l'instant que via 40 profils g�n�riques, sera adapt� sur la prochaine version pour afficher les profils d'une configuration sp�cifique � un compte enseignant
public class HM_ContentProfil : HM_Content
{
    public Button profilPreviousPage_btn;
    private Tween button1Tween = null;
    public Button profilNextPage_btn;
    private Tween button2Tween = null;

    public RectTransform profilsPages_rtf;
    public List<Button> profilIcons_btns;

    public GameObject bluetoothIconOn;
    public GameObject bluetoothIconOff;

    public Button confirm_btn;

    private int currentPageProfils = 0;
    private int nombrePages = 4;
    private int itemsPerPage = 10;
    private int currentProfilID = -1;

    public Image imageApp;
    public Sprite imageEvasion;
    public Sprite imageElargir;
    public Sprite imageLuciole;

    public Outline outlinePage1;
    public Outline outlinePage2;
    public Outline outlinePage3;
    public Outline outlinePage4;

    public TextMeshProUGUI title;

    public override int InitContent()
    {
        HM_PluginController.navContext = HM_PluginController.NAV_CONTEXT.NONE;

        HM_Scripts.contentProfil.bluetoothIconOn.SetActive(false);
        HM_Scripts.contentProfil.bluetoothIconOff.SetActive(false);
        HM_NetworkController.instance = pluginController.GetComponent<HM_NetworkController>();

        if(button1Tween != null)
        {
            button1Tween.Kill();
        }
        profilPreviousPage_btn.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        button1Tween = profilPreviousPage_btn.GetComponent<Image>().DOFade(0.0f, 1.0f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
        profilPreviousPage_btn.GetComponent<Image>().enabled = false;

        if (button2Tween != null)
        {
            button2Tween.Kill();
        }
        profilNextPage_btn.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        button2Tween = profilNextPage_btn.GetComponent<Image>().DOFade(0.0f, 1.0f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
        profilNextPage_btn.GetComponent<Image>().enabled = true;

        outlinePage1.enabled = true;
        outlinePage2.enabled = false;
        outlinePage3.enabled = false;
        outlinePage4.enabled = false;

        HM_Scripts.optionPanel.UpdateData();

#if UNITY_ANDROID
        //on prepare la connexion Bluetooth que pour la sc�ne de d�marrage
        if (HM_DataController.currentChildUser == null && pluginController.HasAndroidBluetoothPermissions())
        {
            if(!HM_NetworkController.instance.blt_initialized)
            {
                HM_NetworkController.instance.BLT_Initialize();
            }
            else
            {
                HM_Scripts.contentProfil.bluetoothIconOn.SetActive(true);
            }
        }
#endif
        //TODO IOS && WINDOWS


        //pluginController.colorDeviceInfo.gameObject.SetActive(true);

        //remise � z�ro de tous les param�tres du contenu

        currentProfilID = -1;

        if (pluginController.sceneStart)
        {
            /*HM_Scripts.optionPanel.backMenuButton.gameObject.SetActive(false);
            HM_Scripts.optionPanel.teacherAccountButton.gameObject.SetActive(true);
            HM_Scripts.optionPanel.disconnectButton.gameObject.SetActive(false);*/

            //Debug.Log(HM_Scripts.optionPanel.isButtonVisible);
            if(!HM_Scripts.optionPanel.isButtonVisible)
                HM_Scripts.optionPanel.ShowButton();

            HM_DataController.currentTeacherAccount = null;

            title.text = "�tape 1/2";
        }
        else
        {
            if (HM_Scripts.optionPanel.isButtonVisible)
                HM_Scripts.optionPanel.HideButton();

            if(HM_DataController.currentChildUser != null)
            {
                title.text = "�valuation - �tape 1/2";
            }
            else
            {
                title.text = "�tape 1/2";
            }
        }

        for (int i = 0; i < nombrePages * itemsPerPage; i++)
        {
            profilIcons_btns[i].GetComponent<Image>().color = new Color32(184, 229, 255, 0);
            profilIcons_btns[i].transform.GetChild(0).GetComponent<Image>().sprite = pluginController.childAvatars[i];
        }

        currentPageProfils = 0;
        for (int i = currentPageProfils * itemsPerPage; i < currentPageProfils * itemsPerPage + itemsPerPage; i++)
        {
            profilIcons_btns[i].interactable = true;
        }

        profilsPages_rtf.anchoredPosition = Vector2.zero;
        profilPreviousPage_btn.interactable = false;
        profilNextPage_btn.interactable = true;
        confirm_btn.interactable = false;

        /*switch(pluginController.appID)
        {
            case APP_ID.EVASION:
                imageApp.enabled = true;
                imageApp.sprite = imageEvasion;
                break;
            case APP_ID.ELARGIR:
                imageApp.enabled = true;
                imageApp.sprite = imageElargir;
                break;
            case APP_ID.LUCIOLE:
                imageApp.enabled = true;
                imageApp.sprite = imageLuciole;
                break;
            default:
                imageApp.enabled = false;
                break;
        }*/

        return 0;
    }

    public override void OnOpenedContent()
    {
        return;
    }

    public void GoToPage(int value)
    {
        //on invalide le choix profil fait si il existe
        if (currentProfilID != -1)
        {
            profilIcons_btns[currentProfilID - 1].GetComponent<Image>().color = new Color32(184, 229, 255, 0);
        }
        currentProfilID = -1;
        confirm_btn.interactable = false;

        //d�sactivation interaction page courante
        for (int i = currentPageProfils * itemsPerPage; i < currentPageProfils * itemsPerPage + itemsPerPage; i++)
        {
            profilIcons_btns[i].interactable = false;
        }

        profilPreviousPage_btn.interactable = true;
        profilPreviousPage_btn.GetComponent<Image>().enabled = true;
        profilNextPage_btn.interactable = true;
        profilNextPage_btn.GetComponent<Image>().enabled = true;
        currentPageProfils = value;

        outlinePage1.enabled = false;
        outlinePage2.enabled = false;
        outlinePage3.enabled = false;
        outlinePage4.enabled = false;
        switch (currentPageProfils)
        {
            case 0:
                outlinePage1.enabled = true;
                profilPreviousPage_btn.interactable = false;
                profilPreviousPage_btn.GetComponent<Image>().enabled = false;
                break;
            case 1:
                outlinePage2.enabled = true;
                break;
            case 2:
                outlinePage3.enabled = true;
                break;
            case 3:
                outlinePage4.enabled = true;
                profilNextPage_btn.interactable = false;
                profilNextPage_btn.GetComponent<Image>().enabled = false;
                break;
        }

        //animation de transition
        profilsPages_rtf.DOAnchorPosX(-currentPageProfils * 604, 0.5f).SetEase(Ease.InOutSine).SetUpdate(true);

        //activation interaction nouvelle page
        for (int i = currentPageProfils * 10; i < currentPageProfils * itemsPerPage + itemsPerPage; i++)
        {
            profilIcons_btns[i].interactable = true;
        }
    }


    //navigation d'une page � une autre
    public void OnClickNavPage(int value)
    {
        //on invalide le choix profil fait si il existe
        if (currentProfilID != -1)
        {
            profilIcons_btns[currentProfilID - 1].GetComponent<Image>().color = new Color32(184, 229, 255, 0);
        }
        currentProfilID = -1;
        confirm_btn.interactable = false;

        //d�sactivation interaction page courante
        for (int i = currentPageProfils * itemsPerPage; i < currentPageProfils * itemsPerPage + itemsPerPage; i++)
        {
            profilIcons_btns[i].interactable = false;
        }

        if (value == 1)//next page
        {
            currentPageProfils++;

            if (currentPageProfils == nombrePages-1)
            {
                profilNextPage_btn.interactable = false;
                profilNextPage_btn.GetComponent<Image>().enabled = false;
            }
            else
            {
                profilNextPage_btn.interactable = true;
                profilNextPage_btn.GetComponent<Image>().enabled = true;
            }

            profilPreviousPage_btn.interactable = true;

            profilPreviousPage_btn.GetComponent<Image>().enabled = true;
        }
        else//previous page
        {
            currentPageProfils--;

            if (currentPageProfils == 0)
            {
                profilPreviousPage_btn.interactable = false;
                profilPreviousPage_btn.GetComponent<Image>().enabled = false;
            }
            else
            {
                profilPreviousPage_btn.interactable = true;
                profilPreviousPage_btn.GetComponent<Image>().enabled = true;
            }

            profilNextPage_btn.interactable = true;

            profilNextPage_btn.GetComponent<Image>().enabled = true;
        }

        outlinePage1.enabled = false;
        outlinePage2.enabled = false;
        outlinePage3.enabled = false;
        outlinePage4.enabled = false;
        switch (currentPageProfils)
        {
            case 0:
                outlinePage1.enabled = true;
                break;
            case 1:
                outlinePage2.enabled = true;
                break;
            case 2:
                outlinePage3.enabled = true;
                break;
            case 3:
                outlinePage4.enabled = true;
                break;
        }

        //animation de transition
        profilsPages_rtf.DOAnchorPosX(-currentPageProfils * 604, 0.5f).SetEase(Ease.InOutSine).SetUpdate(true);

        //activation interaction nouvelle page
        for (int i = currentPageProfils * 10; i < currentPageProfils * itemsPerPage + itemsPerPage; i++)
        {
            profilIcons_btns[i].interactable = true;
        }
    }

    public void OnClickProfil(int id)
    {
        HM_NetworkController.instance.BLT_DeInitialize();

        HM_ContentCode.profilID = id;


        button1Tween.Kill();
        button1Tween = null;

        button2Tween.Kill();
        button2Tween = null;

        //redirection page de v�rification code profil
        CloseContent(HM_Scripts.contentCodeChild);

        //OLD
        /*//surbrillance profil choisi

        if (currentProfilID != -1)
        {
            profilIcons_btns[currentProfilID - 1].GetComponent<Image>().color = new Color32(184, 229, 255, 0);
        }

        currentProfilID = id;

        profilIcons_btns[id - 1].GetComponent<Image>().color = new Color32(184, 229, 255, 255);

        //activation du bouton de confirmation
        confirm_btn.interactable = true;*/
    }

    public void OnClickButtonConfirm()
    {
        HM_ContentCode.profilID = currentProfilID;

        //redirection page de v�rification code profil
        CloseContent(HM_Scripts.contentCodeChild);
    }

    public override int OnClosedContent(HM_Content nextContent = null)
    {
        return 0;
    }

   
}
