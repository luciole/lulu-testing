using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HM_DataController
{
    public static HM_Model model = new HM_Model();

    public static HM_ChildUser currentChildUser = null;
    public static HM_ChildUser secondaryChildUser = null;//pour ELARGIR
    public static HM_TeacherAccount currentTeacherAccount = null;
    public static HM_TeacherAccount newTeacherAccountTemp = null;

    public static string GetChildUserDataJSON()
    {
        if (currentChildUser == null)
            Debug.LogError("pas d'utilisateur actuellement connecté");

        return JsonConvert.SerializeObject(currentChildUser);
    }

    public static string GetSecondaryChildUserDataJSON()
    {
        if (secondaryChildUser == null)
            Debug.LogError("pas d'utilisateur secondaire actuellement connecté");

        return JsonConvert.SerializeObject(secondaryChildUser);
    }
}
