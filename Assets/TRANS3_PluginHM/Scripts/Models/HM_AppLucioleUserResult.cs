using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_AppLucioleUserResult
{
    public int activityId = -1;

    public string resultId = "";

    public string userId = "";

    public float globalDuration = -1;

    public float playDuration = -1;

    public string playDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

    [NonSerialized]
    public static string taskId_session = "";

    public string taskId = "";

    public string revisionDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

    public HM_AppLucioleUserResult_SessionData appData = new HM_AppLucioleUserResult_SessionData();
}

[Serializable]
public class HM_AppLucioleUserResult_SessionData
{
    //donn�es d'activit�
    public int activityID = 0;
    public int missionNbr = 0;//ajout�
    //public int score = 0;//supprim�
    public bool skipped = false;
    public int best_score = 0;//ajout�
    public int total_scores = 0;//ajout�
    public int nb_scores = 0;//ajout�
    public int nb_launches = 0;//ajout�
    public int current_score = 0;//ajout�
    public int maxScore = 0;//ajout�
    public int nb_aidecontext_used = 0;//ajout�
    public bool isScenario = false;
    public List<string> listThemes = new List<string>();

    //donn�es de contexte settings
    public string parcours = "CE1";
    
    //donn�es de contexte progression
    public int avancementGlobal = 0;
    public int avancementCurrentMission = 0;
    public int nbrEtoilesGain = 0;
    public int nbrEtoilesTotal = 0;
    public string currentGrade = "";

}

