using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HM_DataMigration
{
    public static string UpdateData(string dataJson, int dataVersion)
    {
        string dataJsonResult = "";

        /*if (dataVersion == -1 || dataVersion == 1)
        {
            dataJsonResult = MigrateToV2(dataJson);

            if (dataJsonResult.Length > 0)
            {
                Debug.Log("HM_DataMigration version 2 Finished");
                dataVersion = 2;
            }
        }*/

        //depuis Ao�t 2023 obligation d'avoir le mod�le de version 3 minimum

        return dataJsonResult;
    }

    //-1 ou 1 vers version 2 :
    //malheureusement il a exist� un changement sur Evasion pendant la version 1 sans modification du num�ro de version
    //il faut donc v�rifier pour ces cas l� comment les donn�es sont agenc�es manuellement et faire le correctif

    private static string MigrateToV2(string dataJson)
    {
        JObject dataObject = JObject.Parse(dataJson);
        JArray arrayTeachers = (JArray)dataObject["teacherAccounts"];
        foreach (JObject teacher in arrayTeachers)
        {
            JArray arrayChilds = (JArray)teacher["childUsers"];
            foreach (JObject child in arrayChilds)
            {
                //gestion migration data Elargir
                //cas Result appData qui a un nom diff�rente dans le plugin 0.5.3 (version en prod terrain), nomm� "sessionData"
                //cas sessionData potentiellement � Null dans le plugin 0.5.3 (version en prod terrain), � supprimer
                //cas Result ID provenant du plugin 0.5.3 (version en prod terrain) qui est un string "TRANS3_ELARGIR" au lieu d'un INT

                JObject childElargirUserData = (JObject)child["appElargirUserData"];
                JArray childElargirResults = (JArray)childElargirUserData["results"];
                JArray childElargirResultsNew = new JArray();

                foreach (JObject result in childElargirResults)
                {
                    bool needDelete = false;

                    if (result["activityId"].Type == JTokenType.String)
                    {
                        result["activityId"] = 835;
                    }

                    JToken tokenData = result["sessionData"];
                    if (tokenData != null)
                    {
                        if (((JToken)result["sessionData"]).Type == JTokenType.Null)
                        {
                            //Debug.Log("sessionData Null, suppression");
                            needDelete = true;
                        }
                        else
                        {
                            result.Add("appData", result["sessionData"]);
                        }
                        result.Remove("sessionData");
                    }

                    if(!needDelete)
                    {
                        childElargirResultsNew.Add(result);
                    }
                }

                childElargirUserData["results"] = childElargirResultsNew;



                //gestion migration data Evasion
                //cas donn�es de minijeu de type ARRAY au lieu d'�tre OBJECT (changement effectu� en v0.6.1 du plugin)
                //cas donn�es Parseval stock� en STRING au lieu d'�tre en ARRAY (changement effectu� en v0.6.1 du plugin)

                JObject childEvasionUserData = (JObject)child["appEvasionUserData"];
                JObject childEvasionProgression = (JObject)childEvasionUserData["progression"];
                if(childEvasionProgression["parsevalGhost"] is JArray)
                {
                    Debug.Log(childEvasionProgression["parsevalGhost"] is JArray);
                    Debug.Log(((JArray)childEvasionProgression["parsevalGhost"]).Count);
                    Debug.Log(childEvasionProgression["parsevalTower"] is JArray);
                    Debug.Log(((JArray)childEvasionProgression["parsevalTower"]).Count);
                    Debug.Log(childEvasionProgression["parsevalRunner"] is JArray);
                    Debug.Log(((JArray)childEvasionProgression["parsevalRunner"]).Count);
                    Debug.Log(childEvasionProgression["parsevalCastle"] is JArray);
                    Debug.Log(((JArray)childEvasionProgression["parsevalCastle"]).Count);
                }
                if (childEvasionProgression["parsevalGhost"] is JArray && ((JArray)childEvasionProgression["parsevalGhost"]).Count <= 1 && childEvasionProgression["parsevalTower"] is JArray && ((JArray)childEvasionProgression["parsevalTower"]).Count <= 1 && childEvasionProgression["parsevalRunner"] is JArray && ((JArray)childEvasionProgression["parsevalRunner"]).Count <= 1 && childEvasionProgression["parsevalCastle"] is JArray && ((JArray)childEvasionProgression["parsevalCastle"]).Count <= 1)
                {
                    Debug.Log("HM_DataMigration version -1 ou version 1 vers version 2 : format Evasion JARRAY count 1, migration possible");

                    if(((JArray)childEvasionProgression["parsevalGhost"]).Count == 0)
                    {
                        childEvasionProgression["parsevalGhost"] = new JObject();
                    }
                    else
                    {
                        JObject childEvasionProgressionGhostObject = (JObject)((JArray)childEvasionProgression["parsevalGhost"])[0];

                        //v�rification du format levelDifficulty et childAfterLevel, si c'est un String il faut le transformer en Array
                        JArray arrayLevels = (JArray)childEvasionProgressionGhostObject["levels"];
                        foreach (JObject level in arrayLevels)
                        {
                            if (level["levelDifficulty"].Type == JTokenType.String)
                            {
                                string value = level["levelDifficulty"].Value<string>();
                                List<int> levelDifficulty = new List<int>();
                                string[] values = value.Split(',');
                                foreach (string valueInt in values)
                                {
                                    levelDifficulty.Add(int.Parse(valueInt));
                                }

                                level["levelDifficulty"] = JArray.FromObject(levelDifficulty);
                            }
                            if (level["childAfterLevel"].Type == JTokenType.String)
                            {
                                string value = level["childAfterLevel"].Value<string>();
                                List<int> levelChild = new List<int>();
                                string[] values = value.Split(',');
                                foreach (string valueInt in values)
                                {
                                    levelChild.Add(int.Parse(valueInt));
                                }

                                level["childAfterLevel"] = JArray.FromObject(levelChild);
                            }
                        }

                        childEvasionProgression["parsevalGhost"] = childEvasionProgressionGhostObject;
                    }

                    if (((JArray)childEvasionProgression["parsevalTower"]).Count == 0)
                    {
                        childEvasionProgression["parsevalTower"] = new JObject();
                    }
                    else
                    {
                        JObject childEvasionProgressionTowerObject = (JObject)((JArray)childEvasionProgression["parsevalTower"])[0];

                        //v�rification du format levelDifficulty et childAfterLevel, si c'est un String il faut le transformer en Array
                        JArray arrayLevels = (JArray)childEvasionProgressionTowerObject["levels"];
                        foreach (JObject level in arrayLevels)
                        {
                            if (level["levelDifficulty"].Type == JTokenType.String)
                            {
                                string value = level["levelDifficulty"].Value<string>();
                                List<int> levelDifficulty = new List<int>();
                                string[] values = value.Split(',');
                                foreach (string valueInt in values)
                                {
                                    levelDifficulty.Add(int.Parse(valueInt));
                                }

                                level["levelDifficulty"] = JArray.FromObject(levelDifficulty);
                            }
                            if (level["childAfterLevel"].Type == JTokenType.String)
                            {
                                string value = level["childAfterLevel"].Value<string>();
                                List<int> levelChild = new List<int>();
                                string[] values = value.Split(',');
                                foreach (string valueInt in values)
                                {
                                    levelChild.Add(int.Parse(valueInt));
                                }

                                level["childAfterLevel"] = JArray.FromObject(levelChild);
                            }
                        }

                        childEvasionProgression["parsevalTower"] = childEvasionProgressionTowerObject;
                    }

                    if (((JArray)childEvasionProgression["parsevalRunner"]).Count == 0)
                    {
                        childEvasionProgression["parsevalRunner"] = new JObject();
                    }
                    else
                    {
                        JObject childEvasionProgressionRunnerObject = (JObject)((JArray)childEvasionProgression["parsevalRunner"])[0];

                        //v�rification du format levelDifficulty et childAfterLevel, si c'est un String il faut le transformer en Array
                        JArray arrayLevels = (JArray)childEvasionProgressionRunnerObject["levels"];
                        foreach (JObject level in arrayLevels)
                        {
                            if (level["levelDifficulty"].Type == JTokenType.String)
                            {
                                string value = level["levelDifficulty"].Value<string>();
                                List<int> levelDifficulty = new List<int>();
                                string[] values = value.Split(',');
                                foreach (string valueInt in values)
                                {
                                    levelDifficulty.Add(int.Parse(valueInt));
                                }

                                level["levelDifficulty"] = JArray.FromObject(levelDifficulty);
                            }
                            if (level["childAfterLevel"].Type == JTokenType.String)
                            {
                                string value = level["childAfterLevel"].Value<string>();
                                List<int> levelChild = new List<int>();
                                string[] values = value.Split(',');
                                foreach (string valueInt in values)
                                {
                                    levelChild.Add(int.Parse(valueInt));
                                }

                                level["childAfterLevel"] = JArray.FromObject(levelChild);
                            }
                        }

                        childEvasionProgression["parsevalRunner"] = childEvasionProgressionRunnerObject;
                    }

                    if (((JArray)childEvasionProgression["parsevalCastle"]).Count == 0)
                    {
                        childEvasionProgression["parsevalCastle"] = new JObject();
                    }
                    else
                    {
                        JObject childEvasionProgressionCastleObject = (JObject)((JArray)childEvasionProgression["parsevalCastle"])[0];

                        //v�rification du format levelDifficulty et childAfterLevel, si c'est un String il faut le transformer en Array
                        JArray arrayLevels = (JArray)childEvasionProgressionCastleObject["levels"];
                        foreach (JObject level in arrayLevels)
                        {
                            if (level["levelDifficulty"].Type == JTokenType.String)
                            {
                                string value = level["levelDifficulty"].Value<string>();
                                List<int> levelDifficulty = new List<int>();
                                string[] values = value.Split(',');
                                foreach (string valueInt in values)
                                {
                                    levelDifficulty.Add(int.Parse(valueInt));
                                }

                                level["levelDifficulty"] = JArray.FromObject(levelDifficulty);
                            }
                            if (level["childAfterLevel"].Type == JTokenType.String)
                            {
                                string value = level["childAfterLevel"].Value<string>();
                                List<int> levelChild = new List<int>();
                                string[] values = value.Split(',');
                                foreach (string valueInt in values)
                                {
                                    levelChild.Add(int.Parse(valueInt));
                                }

                                level["childAfterLevel"] = JArray.FromObject(levelChild);
                            }
                        }

                        childEvasionProgression["parsevalCastle"] = childEvasionProgressionCastleObject;
                    }
                }
            }
        }

        string dataJsonResult = dataObject.ToString(Formatting.None);

        /*HM_Log.ReplaceTextFile("test1", dataJson);
        HM_Log.ReplaceTextFile("test2", dataJsonResult);
        Debug.Log(dataJson);
        Debug.Log(dataJsonResult);*/

        return dataJsonResult;
    }
}
