using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_AppElargirUserResult
{
    public int activityId = -1;

    public string resultId = "";

    public string userId = "";

    public float globalDuration = -1;

    public float playDuration = -1;

    public string playDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

    [NonSerialized]
    public static string taskId_session = "";

    public string taskId = "";

    public string revisionDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

    public HM_AppElargirUserResult_SessionData appData = new HM_AppElargirUserResult_SessionData();
}

//HM_AppElargirUserResult_SessionData permet d'obtenir le contexte pr�cis pour chaque session effectu�
[Serializable]
public class HM_AppElargirUserResult_SessionData
{
    //donn�es de contexte JEU

    //les �l�ments suivants proviennent normalement d'information du dernier exercice du dernier text dans la PROGRESSION
    public int idText = -1;//l'id du texte effectu� 

    public int idExercice = -1;//l'index de la exercice [0 | 2], 3 exercices par textes (sauf textes �valuation)

    public int idSession = -1;//l'index de la session sur le parcours

    public int level = 2;//mode de lecture, 1: Syllabe; 2: Mot; 3: Groupe de sens; 4:Groupe de souffle 

    public int evaluation = -1;//note de l'�valuation [1 | 5]

    public DateTime evaluationDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc);

    public string evaluationUserID = "";//id de l'�l�ve secondaire ayant �valu�

    public string audio = "";//lien de l'audio

    //les �l�ments suivants proviennent directement de la PROGRESSION
    public int nextText = -2;//provient de PROGRESSION->nextText

    public int nextExercice = -1;//provient de PROGRESSION->nextExercice

    public int nextSession = -1;//provient de PROGRESSION->idSession

    //donn�es de contexte SETTINGS (afin de retrouver quels param�tres �taient utilis�s � la fin de la session)
    public string parcours = "CE1";

    public bool autoDifficultyChange = true;
  
    public bool highlightedBreathTimeHasBeenUsed = true;

    public bool wordLinksHasBeenUsed = true;

    public bool highlightDifficultWordsHasBeenUsed = true;

    public int karaokeHighlightBreathMinLevel = 3;

    public int discoveryTime = 120;

    public int evaluationTime = 300;

    public int freeReadingTime = 120;

    public int soundReturn = 0;

    public int readingSpeedObjective = 100;

    public int readingSpeed = 100;

    public int globalVolume = 50;

    //param�tre sp�cifique PluginHumansMatter (� ne pas retoucher)

    public string audioLink = "";
}
