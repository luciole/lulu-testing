using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_AppElargirUserSettings 
{
    public string revisionDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

    public string parcours = "CE1";

    public int nextText = -2;

    public int difficultyMode = 0;

    public bool autoDifficultyChange = true;

    public bool highlightBreathAvailabilityInDiscoveryMode = true;

    public bool wordLinksAvailabilityInDiscoveryMode = true;

    public bool highlightDifficultWordsAvailabilityInDiscoveryMode = true;

    public int karaokeHighlightBreathMinLevel = 3;

    public int discoveryTime = 120;//avant 120

    public int evaluationTime = 300;//avant 300

    public int freeReadingTime = 120;//avant 120

    public int soundReturn = 50;//de 0 � 100

    public int readingSpeedObjective = 100;

    public int readingSpeed = 100;//correspond � readingSpeedObjective, valeur par d�faut toujours

    public int globalVolume = 50;


    public HM_AppElargirUserSettings()
    {

    }
    public HM_AppElargirUserSettings(HM_AppElargirUserSettings dataToCopy)
    {
        CopyContent(dataToCopy);
    }

    public void CopyContent(HM_AppElargirUserSettings dataToCopy)
    {
        revisionDate = dataToCopy.revisionDate;

        parcours = dataToCopy.parcours;

        nextText = dataToCopy.nextText;

        difficultyMode = dataToCopy.difficultyMode;

        autoDifficultyChange = dataToCopy.autoDifficultyChange;

        highlightBreathAvailabilityInDiscoveryMode = dataToCopy.highlightBreathAvailabilityInDiscoveryMode;

        wordLinksAvailabilityInDiscoveryMode = dataToCopy.wordLinksAvailabilityInDiscoveryMode;

        highlightDifficultWordsAvailabilityInDiscoveryMode = dataToCopy.highlightDifficultWordsAvailabilityInDiscoveryMode;

        karaokeHighlightBreathMinLevel = dataToCopy.karaokeHighlightBreathMinLevel;

        discoveryTime = dataToCopy.discoveryTime;

        evaluationTime = dataToCopy.evaluationTime;

        freeReadingTime = dataToCopy.freeReadingTime;

        soundReturn = dataToCopy.soundReturn;

        readingSpeed = dataToCopy.readingSpeed;

        readingSpeedObjective = dataToCopy.readingSpeedObjective;

        globalVolume = dataToCopy.globalVolume;
    }
}
