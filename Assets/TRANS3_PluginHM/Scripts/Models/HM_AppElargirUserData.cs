using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Chaque app est décomposée de base en variables communes et deux blocs "progression" et "settings" spécifiques à l'app
[Serializable]
public class HM_AppElargirUserData
{
    public HM_AppElargirUserProgression progression = new HM_AppElargirUserProgression();//données d'avancement et de suivi

    public HM_AppElargirUserSettings settings = new HM_AppElargirUserSettings();//données de paramétrage

    public List<HM_AppElargirUserResult> results = new List<HM_AppElargirUserResult>();//données de résultat
}
