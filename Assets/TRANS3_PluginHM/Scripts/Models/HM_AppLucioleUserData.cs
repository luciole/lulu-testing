using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Chaque app est décompos�ée de base en variables communes et deux blocs "progression" et "settings" spécifiques à l'app
[Serializable]
public class HM_AppLucioleUserData
{
    public HM_AppLucioleUserProgression progression = new HM_AppLucioleUserProgression();//données d'avancement et de suivi

    public HM_AppLucioleUserSettings settings = new HM_AppLucioleUserSettings();//données de paramétrage

    public List<HM_AppLucioleUserResult> results = new List<HM_AppLucioleUserResult>();//données de résultat
}
