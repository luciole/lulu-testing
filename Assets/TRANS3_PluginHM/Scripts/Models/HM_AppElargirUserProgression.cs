using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_AppElargirUserProgression 
{
    public string dateUpdate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

    public int currentLevel = 2;

    public List<HM_AppElargirUserProgression_Text> texts = new List<HM_AppElargirUserProgression_Text>();

    public int nextText = -1;

    public int nextExercice = 0;

    public int nextSession = 0;
}

[Serializable]
public class HM_AppElargirUserProgression_Text
{
    public int idText = 1;

    public List<HM_AppElargirUserProgression_Exercice> exercices = new List<HM_AppElargirUserProgression_Exercice>();
}

[Serializable]
public class HM_AppElargirUserProgression_Exercice
{
    public int level = 1;

    public int evaluation = -1;

    public DateTime evaluationDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc);

    public string evaluationUserID = "";

    public string audio = "";

    public bool skipped = false;

    public int session = -1;
}