using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_AppLucioleUserSettings
{
    public string revisionDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

    public string parcours = "CE1";

    public bool skipLevel = false;

    public string currentLevelInformation = "";

    public int sessionTime = 3;

    public bool QRCodeEnabled = true;

    public int skipToMission = -1;

    public HM_AppLucioleUserSettings()
    {

    }
    public HM_AppLucioleUserSettings(HM_AppLucioleUserSettings dataToCopy)
    {
        CopyContent(dataToCopy);
    }

    public void CopyContent(HM_AppLucioleUserSettings dataToCopy)
    {
        revisionDate = dataToCopy.revisionDate;

        parcours = dataToCopy.parcours;

        skipLevel = dataToCopy.skipLevel;
        skipToMission = dataToCopy.skipToMission;
        sessionTime = dataToCopy.sessionTime;
        QRCodeEnabled = dataToCopy.QRCodeEnabled;
    }
}
