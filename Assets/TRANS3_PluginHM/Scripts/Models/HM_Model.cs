using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[Serializable]
public class HM_Model
{
    public List<HM_TeacherAccount> teacherAccounts = new List<HM_TeacherAccount>();

    public string deviceId = "";

    public int currentColorID = -1;

    public int modelVersion = 6;

    [NonSerialized]
    private const int currentModelVersion = 6;

    public string lastSynchronization = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

    /*private static string sharedFolderPath = "/storage/emulated/0/TRANS3_SharedFolder/";

    public static void Initialize()
    {
        // V�rifier si le dossier existe
        if (!Directory.Exists(sharedFolderPath))
        {
            Directory.CreateDirectory(sharedFolderPath);
        }
    }*/

    public static int Load(string saveData = null)
    {
        string dataJson = PlayerPrefs.GetString("HM_SaveData", "");

        if(saveData != null)
        {
            dataJson = saveData;
        }
        //jeu de donn�es pour test
        /*TextAsset mytxtData = (TextAsset)Resources.Load("saveData");
        dataJson = mytxtData.text;*/
        
        //Debug.Log("SAVE DATA : " +dataJson);

        if (dataJson.Length > 0)
        {
            try
            {
                HM_Model_Version modelVersionClass = JsonConvert.DeserializeObject<HM_Model_Version>(dataJson);

                if(modelVersionClass.modelVersion < currentModelVersion)
                {
                    string dataJsonResult = HM_DataMigration.UpdateData(dataJson, modelVersionClass.modelVersion);
                    if(dataJsonResult.Length == 0)
                    {
                        throw new Exception("Migration failed from version "+ modelVersionClass.modelVersion + " to version "+currentModelVersion);
                    }
                    else
                    {
                        dataJson = dataJsonResult;
                    }

                    //� ce niveau les donn�es doivent pouvoir �tre accept�es par le DeserializeObject � la derni�re version du format
                }

                HM_DataController.model = JsonConvert.DeserializeObject<HM_Model>(dataJson);

                if (modelVersionClass.modelVersion < currentModelVersion)
                {
                    HM_DataController.model.modelVersion = currentModelVersion;

                    //save pour bien enregistrer le nouveau num�ro de version
                    HM_DataController.model.Save();
                }
            }
            catch(Exception e)
            {
                //Debug.LogError("ERROR Deserialize JSON from data received JSON : " + dataJson);
                Debug.LogError("ERROR Loading SaveData : " + e.Message);
                HM_Log.AppendTextToFile("HM_Log.txt", "ErrorLoad " + e.Message);
                HM_Log.ReplaceTextFile("HM_saveDataError_" + DateTime.UtcNow.ToString("yyyy-MM-ddTHH-mm-ssK")+".json", dataJson);
                return -1;
            }

            //gestion sp�cifique remise � z�ro certains param�tres
            /*if(HM_DataController.model.teacherAccounts != null)
            {
                foreach (HM_TeacherAccount t in HM_DataController.model.teacherAccounts)
                {
                    foreach (HM_ChildUser c in t.childUsers)
                    {

                    }
                }
            }*/

            HM_Log.ReplaceTextFile("HM_saveDataLoad.json", JsonConvert.SerializeObject(HM_DataController.model));

            return 0;
        }
        else
        {
            HM_DataController.model.deviceId = System.Guid.NewGuid().ToString();
            HM_DataController.model.Save();
            return 1;
        }
    }

    public void Save()
    {
        string dataJson = JsonConvert.SerializeObject(HM_DataController.model);

        PlayerPrefs.SetString("HM_SaveData", JsonConvert.SerializeObject(HM_DataController.model));

        PlayerPrefs.Save();

        HM_Log.ReplaceTextFile("HM_saveData.json", JsonConvert.SerializeObject(HM_DataController.model));
    }

    public void DeleteSaveData()
    {
        PlayerPrefs.SetString("HM_SaveData","");
    }
}

[Serializable]
public class HM_Model_Version
{
    public int modelVersion = 1;
}