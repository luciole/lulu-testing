using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_ChildUser
{
    public string userID = "";//GUID g�n�r� via Guid.NewGuid().ToString() pour compte g�n�rique local, g�r� par le serveur pour profils cr��s via Interface Enseignant

    public int userIconID = -1;

    public string loginCode = "000000";

    public bool isGeneric = false;
    public bool fromGeneric = false;

    public bool activeUser = false;

    public int genericGroup = -1;

    //donn�es sp�cifiques profils li� � un compte prof
    public string name = "";
    public string surname = "";

    public bool isArchived = false;

    public int deviceColor = -1;

    public string schoolGrade = "";

    public string commentary = "";

    public string revisionDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

    //donn�es sp�cifiques ELARGIR
    public HM_AppElargirUserData appElargirUserData = new HM_AppElargirUserData();

    //donn�es sp�cifiques EVASION
    public HM_AppEvasionUserData appEvasionUserData = new HM_AppEvasionUserData();

    //donn�es sp�cifiques LUCIOLE
    public HM_AppLucioleUserData appLucioleUserData = new HM_AppLucioleUserData();
}
