using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_AppEvasionUserResult
{
    public int activityId = -1;

    public string resultId = "";

    public string userId = "";

    public float globalDuration = -1;

    public float playDuration = -1;

    public string playDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

    [NonSerialized]
    public static string taskId_session = "";

    public string taskId = "";

    public string revisionDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

    public HM_AppEvasionUserResult_SessionData appData = new HM_AppEvasionUserResult_SessionData();
}

[Serializable]
public class HM_AppEvasionUserResult_SessionData
{
    public int minigameId = -1;//identifiant du minijeu

    public List<HM_AppEvasionUserResult_SessionData_Level> levels = new List<HM_AppEvasionUserResult_SessionData_Level>();

    public int page = 0;
}

[Serializable]
public class HM_AppEvasionUserResult_SessionData_Level
{
    public List<int> levelDifficulty = new List<int>();

    public bool success = false;

    public int note = 0;

    public List<int> childAfterLevel = new List<int>();
}
