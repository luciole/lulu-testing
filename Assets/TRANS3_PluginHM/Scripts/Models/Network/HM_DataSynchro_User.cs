using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_DataSynchro_User
{
    public string userID;

    public string revisionDate;

    public int userIconID;

    public string loginCode;

    public bool isGeneric = false;

    public bool isArchived = false;

    public int genericGroup = -1;

    public string name = "";

    public string surname = "";

    public int deviceColor = -1;

    public string schoolGrade;

    public HM_DataSynchro_User()
    {
    }

    public HM_DataSynchro_User(HM_ChildUser user, HM_TeacherAccount teacher)
    {
        userID = user.userID;

        revisionDate = user.revisionDate;

        userIconID = user.userIconID;

        loginCode = user.loginCode;

        isGeneric = user.isGeneric;

        isArchived = user.isArchived;

        genericGroup = user.genericGroup;

        name = user.name;

        surname = user.surname;

        deviceColor = user.deviceColor;

        //on affecte par d�faut la couleur du device actuel au user si il n'avait pas de couleur affect�e
        if (user.deviceColor == -1)
        {
            deviceColor = HM_DataController.model.currentColorID;
        }

        schoolGrade = user.schoolGrade;
    }
}
