using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_DataSynchro 
{
    public string lastSynchronization = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

    public int modelVersion = -1;

    public string deviceId = "";

    public int deviceColor = -1;
	
    public string teacherCode = "";

    public List<int> genericGroupsUsed = new List<int>();

    public List<HM_DataSynchro_User> users = new List<HM_DataSynchro_User>();

    public List<HM_DataSynchro_UserProgression> usersProgressions = new List<HM_DataSynchro_UserProgression>();

    public List<HM_DataSynchro_UserSettings> usersSettings = new List<HM_DataSynchro_UserSettings>();

    public List<HM_DataSynchro_UserResult> usersResults = new List<HM_DataSynchro_UserResult>();

    public int LoadFromAppData(string _teacherCode)
    {
        lastSynchronization = HM_DataController.model.lastSynchronization;

        modelVersion = HM_DataController.model.modelVersion;

        deviceId = HM_DataController.model.deviceId;

        deviceColor = HM_DataController.model.currentColorID;

        teacherCode = _teacherCode;

        if (HM_Scripts.pluginController.appID == APP_ID.EVALULU)//pour cette application on demande toujours les donn�es sans en envoyer
        {
            return 0;
        }

        HM_TeacherAccount teacher = null;

        //string dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        DateTime initDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc);

        foreach (HM_TeacherAccount t in HM_DataController.model.teacherAccounts)
        {
            if(t.teacherCode.ToUpper().Equals(_teacherCode.ToUpper()))
            {
                teacher = t;
                break;
            }
        }

        if(teacher == null)
        {
            return -1;
        }

        foreach(int i in teacher.genericGroupsUsed)
        {
            genericGroupsUsed.Add(i);
        }

        bool hasActiveUser = false;
        foreach (HM_ChildUser c in teacher.childUsers)
        {
            if(c.activeUser)
            {
                hasActiveUser = true;

                users.Add(new HM_DataSynchro_User(c,teacher));

                //on n'envoit pas de donn�es qui n'ont pas �t� modifi�es au moins une fois, du coup check sur la valeur par d�faut de la date (EDIT : retirer d� � une erreur serveur non g�r� pour l'instant)
                //string dateFormatCheck = "";

                /*dateFormatCheck = DateTime.Parse(c.appEvasionUserData.progression.dateUpdate).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");
                c.appEvasionUserData.progression.dateUpdate = dateFormatCheck;
                if (!DateTime.ParseExact(dateFormatCheck, dateFormat, System.Globalization.CultureInfo.InvariantCulture).Equals(initDate))*/
                    usersProgressions.Add(new HM_DataSynchro_UserProgression_Evasion(c));

                /*dateFormatCheck = DateTime.Parse(c.appElargirUserData.progression.dateUpdate).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");
                c.appElargirUserData.progression.dateUpdate = dateFormatCheck;
                if (!DateTime.ParseExact(dateFormatCheck, dateFormat, System.Globalization.CultureInfo.InvariantCulture).Equals(initDate))*/
                    usersProgressions.Add(new HM_DataSynchro_UserProgression_Elargir(c));

                /*dateFormatCheck = DateTime.Parse(c.appLucioleUserData.progression.dateUpdate).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");
                c.appLucioleUserData.progression.dateUpdate = dateFormatCheck;
                if (!DateTime.ParseExact(dateFormatCheck, dateFormat, System.Globalization.CultureInfo.InvariantCulture).Equals(initDate))*/
                    usersProgressions.Add(new HM_DataSynchro_UserProgression_Luciole(c));

                /*dateFormatCheck = DateTime.Parse(c.appEvasionUserData.settings.revisionDate).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");
                c.appEvasionUserData.settings.revisionDate = dateFormatCheck;
                if (!DateTime.ParseExact(dateFormatCheck, dateFormat, System.Globalization.CultureInfo.InvariantCulture).Equals(initDate))*/
                    usersSettings.Add(new HM_DataSynchro_UserSettings_Evasion(c));

                /*dateFormatCheck = DateTime.Parse(c.appElargirUserData.settings.revisionDate).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");
                c.appElargirUserData.settings.revisionDate = dateFormatCheck;
                if (!DateTime.ParseExact(dateFormatCheck, dateFormat, System.Globalization.CultureInfo.InvariantCulture).Equals(initDate))*/
                    usersSettings.Add(new HM_DataSynchro_UserSettings_Elargir(c));

                /*dateFormatCheck = DateTime.Parse(c.appLucioleUserData.settings.revisionDate).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK");
                c.appLucioleUserData.settings.revisionDate = dateFormatCheck;
                if (!DateTime.ParseExact(dateFormatCheck, dateFormat, System.Globalization.CultureInfo.InvariantCulture).Equals(initDate))*/
                    usersSettings.Add(new HM_DataSynchro_UserSettings_Luciole(c));

                HM_DataController.model.Save();

                foreach (HM_AppEvasionUserResult resultEvasion in c.appEvasionUserData.results)
                {
                    usersResults.Add(new HM_DataSynchro_UserResult_Evasion(c, resultEvasion));
                }

                foreach (HM_AppElargirUserResult resultElargir in c.appElargirUserData.results)
                {
                    usersResults.Add(new HM_DataSynchro_UserResult_Elargir(c, resultElargir));
                }

                foreach (HM_AppLucioleUserResult resultLuciole in c.appLucioleUserData.results)
                {
                    usersResults.Add(new HM_DataSynchro_UserResult_Luciole(c, resultLuciole));
                }
            }
        }

        if(!hasActiveUser)
        {
            return -2;
        }

        return 0;
    }

    public int UnloadToAppData()
    {
        //TODO

        return 0;
    }
}

[Serializable]
public class HM_DataSynchro_ModelVesion
{
    public int modelVersion = -1;
}
