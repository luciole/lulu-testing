using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_DataSynchro_UserResult
{
    public int activityId;

    public string userID;

    public string revisionDate;

    public string resultId;

    public float globalDuration;

    public float playDuration;

    public string playDate;

    public string taskId;

    public void Load(HM_ChildUser user)
    {
        userID = user.userID;
    }
}

[Serializable]
public class HM_DataSynchro_UserResult_Evasion : HM_DataSynchro_UserResult
{
    public HM_AppEvasionUserResult_SessionData appData;

    public HM_DataSynchro_UserResult_Evasion()
    {
    }
    public HM_DataSynchro_UserResult_Evasion(HM_ChildUser user, HM_AppEvasionUserResult resultSave)
    {
        base.Load(user);

        activityId = 834;

        revisionDate = resultSave.revisionDate;

        resultId = resultSave.resultId;

        globalDuration = resultSave.globalDuration;

        playDuration = resultSave.playDuration;

        playDate = resultSave.playDate;

        appData = resultSave.appData;

        taskId = resultSave.taskId;
    }
}

[Serializable]
public class HM_DataSynchro_UserResult_Elargir : HM_DataSynchro_UserResult
{
    public HM_AppElargirUserResult_SessionData appData;

    public HM_DataSynchro_UserResult_Elargir()
    {
    }
    public HM_DataSynchro_UserResult_Elargir(HM_ChildUser user, HM_AppElargirUserResult resultSave)
    {
        base.Load(user);

        activityId = 835;

        revisionDate = resultSave.revisionDate;

        resultId = resultSave.resultId;

        globalDuration = resultSave.globalDuration;

        playDuration = resultSave.playDuration;

        playDate = resultSave.playDate;

        appData = resultSave.appData;

        taskId = resultSave.taskId;
    }
}

[Serializable]
public class HM_DataSynchro_UserResult_Luciole : HM_DataSynchro_UserResult
{
    public HM_AppLucioleUserResult_SessionData appData;

    public HM_DataSynchro_UserResult_Luciole()
    {
    }
    public HM_DataSynchro_UserResult_Luciole(HM_ChildUser user, HM_AppLucioleUserResult resultSave)
    {
        base.Load(user);

        activityId = 836;

        revisionDate = resultSave.revisionDate;

        resultId = resultSave.resultId;

        globalDuration = resultSave.globalDuration;

        playDuration = resultSave.playDuration;

        playDate = resultSave.playDate;

        appData = resultSave.appData;

        taskId = resultSave.taskId;
    }
}