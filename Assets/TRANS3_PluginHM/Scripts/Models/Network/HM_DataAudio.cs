using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_DataAudio
{
    public string localPath = "";
    public string filename = "";
    public string serverPath = "";
    public int childIconID = 0;
    public string childCode = "";
    public string teacherCode = "";
    public string resultID = "";
}
