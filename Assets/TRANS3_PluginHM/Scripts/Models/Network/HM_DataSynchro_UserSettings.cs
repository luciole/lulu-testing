using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_DataSynchro_UserSettings
{
    public int activityId;

    public string userID;

    public string revisionDate;

    public void Load(HM_ChildUser user)
    {
        userID = user.userID;
    }
}

[Serializable]
public class HM_DataSynchro_UserSettings_Evasion : HM_DataSynchro_UserSettings
{
    public HM_AppEvasionUserSettings appData;

    public HM_DataSynchro_UserSettings_Evasion()
    {
    }
    public HM_DataSynchro_UserSettings_Evasion(HM_ChildUser user)
    {
        base.Load(user);

        activityId = 834;

        revisionDate = user.appEvasionUserData.settings.revisionDate;

        appData = user.appEvasionUserData.settings;
    }
}

[Serializable]
public class HM_DataSynchro_UserSettings_Elargir : HM_DataSynchro_UserSettings
{
    public HM_AppElargirUserSettings appData;

    public HM_DataSynchro_UserSettings_Elargir()
    {
    }
    public HM_DataSynchro_UserSettings_Elargir(HM_ChildUser user)
    {
        base.Load(user);

        activityId = 835;

        revisionDate = user.appElargirUserData.settings.revisionDate;

        appData = user.appElargirUserData.settings;
    }
}

[Serializable]
public class HM_DataSynchro_UserSettings_Luciole : HM_DataSynchro_UserSettings
{
    public HM_AppLucioleUserSettings appData;
    public HM_DataSynchro_UserSettings_Luciole()
    {
    }
    public HM_DataSynchro_UserSettings_Luciole(HM_ChildUser user)
    {
        base.Load(user);

        activityId = 836;

        revisionDate = user.appLucioleUserData.settings.revisionDate;

        appData = user.appLucioleUserData.settings;
    }
}
