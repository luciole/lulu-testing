using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_DataSynchro_UserProgression
{
    public int activityId;

    public string userID;

    public string revisionDate;

    public void Load(HM_ChildUser user)
    {
        userID = user.userID;
    }
}

[Serializable]
public class HM_DataSynchro_UserProgression_Evasion : HM_DataSynchro_UserProgression
{
    public HM_AppEvasionUserProgression appData;

    public HM_DataSynchro_UserProgression_Evasion()
    {
    }

    public HM_DataSynchro_UserProgression_Evasion(HM_ChildUser user)
    {
        base.Load(user);

        activityId = 834;

        revisionDate = user.appEvasionUserData.progression.dateUpdate;

        appData = user.appEvasionUserData.progression;
    }
}

[Serializable]
public class HM_DataSynchro_UserProgression_Elargir : HM_DataSynchro_UserProgression
{
    public HM_AppElargirUserProgression appData;

    public HM_DataSynchro_UserProgression_Elargir()
    {
    }
    public HM_DataSynchro_UserProgression_Elargir(HM_ChildUser user)
    {
        base.Load(user);

        activityId = 835;

        revisionDate = user.appElargirUserData.progression.dateUpdate;

        appData = user.appElargirUserData.progression;
    }
}

[Serializable]
public class HM_DataSynchro_UserProgression_Luciole : HM_DataSynchro_UserProgression
{
    public HM_AppLucioleUserProgression appData;

    public HM_DataSynchro_UserProgression_Luciole()
    {
    }
    public HM_DataSynchro_UserProgression_Luciole(HM_ChildUser user)
    {
        base.Load(user);

        activityId = 836;

        revisionDate = user.appLucioleUserData.progression.dateUpdate;

        appData = user.appLucioleUserData.progression;
    }
}