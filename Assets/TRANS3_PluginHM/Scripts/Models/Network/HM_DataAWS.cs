using Amazon.Extensions.CognitoAuthentication;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_DataAWS
{
    [NonSerialized]
    public string _service = "execute-api";

    public string _idToken = "";
    public string _accessToken = "";
    public string _refreshToken = "";

    public string _accessKey = "";
    public string _secretKey = "";
    public string _securityToken = "";

    public string identityId = "";

    [NonSerialized]
    public DateTime refreshTime = DateTime.Now;

    [NonSerialized]
    public CognitoUser _user = null;

    public string _userid = "";
}
