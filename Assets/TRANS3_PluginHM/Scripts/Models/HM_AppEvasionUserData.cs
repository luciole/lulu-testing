using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Chaque app est d�compos�e de base en variables communes et deux blocs "progression" et "settings" sp�cifiques � l'app
[Serializable]
public class HM_AppEvasionUserData
{
    public HM_AppEvasionUserProgression progression = new HM_AppEvasionUserProgression();//donn�es d'avancement et de suivi

    public HM_AppEvasionUserSettings settings = new HM_AppEvasionUserSettings();//donn�es de param�trage

    public List<HM_AppEvasionUserResult> results = new List<HM_AppEvasionUserResult>();//donn�es de r�sultat
}
