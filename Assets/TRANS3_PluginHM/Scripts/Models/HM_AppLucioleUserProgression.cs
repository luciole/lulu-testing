using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_AppLucioleUserProgression 
{
    public string dateUpdate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

    public string fluenceId = "";

    public HM_AppLucioleUserProgression_LucioleParameters lucioleParameters = new HM_AppLucioleUserProgression_LucioleParameters();
}


[Serializable]
public class HM_AppLucioleUserProgression_LucioleParameters 
{
	public string applicationVersionLuciole = "";//ajout�
	public string iosBuild = "";//ajout�
	public int androidBundleVersionCode = 0;//ajout�
	//public int versionLuciole = 0;//supprim�
	public int avancementGlobal = 0;
	public int avancementCurrentMission = 0;
	public int nbrEtoilesGain = 0;
	public int nbrEtoilesTotal = 0;
	public int currentMissionIdx = 0;
	public string currentGrade = "";
	public bool carnetUnlocked = false;//format chang�
	public bool carteUnlocked = false;//format chang�
	public bool trainingUnlocked = false;//format chang�
	public bool aideContextuelleUnlocked = false;//ajout�
	public int quadriChrominoLastMissionUnlocked = 0;//num�ro de soumission
	public List<int> progressionMissions = new List<int>();
	public List<string> carnetDeNotes = new List<string>();//nom modifi�
	//public int scores = 0;//supprim�
	public int currentBlocIdx = 0;//ajout�
	public List<HM_AppLucioleUserProgression_Scoremanager> scoremanager = new List<HM_AppLucioleUserProgression_Scoremanager>();
	public HM_AppLucioleUserProgression_PlaylistEnseignant playlistEnseignant = new HM_AppLucioleUserProgression_PlaylistEnseignant();//ajout�
	public HM_AppLucioleUserProgression_Notifications notifications = new HM_AppLucioleUserProgression_Notifications();//ajout�
	public List<HM_AppLucioleUserProgression_Succes> succes = new List<HM_AppLucioleUserProgression_Succes>();//type modifi�
    public List<string> avatar = new List<string>();
}

[Serializable]
public class HM_AppLucioleUserProgression_Scoremanager
{
    public int activityID = 0;
	public int missionNbr = 0;//ajout�
	//public int score = 0;//supprim�
	public bool skipped = false;
	public int best_score = 0;//ajout�
	public int total_scores = 0;//ajout�
	public int nb_scores = 0;//ajout�
	public int nb_launches = 0;//ajout�
	public int maxScore = 0;//ajout�
	public int nb_aidecontext_used = 0;//ajout�
	public int challenge_score = 0;//ajout�
	public int initial_score = 0;
}

[Serializable]
public class HM_AppLucioleUserProgression_PlaylistEnseignant//ajout�
{
	public List<int> idActivites = new List<int>();
	public string dateDebut = "";
	public string dateFin = "";
	public int progressionDansPlaylist = 0;
	public List<HM_AppLucioleUserProgression_Scoremanager> scoremanager = new List<HM_AppLucioleUserProgression_Scoremanager>();
}

[Serializable]
public class HM_AppLucioleUserProgression_Notifications//ajout�
{
	public List<HM_AppLucioleUserProgression_NotificationsEvenement> listeEvenements = new List<HM_AppLucioleUserProgression_NotificationsEvenement>();
	public List<HM_AppLucioleUserProgression_NotificationsAlertes> listeAlertes = new List<HM_AppLucioleUserProgression_NotificationsAlertes>();
	public List<HM_AppLucioleUserProgression_NotificationsAutoEvals> listeAutoEvals = new List<HM_AppLucioleUserProgression_NotificationsAutoEvals>();
}

[Serializable]
public class HM_AppLucioleUserProgression_NotificationsEvenement//ajout�
{
	public string type = "";
	public string titre = "";
	public string description = "";
	public string date = "";
	public int importanceDeZeroADix = 0;
	public bool archived = false;
}

[Serializable]
public class HM_AppLucioleUserProgression_NotificationsAlertes//ajout�
{
	public string type = "";
	public string titre = "";
	public string description = "";
	public string date = "";
	public int importanceDeZeroADix = 0;
	public bool archived = false;
}

[Serializable]
public class HM_AppLucioleUserProgression_NotificationsAutoEvals//ajout�
{
	public string question = "";
	public List<HM_AppLucioleUserProgression_NotificationsAutoEvalsReponse> reponseList = new List<HM_AppLucioleUserProgression_NotificationsAutoEvalsReponse>();
	public int importanceDeZeroADix = 0;
	public bool archived = false;
}

[Serializable]
public class HM_AppLucioleUserProgression_NotificationsAutoEvalsReponse//ajout�
{
	public string date = "";
	public string reponse = "";
}

[Serializable]
public class HM_AppLucioleUserProgression_Succes//ajout�
{
	public string id = "";
	public string titre = "";
	public string description = "";
	public string date = "";
}

