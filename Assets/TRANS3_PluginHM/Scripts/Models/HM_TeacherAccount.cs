using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_TeacherAccount
{
    public int dataVersion = -1;

    public string teacherCode = "";

    public List<int> genericGroupsUsed = new List<int>();

    public List<HM_ChildUser> childUsers = new List<HM_ChildUser>();

    public HM_DataAWS dataAWS = new HM_DataAWS();

    public string dateLastSynchro = "";
}
