using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_AppEvasionUserProgression
{
    public string dateUpdate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");//donn�e g�r�e c�t� plugin

    public HM_AppEvasionUserProgression_Minigame parsevalGhost = new HM_AppEvasionUserProgression_Minigame();
    public HM_AppEvasionUserProgression_Minigame parsevalTower = new HM_AppEvasionUserProgression_Minigame();
    public HM_AppEvasionUserProgression_Minigame parsevalRunner = new HM_AppEvasionUserProgression_Minigame();
    public HM_AppEvasionUserProgression_Minigame parsevalCastle = new HM_AppEvasionUserProgression_Minigame();
}

[Serializable]
public class HM_AppEvasionUserProgression_Minigame
{
    public List<int> pages = new List<int>();//liste des pages acquises pour ce minijeu, l'entier repr�sentant la valeur de la page (bronze, argent ou or)

    public List<HM_AppEvasionUserProgression_Level> levels = new List<HM_AppEvasionUserProgression_Level>();//historique des niveau effectu�s
}

[Serializable]
public class HM_AppEvasionUserProgression_Level
{
    public List<int> levelDifficulty = new List<int>();//les valeurs Parseval de l'exercice

    public bool success = false;

    public int note = 0;

    public List<int> childAfterLevel = new List<int>();//les valeurs Parseval de l'�valutation utilisateur
}