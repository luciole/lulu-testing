using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HM_AppEvasionUserSettings
{
    public string revisionDate = new DateTime(2000, 1, 1, 00, 00, 00, DateTimeKind.Utc).ToString("yyyy-MM-ddTHH:mm:ssK");

    public string parcours = "CP";

    public HM_AppEvasionUserSettings()
    {

    }
    public HM_AppEvasionUserSettings(HM_AppEvasionUserSettings dataToCopy)
    {
        CopyContent(dataToCopy);
    }

    public void CopyContent(HM_AppEvasionUserSettings dataToCopy)
    {
        revisionDate = dataToCopy.revisionDate;

        parcours = dataToCopy.parcours;
    }
}
