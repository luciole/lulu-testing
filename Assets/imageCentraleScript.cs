using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ChargementJSONStructure;

public class imageCentraleScript : MonoBehaviour
{
    public mainManager mM;
    
    void OnMouseDown()
    {
        Debug.Log("OnMouseDown image centrale " + mM.playerTurn + " mM.current_step=" + mM.current_step);
        if (mM.playerTurn && mM.current_step==0)
        {
            mM.flecheReadyBeenClicked();
        }

    }
}
