using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class listenAnim : MonoBehaviour
{
    // Start is called before the first frame update
    public Image imageObject;
    public Sprite[] sprites;
    private int currentState = 0;  // Indice de l'�tat actuel.
    private float stateDuration = 0.1f;  // Dur�e de chaque �tat en seconde
    private Coroutine lacoroute;
    public void launchListen()
    {
        if(lacoroute != null)
        {
            StopCoroutine(lacoroute);
            imageObject.sprite = sprites[0];
        }
        lacoroute = StartCoroutine(AnimatePictogram());
    }

    private IEnumerator AnimatePictogram()
    {
        while (true)
        {
            // Changez le sprite de l'image UI en fonction de l'�tat actuel.
            imageObject.sprite = sprites[currentState];

            // Attendez la dur�e de l'�tat actuel.
            yield return new WaitForSeconds(stateDuration);

            // Passez � l'�tat suivant.
            currentState = (currentState + 1) % sprites.Length;
        }
    }
}
