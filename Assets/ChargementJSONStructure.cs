using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargementJSONStructure : MonoBehaviour
{
    public string jsonFileName; // Nom du fichier JSON (sans l'extension)
    public RootObject data;
    [Serializable]
    public class Asset
    {
        public string snd_path;
        public string img_path;
    }

    [Serializable]
    public class EpreuveItem
    {
        public string id;
        public string type;
        public string pret;
        public string consigne;
        public string fond;
        public List<Choix> choix;
        public bool randomiser;
        public string miseEnAction;
        public string reveil;
        public string transition;
        public string description;
        public bool enumerer;
        public string separateurChoix;
        public bool surligner;
        public string cible;
        public bool bloqueErreurs;
        public string explication;
        public float surlignerExplication;
        public string toucherLesChoix;
        public string feedbackPlus;
        public string feedbackMoins;
    }

    [Serializable]
    public class Choix
    {
        public string id;
        public string image;
        public string son;
        public string description;
        public bool correct;
        public string feedback;
        public int numZone;
        public string imgZone;
    }

    [Serializable]
    public class Epreuve
    {
        public string id;
        public string titre;
        public string desc;
        public string modalite;
        public string consigne;
        public string fond;
        public bool randomiser;
        public int nbItemsMax;
        public List<EpreuveItem> items;
    }

    [Serializable]
    public class Session
    {
        public string consigne;
        public List<Epreuve> epreuves;
    }

    [Serializable]
    public class RootObject
    {
        public Dictionary<string, Asset> assets;
        public List<Session> sessions;
    }

    public bool checkifitemexists(string assetname, bool checkofimgpath)
    {
        if (data.assets.ContainsKey(assetname))
        {
            if (checkofimgpath && data.assets[assetname].img_path != null)
            {
                Sprite image = Resources.Load<Sprite>(data.assets[assetname].img_path);
                if(image != null)
                {
                    return true;
                }

            }
            else if(data.assets[assetname].snd_path != null)
            {
                AudioClip aud = Resources.Load<AudioClip>(data.assets[assetname].snd_path);
                if(aud != null)
                {
                    return true;
                }

            }
        }
        return false;
    }

    public void chargeJsonStructure()
    {
        // Chargement du fichier JSON depuis le dossier "Assets"

        TextAsset jsonFile = Resources.Load<TextAsset>(jsonFileName);

        if (jsonFile != null)
        {
            // Lecture du contenu JSON sous forme de chaîne de caractères
            string jsonText = jsonFile.text;

            // Vous pouvez maintenant analyser le JSON en utilisant une bibliothèque comme JsonUtility, SimpleJSON, ou Json.NET (Newtonsoft.Json).
            // Par exemple, en utilisant JsonUtility :
            //data = JsonUtility.FromJson<RootObject>(jsonText);
            data = JsonConvert.DeserializeObject<RootObject>(jsonFile.ToString());
            Debug.Log("succes " + data);
        }
        else
        {
            Debug.LogError("Le fichier JSON " + jsonFileName + " n'a pas été trouvé.");
        }
    }
}
