using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class elementGD : boutonQCM
{
    // Start is called before the first frame update
    private Vector3 offset;
    private bool isDragging = false;
    public Vector3 positionInitiale;
    public dropZoneGD dropZOneWhereItsCurrentlyPlaced;

    void OnMouseDown()
    {
        if (mM.playerTurn && mM.gdmanager.oneElementIsMoving==null && !returningToPreviousLocation && !mM.gameObjFin.activeSelf)
        {
            if (mM.gdmanager.toucherLesCHoixGDActif)
            {
                mM.gdmanager.choixTouchedGD(this);
            }
            else
            {
                mM.gdmanager.oneElementIsMoving = this;
                mM.executeAsset(choix.son, soundFinishedPlaying,false);
                offset = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
                isDragging = true;
            }
            
        }
    }
    private void soundFinishedPlaying()
    {

    }
    void Start()
    {
        positionInitiale = this.transform.position;
    }

    private void OnMouseDrag()
    {
        if (isDragging && mM.playerTurn && mM.gdmanager.oneElementIsMoving == this && !returningToPreviousLocation && !mM.gdmanager.toucherLesCHoixGDActif && !mM.gameObjFin.activeSelf)
        {
            Vector3 newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) + offset;
            transform.position = new Vector3(newPosition.x, newPosition.y, transform.position.z);
        }
    }

    private void OnMouseUp()
    {
        
        if (mM.playerTurn && mM.gdmanager.oneElementIsMoving == this && !returningToPreviousLocation && !mM.gdmanager.toucherLesCHoixGDActif && !mM.gameObjFin.activeSelf)
        {
            mM.gdmanager.checkifElementIsOverAnEligiblePlace();
            mM.gdmanager.oneElementIsMoving = null;
        }
        isDragging = false;
    }
    private bool returningToPreviousLocation = false;
    public IEnumerator ReturnToInitialPosition(bool superfastmode = false)
    {
        returningToPreviousLocation = true;
        this.boxcoll.enabled = false;
        float duration = 0.25f; // Dur�e de l'animation de retour (en secondes)
        if (superfastmode)
        {
            duration = 0.0f;
        }
        float elapsedTime = 0f;
        Vector3 startPosition = transform.position;

        while (elapsedTime < duration)
        {
            transform.position = Vector3.Lerp(startPosition, positionInitiale, elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.position = positionInitiale; // Assurez-vous que l'�l�ment soit exactement � sa position initiale � la fin.
        this.boxcoll.enabled = true;
        returningToPreviousLocation = false;
    }

    public IEnumerator GoingToDropZone(float duration, Vector3 positionGD, int index, Action function=null)
    {
        yield return new WaitForSeconds(duration * index);
        if (duration == 0.0f)
        {
            duration = 0.5f;
        }

            float elapsedTime = 0f;
            Vector3 startPosition = transform.position;
            while (elapsedTime < duration)
            {
                float curveValue = mM.posScript.animationCurveSmoothStartEnd.Evaluate(elapsedTime/duration);
                transform.position = Vector3.Lerp(startPosition, positionGD, curveValue);
                elapsedTime += Time.deltaTime;
                yield return null;
            }
            transform.position = positionGD; // Assurez-vous que l'�l�ment soit exactement � sa position initiale � la fin.
            yield return new WaitForSeconds(0.5f);
            elapsedTime = 0f;
        duration = duration / 2.0f;
            while (elapsedTime <= duration)
            {
                float alpha = 1 - (elapsedTime / duration);
                // Assurez-vous que l'alpha reste dans la plage [0, 1].
                alpha = Mathf.Clamp01(alpha);
                
                // Mettez � jour la couleur du SpriteRenderer en modifiant son canal alpha.
                Color newColor = spriterenderer.color;
                newColor.a = alpha;
                spriterenderer.color = newColor;
                elapsedTime += Time.deltaTime;
                 yield return null;
        }
            StartCoroutine(ReturnToInitialPosition(true));
            elapsedTime = 0f;
            while (elapsedTime <= duration)
            {
                float alpha = (elapsedTime / duration);
                // Assurez-vous que l'alpha reste dans la plage [0, 1].
                alpha = Mathf.Clamp01(alpha);

                // Mettez � jour la couleur du SpriteRenderer en modifiant son canal alpha.
                Color newColor = spriterenderer.color;
                newColor.a = alpha;
                spriterenderer.color = newColor;
                elapsedTime += Time.deltaTime;
                yield return null;
        }
            //yield return new WaitForSeconds(duration);
            if(function != null)
            {
                function();
            }
       
    }





}
