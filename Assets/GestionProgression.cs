using UnityEngine;
using System.Collections.Generic;
using System;
using Newtonsoft.Json;

public class GestionProgression : MonoBehaviour
{
    public mainManager mM;
    // Appeler cette fonction pour sauvegarder la progression
    public void SauveProgression(int numeroSession3, bool forcezero = false)
    {
        // Cr�er un objet contenant les donn�es � sauvegarder, y compris le timestamp actuel



        // Charger la liste actuelle de sauvegardes ou cr�er une nouvelle liste si elle n'existe pas encore
        listDeDonnees sauvegardes = ChargerSauvegardes();
        bool foundinside = false;
        foreach(DonneesProgression dd in sauvegardes.listeDeDonnees)
        {
            if(dd.clef == mM.clefPourSauvegardeUtilisateur)
            {
                foundinside = true;
                if(numeroSession3> dd.NumeroSession)
                {
                    dd.NumeroSession = numeroSession3;
                }
                if (forcezero)
                {
                    dd.NumeroSession = 0;
                }
            }
        }
        if (!foundinside)
        {
            DonneesProgression newdonneesForUser = new DonneesProgression();
            newdonneesForUser.clef = mM.clefPourSauvegardeUtilisateur;
            newdonneesForUser.NumeroSession = numeroSession3;
            sauvegardes.listeDeDonnees.Add(newdonneesForUser);
        }


        // Ajouter la nouvelcole sauvegarde � la liste des sauvegardes associ�es � cette cl�
        Debug.Log("sauvegarde count = " + sauvegardes.listeDeDonnees.Count);
        string jsonData = JsonUtility.ToJson(sauvegardes);
        Debug.Log("jsontosave = " + jsonData);
        // Sauvegarder le JSON dans PlayerPrefs
        PlayerPrefs.SetString("Sauvegardes", jsonData);
        // Sauvegarder PlayerPrefs pour s'assurer que les donn�es sont enregistr�es imm�diatement
        PlayerPrefs.Save();
        Debug.Log("Progression sauvegard�e avec succ�s !");
        string jsonDataRetrieved  = PlayerPrefs.GetString("Sauvegardes", "{}");
        Debug.Log("jsonDataRetrieved = " + jsonDataRetrieved);
    }

    // Charger la liste actuelle de sauvegardes ou cr�er une nouvelle liste si elle n'existe pas encore
    public listDeDonnees ChargerSauvegardes()
    {
        string jsonData = PlayerPrefs.GetString("Sauvegardes", "{}");

        // V�rifier si la cha�ne JSON n'est pas vide
        if (!string.IsNullOrEmpty(jsonData))
        {
            // D�s�rialiser la cha�ne JSON en une liste d'objets DonneesProgression
            return JsonUtility.FromJson<listDeDonnees>(jsonData);
        }
        else
        {
            // Si la cha�ne JSON est vide, retourner une liste vide
            return new listDeDonnees();
        }
    }
}
[Serializable]
public class listDeDonnees
{
    [SerializeField]
    public List<DonneesProgression> listeDeDonnees;
}

// Classe pour stocker les donn�es de progression
[Serializable]
public class DonneesProgression
{
    public string clef;
    public int NumeroSession;
}
