using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreenScript : MonoBehaviour
{
    // Start is called before the first frame update
    public TextMeshProUGUI versionEvaluluTexte;
    void Start()
    {
        //versionEvaluluTexte.text = Application.version;
        timeElapsed = 0.0f;
        timeToElapseBeforeNextScene = 0.5f;
        Debug.LogError("splashscren");
        //Session.saveNewTraces(updateVersionLuciole(), "S_SPlashScreen","Nav_Scenes","Ouverture");
    }
    private float timeElapsed = 0.0f;
    public float timeToElapseBeforeNextScene = 0.5f;
    public static bool mode_Plugin_HM = true;// mode plugin Humansmatter
    [SerializeField] public string version_text;

    public string updateVersionLuciole()
    {
    #if UNITY_EDITOR
            version_text = "v_" + Application.version;
            version_text = version_text + "_bvc_" + PlayerSettings.Android.bundleVersionCode;
    #endif
        return version_text;
    }
    // Update is called once per frame
    void Update()
    {
        if (timeElapsed > timeToElapseBeforeNextScene)
        {
            Debug.Log("splashscreenfinished");
            if (mode_Plugin_HM)
            {
                SceneManager.LoadScene("TRANS3_PluginHM/Scenes/HM_SceneStart");
            }

            else
            {
                SceneManager.LoadScene("Scenes/MainScene");
            }
                
        }
        else
        {
            timeElapsed += Time.deltaTime;

        }
    }
}
